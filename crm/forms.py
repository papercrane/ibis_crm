from django import forms
from django.forms import ModelForm

from crm.models import Campaign


class CampaignForm(ModelForm):

    class Meta:
        model = Campaign
        widgets = {
            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "media_type": forms.Select(attrs={'class': "form-control"}),
            "budget": forms.NumberInput(attrs={'class': "form-control"}),
            "description": forms.Textarea(attrs={'class': "form-control", 'rows': 3}),
            "offer_details": forms.Textarea(attrs={'class': "form-control", 'rows': 3}),
            "start_date": forms.DateInput(attrs={'class': "form-control tdate", 'id': "start_date"}),
            "end_date": forms.DateInput(attrs={'class': "form-control tdate", 'id': "end_date"}),
            "media": forms.Select(attrs={'class': "form-control"}),
            "category": forms.Select(attrs={'class': "form-control"}),
            "location": forms.Select(attrs={'class': "form-control"}),

        }
        fields = ['name','budget','media_type','description', 'offer_details', 'start_date', 'end_date','category', 'location']

