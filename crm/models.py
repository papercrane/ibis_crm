from django.db import models
from erp_core.models import ErpAbstractBaseModel
from django.core.validators import MinValueValidator
from location.models import City


class Media(ErpAbstractBaseModel):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class CampaignCategory(ErpAbstractBaseModel):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Campaign(ErpAbstractBaseModel):
    ONLINE = 'online'
    OFFLINE = 'offline'
    TYPES = (
        (ONLINE, "online"),
        (OFFLINE, "offline"),
    )
    name = models.CharField(max_length=255)
    media_type = models.CharField(max_length=30, choices=TYPES,default=TYPES[0])
    budget = models.DecimalField(max_digits=20, decimal_places=2, default=0, validators=[MinValueValidator(0)])
    description = models.TextField()
    offer_details = models.TextField(null=True, blank=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    no_of_responses = models.PositiveIntegerField(default=0)
    no_of_conversions = models.PositiveIntegerField(default=0)
    group = models.TextField('Campaign Group', null=True, blank=True)

    media = models.ForeignKey(Media, on_delete=models.CASCADE, null=True, blank=True)
    category = models.ForeignKey(CampaignCategory, on_delete=models.CASCADE)
    location = models.ForeignKey(City, on_delete=models.CASCADE, null=True, blank=True)



    def __str__(self):
        return self.name