from django.conf.urls import url
from crm import campaign

app_name = "crm"

urlpatterns = [

    url(r'^campaign/create/$', campaign.campaign_create, name="campaign_create"),
    url(r'^campaign/list/$', campaign.campaign_list, name="campaign_list"),
    url(r'^campaign/(?P<pk>[^/]+)/detail/$', campaign.campaign_detail, name="campaign_detail"),
    url(r'^campaign/(?P<pk>[^/]+)/edit/$', campaign.campaign_edit, name="campaign_edit"),
    url(r'^campaign/(?P<pk>[^/]+)/list_products/$', campaign.products, name="products"),


]
