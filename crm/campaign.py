from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from crm.filters import CampaignFilter
from crm.forms import CampaignForm
from crm.models import Campaign

@login_required
def campaign_list(request):
    page_number = request.GET.get('page', 1)

    f = CampaignFilter(request.GET, queryset=Campaign.objects.all().order_by("-created_at"))
    paginator = Paginator(f.qs, 20)

    try:
        campaigns = paginator.page(page_number)
    except PageNotAnInteger:
        campaigns = paginator.page(1)
    except EmptyPage:
        campaigns = paginator.page(paginator.num_pages)

    context = {

        'campaigns': campaigns,
        'form': f.form,

    }
    return render(request, "crm/campaign/list.html", context)

@login_required
def campaign_create(request):
    campaign_form = CampaignForm(request.POST or None)
    if request.method == "POST":
        if campaign_form.is_valid():
            campaign = campaign_form.save(commit=False)
            campaign.save()
            return HttpResponseRedirect(reverse('crm:campaign_detail', args=[campaign.id]))
    context = {
        "campaign_form": campaign_form,
        'form_url': reverse_lazy('crm:campaign_create'),
        "type": "create"
    }
    return render(request, 'crm/campaign/edit.html', context)

@login_required
def campaign_detail(request, pk):
    campaign = Campaign.objects.get(id=pk)
    context = {
        "campaign": campaign,
        "pk": pk
    }
    return render(request, "crm/campaign/detail.html", context)

@login_required
def campaign_edit(request, pk):
    campaign = get_object_or_404(Campaign, id=pk)
    campaign_form = CampaignForm(
        request.POST or None, instance=campaign)
    if request.method == "POST":
        if campaign_form.is_valid():
            campaign = campaign_form.save(commit=False)
            campaign.save()
            return HttpResponseRedirect(reverse('crm:campaign_detail', args=[campaign.id]))
    context = {
        "campaign_form": campaign_form,
        'form_url': reverse_lazy('crm:campaign_edit', args=[campaign.pk]),
        "type": "edit",
    }
    return render(request, 'crm/campaign/edit.html', context)

@login_required
def products(request, pk):
    campaign = Campaign.objects.get(id=pk)
    context = {
        "campaign": campaign,
    }
    return render(request, "crm/campaign/products.html", context)
