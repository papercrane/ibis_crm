from sitetree.utils import item, tree

sitetrees = (
    tree('sidebar', items=[
        item(
            'Campaign',
            'crm:campaign_list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="newspaper-o",

        ),
    ]),

    tree('sidebar_branch_head', items=[
        item(
            'Campaign',
            'crm:campaign_list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="newspaper-o",

        ),
    ]),

    tree('sidebar_marketing_head', items=[
        item(
            'Campaign',
            'crm:campaign_list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="newspaper-o",

        ),
    ]),

    tree('sidebar_director', items=[
        item(
            'Campaign',
            'crm:campaign_list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="newspaper-o",
        ),
    ]),

)
