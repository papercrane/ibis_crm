from django import template

register = template.Library()


@register.filter
def get_type(value):
    return type(value)


@register.simple_tag
def compare_uuid(arg1, arg2):
    if str(arg1) == str(arg2):
        return "selected"
    return ""


@register.simple_tag
def changeSign(arg1):
    return -arg1


@register.assignment_tag
def totaladmission_cal(arg1):
    ta = 0
    for item in arg1:
        if 'total_admission' in item.keys():
            ta = ta + item['total_admission']
    return ta


@register.assignment_tag
def closedadmission_cal(arg1):
    ca = 0
    for item in arg1:
        if 'closed_admission' in item.keys():
            ca = ca + item['closed_admission']
    return ca


@register.assignment_tag
def currentadmission_cal(arg1):
    ca = 0
    for item in arg1:
        if 'current_admission' in item.keys():
            ca = ca + item['current_admission']
    return ca
# @register.filter(name='display')
# def display_value(bf):
#     """Returns the display value of a BoundField"""
#     return dict(bf.field.choices).get(bf.data, '')

@register.simple_tag
def pag_count(arg1, arg2,arg3):
    present_count = arg1+(arg2-1)*arg3
    return present_count