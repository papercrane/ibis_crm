import re

import django_filters
from django import forms
from django.contrib.auth.models import User
from django.db.models import Q
from crm.models import Campaign
from enquiry.models import Enquiry,Activity
from product.models import Product
from django_filters import widgets
from erp_core.models import ActivityType
from employee.models import Employee

class EnquiryFilter(django_filters.FilterSet):
    code = django_filters.CharFilter(name="code", lookup_expr='icontains',
                                     widget=forms.TextInput(attrs={'class': 'form-control'}))
    name_of_candidate = django_filters.CharFilter(name="name_of_candidate", lookup_expr='icontains',
                                                  widget=forms.TextInput(attrs={'class': 'form-control'}))
    year_of_passout = django_filters.NumberFilter(name="year_of_passout", lookup_expr='iexact',
                                                  widget=forms.TextInput(attrs={'class': 'form-control'}))
    course = django_filters.ModelChoiceFilter(
        queryset=Product.objects.all(),
        # name='',
        method='filter_by_program',
        widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"})
    )
    campaign = django_filters.ModelChoiceFilter(
        queryset=Campaign.objects.all(),
        # name='',
        method='filter_by_campaign',
        widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"})
    )
    status = django_filters.ChoiceFilter(choices=Enquiry.STATUSES,
                                         widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"}))

    activity_status = django_filters.ChoiceFilter(choices=Activity.STATUSES, method='filter_by_activity_status' ,widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"}))

    special_care = django_filters.BooleanFilter(
        name="special_care",
        widget=widgets.BooleanWidget(attrs={'class': 'form-control'})
    )
    contact_number = django_filters.CharFilter(name="contact_number", lookup_expr='icontains',
                                                  widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    date = django_filters.CharFilter(method='filter_by_date_range',widget=forms.TextInput(attrs={'class': 'form-control tdate', 'style': "width: 100%;", 'name':'month_year'}))

    class Meta:
        model = Enquiry
        fields = ['code', ]

    def filter_by_program(self, queryset, name, value):
        return queryset.filter(
            Q(course__name__icontains=value)
        )

    def filter_by_campaign(self, queryset, name, value):
        return queryset.filter(
            Q(campaign__name__icontains=value)
        )

    def filter_by_activity_status(self, queryset, name, value):
        return queryset.filter(
            Q(activities__status__icontains=value)
        )

    def filter_by_date_range(self, queryset, name, value):
        date_for_check = re.findall('\d{4}-\d{2}-\d{2}', str(value))
        return queryset.filter(
            Q(date__range=date_for_check)
        )



class ActivityFilter(django_filters.FilterSet):
    
    type = django_filters.ModelChoiceFilter(
        queryset=ActivityType.objects.all(),
        # name='',
        method='filter_by_type',
        widget=forms.Select(attrs={'class': 'form-control tdate', 'style': "width: 100%;"})
    )
    assigned_to = django_filters.ModelChoiceFilter(
        queryset=Employee.objects.all(),
        name='assigned_to',
        # method='filter_by_type',
        widget=forms.Select(attrs={'class': 'form-control tdate', 'style': "width: 100%;"})
    )
    created_by = django_filters.ModelChoiceFilter(
        queryset=User.objects.all(),
        name='created_by',
        # method='filter_by_type',
        widget=forms.Select(attrs={'class': 'form-control tdate', 'style': "width: 100%;"})
    )
    date = django_filters.CharFilter(widget=forms.TextInput(attrs={'class': 'form-control', 'style': "width: 100%;"}))



    class Meta:
        model = Activity
        fields = ['type','date']


    def filter_by_type(self, queryset, name, value):
        return queryset.filter(
            Q(type__name__icontains=value)
        )
