from django import forms
from django.forms import ModelForm
from enquiry.models import Activity, CodeSetting, Enquiry
from erp_core.models import ActivityType


class EnquiryActivityForm(ModelForm):
    class Meta:
        model = Activity
        widgets = {
            "remark": forms.Textarea(attrs={'class': "form-control", 'required': "required", 'id': "remark"}),
            "date": forms.DateInput(attrs={'class': "form-control datepicker", 'required': "required", "type": "text"}),
            "type": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "assigned_to": forms.Select(
                attrs={'class': "form-control", 'required': "required", 'id': "employee"}),
        }
        fields = ['date', 'remark', 'type', 'assigned_to']


class CodeSettingsForm(ModelForm):
    class Meta:
        model = CodeSetting
        widgets = {
            "prefix": forms.TextInput(attrs={'class': "form-control", 'id': "prefix", 'required': "required"}),
            "count_index": forms.TextInput(attrs={'class': "form-control", 'id': "count_index"}),
            "no_of_characters": forms.TextInput(attrs={'class': "form-control", 'id': "no_of_characters"}),
        }
        fields = ['prefix', 'count_index', 'no_of_characters']


class EnquiryForm(ModelForm):
    class Meta:
        model = Enquiry
        widgets = {
            "code": forms.TextInput(attrs={'class': "form-control", 'required': "required", 'id': "code"}),
            "name_of_candidate": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "contact_number": forms.NumberInput(attrs={'class': "form-control cntct_nbmr", 'required': "required"}),
            "location": forms.Select(attrs={'class': "form-control"}),
            "name_of_college": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "year_of_passout": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            "course": forms.SelectMultiple(
                attrs={'class': 'form-control js-basic-multiple', 'multiple': "TRUE", 'id': 'id_program'}),
            "status": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "campaign": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "refered_by_employee": forms.Select(attrs={'class': "form-control"}),
            "refered_by_student": forms.Select(attrs={'class': "form-control"}),
            "special_care": forms.CheckboxInput(attrs={}),
            "email": forms.EmailInput(attrs={'class': "form-control"}),
            "stream": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "whatsapp_number": forms.NumberInput(attrs={'class': "form-control wtspp_nmbr", 'required': "required"}),
            "assigned_to": forms.Select(attrs={'class': "form-control"}),
            "date": forms.DateInput(
                attrs={'class': "form-control tdate", 'required': "required", "type": "text", "name": "datefilter"}),
            "interested_course": forms.SelectMultiple(
                attrs={'class': 'form-control js-basic-multiple', 'multiple': "TRUE", 'id': 'id_course'}),

        }

        fields = ['refered_by_student', 'email', 'code', 'name_of_candidate', 'contact_number', 'location',
                  'name_of_college', 'year_of_passout', 'course', 'status', 'campaign', 'refered_by_employee',
                  'special_care', 'stream', 'whatsapp_number', 'assigned_to', 'date', 'interested_course']

    # def __init__(self, *args, **kwargs):
    #     contact_qs = None
    #     if 'contact_qs' in kwargs:
    #         contact_qs = kwargs.pop('contact_qs')

    #     employee_qs = None
    #     if 'employee_qs' in kwargs:
    #         employee_qs = kwargs.pop('employee_qs')

    #     super(EnquiryForm, self).__init__(*args, **kwargs)
    #     if contact_qs:
    #         self.fields['contact'].queryset = contact_qs
    #     if employee_qs:
    #         self.fields['employee'].queryset = employee_qs
    # if 'initial' in kwargs and 'salesperson' in kwargs['initial']:
    #     self.fields['employee'].initial = kwargs['initial']['salesperson']


class EnquiryEditForm(ModelForm):
    class Meta:
        model = Enquiry
        widgets = {
            # "employee": forms.Select(attrs={'class': "form-control"}),
            "name_of_candidate": forms.Select(attrs={'class': "form-control", 'required': "required", 'id': "type"}),

        }

        fields = ['name_of_candidate', ]


class ActivityTypeForm(ModelForm):
    class Meta:
        model = ActivityType
        widgets = {
            # "employee": forms.Select(attrs={'class': "form-control"}),
            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required", 'id': "type"}),

        }

        fields = ['name', ]


class ActivityForm(ModelForm):
    class Meta:
        model = Activity
        widgets = {
            "remark": forms.TextInput(attrs={'class': "form-control", 'required': "required", 'id': "remark"}),
            "date": forms.DateInput(
                attrs={'class': "form-control tdate", 'required': "required", "type": "text", "name": "datefilter"}),
            "type": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "assigned_to": forms.Select(attrs={'class': "form-control"}),

        }
        fields = ['date', 'remark', 'type', 'assigned_to']


class UploadFileForm(forms.Form):
    file = forms.FileField()


class StudentEnquiryForm(ModelForm):
    class Meta:
        model = Enquiry
        widgets = {
            "name_of_candidate": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),

            "location": forms.Select(attrs={'class': "form-control"}),
            "name_of_college": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "year_of_passout": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            "campaign": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "email": forms.EmailInput(attrs={'class': "form-control", 'required': "required"}),

        }
        fields = ['name_of_candidate', 'location', 'name_of_college', 'year_of_passout', 'campaign',
                  'email']


class NewRegistrationLookupForm(forms.Form):
    contact = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': "form-control", 'required': "required", 'placeholder': "Contact number or Email"}))


class NewRegistrationForm(ModelForm):
    class Meta:
        model = Enquiry
        widgets = {
            "code": forms.TextInput(attrs={'class': "form-control", 'required': "required", 'id': "code"}),
            "name_of_candidate": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "contact_number": forms.NumberInput(attrs={'class': "form-control cntct_nbmr", 'required': "required"}),
            "location": forms.Select(attrs={'class': "form-control"}),
            "name_of_college": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "year_of_passout": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            "course": forms.SelectMultiple(
                attrs={'class': 'form-control js-basic-multiple', 'multiple': "TRUE", 'id': 'id_program'}),
            "status": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "campaign": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "refered_by_employee": forms.Select(attrs={'class': "form-control"}),
            "refered_by_student": forms.Select(attrs={'class': "form-control"}),
            # "special_care": forms.CheckboxInput(attrs={}),
            "email": forms.EmailInput(attrs={'class': "form-control"}),
            "stream": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "whatsapp_number": forms.NumberInput(attrs={'class': "form-control wtspp_nmbr", 'required': "required"}),
            "assigned_to": forms.Select(attrs={'class': "form-control"}),
            "date": forms.DateInput(
                attrs={'class': "form-control tdate", 'required': "required", "type": "text", "name": "datefilter"}),
            "interested_course": forms.SelectMultiple(
                attrs={'class': 'form-control js-basic-multiple', 'multiple': "TRUE", 'id': 'id_course'}),

        }

        fields = ['refered_by_student', 'email', 'code', 'name_of_candidate', 'contact_number', 'location',
                  'name_of_college', 'year_of_passout', 'course', 'status', 'campaign', 'refered_by_employee',
                  'stream', 'whatsapp_number', 'assigned_to', 'date', 'interested_course']
