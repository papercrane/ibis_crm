import socket

from django.shortcuts import get_object_or_404, render
from django.db import transaction
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
from django.core.mail import BadHeaderError, EmailMessage
from django import template
from enquiry.filters import EnquiryFilter, ActivityFilter
from enquiry.models import Enquiry, Activity
from enquiry.forms import EnquiryForm, ActivityForm, UploadFileForm, NewRegistrationLookupForm, StudentEnquiryForm, \
    NewRegistrationForm
from erp_core.pagination import paginate, custom_paginate

from smtplib import SMTPException
from location.forms import CityForm

from django.db.models import Q
from django.db.models import Count
from django.contrib.auth.decorators import login_required, permission_required
from django.utils import timezone

register = template.Library()


@login_required
def list(request):
    employee = request.user.employee.primary_designation.name
    if employee == "Tele-Caller":
        enquires = Enquiry.objects.annotate(
            activity_count=Count('activities', filter=Q(activities__status=Activity.STATUS_NO_RESPONSE))).filter(
            Q(employee=request.user.employee) | Q(assigned_to=request.user.employee)).order_by('-date')
    else:
        enquires = Enquiry.objects.annotate(
            activity_count=Count('activities', filter=Q(activities__status=Activity.STATUS_NO_RESPONSE))).order_by(
            '-date')

    check_list1 = ["Admin", "Front Office Admin", "Tele-Caller", "Tele Caller Head", "Marketing Head"]

    f = EnquiryFilter(request.GET, queryset=enquires)
    upload_form = UploadFileForm()
    enquiries, pagination, current_page, per_page = custom_paginate(request, queryset=f.qs, per_page=60, default=True)
    context = {
        'enquiries': enquiries,
        'pagination': pagination,
        'filter': f,
        'count': enquires.count(),
        'upload_form': upload_form,
        'employee': employee,
        'check_list1': check_list1,
        'current_page': current_page,
        'per_page': per_page
    }

    return render(request, "enquiry/list.html", context)


@login_required
def mylist(request):
    user = request.user.employee
    enquires = Enquiry.objects.filter(employee=user)
    f = EnquiryFilter(request.GET, queryset=enquires)
    upload_form = UploadFileForm()

    enquiries, pagination = paginate(request, queryset=f.qs, per_page=20, default=True)
    context = {
        'enquiries': enquiries,
        'filter': f,
        'count': enquires.count(),
        'upload_form': upload_form
    }
    return render(request, "enquiry/mylist.html", context)


@login_required
@permission_required('enquiry.delete_enquiry', raise_exception=True)
def delete(request, pk):
    if request.method == "POST":
        enquiry = get_object_or_404(Enquiry, id=pk)
        enquiry.delete()
        messages.add_message(request, messages.SUCCESS, 'Lead deleted successfully')

    return HttpResponseRedirect(reverse('enquiry:list'))


@login_required
@transaction.atomic
def create(request):
    enquiry_form = EnquiryForm(request.POST or None)
    activity_form = ActivityForm(request.POST or None)
    city_form = CityForm()
    if request.method == "POST":
        if enquiry_form.is_valid() and activity_form.is_valid():
            enquiry = enquiry_form.save(commit=False)
            enquiry.employee = request.user.employee
            enquiry.date = timezone.now().date()
            enquiry.save()
            enquiry_form.save_m2m()
            activity = activity_form.save(commit=False)
            activity.enquiry = enquiry
            activity.created_by = request.user
            activity.save()
            messages.add_message(request, messages.SUCCESS, 'Lead created successfully')
            return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[enquiry.id]))
    context = {
        'enquiry_form': enquiry_form,
        'form_url': reverse_lazy('enquiry:create'),
        'type': "Create",
        'city_form': city_form,
        'activity_form': activity_form,
    }
    return render(request, "enquiry/edit.html", context)


@login_required
def pending_follow_ups(request):
    activities = Activity.objects.filter(date__lte=timezone.now().date())
    f = ActivityFilter(request.GET, queryset=activities)
    activities, pagination = paginate(request, queryset=f.qs, per_page=50, default=True)
    context = {
        'activities': activities,
        'pagination': pagination,
        'filter': f,

    }
    return render(request, "enquiry/pendingfollowups.html", context)


@login_required
def enquiry_detail(request, pk):
    enquiry = get_object_or_404(Enquiry, id=pk)
    designation = request.user.employee.primary_designation.name
    activities = enquiry.activities.filter(status=Activity.STATUS_COMPLETED)
    followups = enquiry.activities.exclude(status=Activity.STATUS_COMPLETED)
    designation_checklist = ["Front Office Admin", "Admin", "Ibis Admin"]
    context = {
        'enquiry': enquiry,
        'designation_checklist': designation_checklist,
        'designation': designation,
        'activities': activities,
        'followups': followups,
        'statuses': Activity.STATUSES,
        'lead_statuses': Enquiry.STATUSES,
    }
    return render(request, "enquiry/enquiry_detail.html", context)


@login_required
@transaction.atomic
@permission_required('enquiry.change_enquiry', raise_exception=True)
def edit(request, pk):
    enquiry = get_object_or_404(Enquiry, id=pk)
    enquiry_form = EnquiryForm(request.POST or None, instance=enquiry)
    if request.method == "POST":
        if enquiry_form.is_valid():
            enquiry = enquiry_form.save(commit=False)
            enquiry.save()
            enquiry_form.save_m2m()
            messages.add_message(request, messages.SUCCESS, 'Lead updated successfully')
            return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[enquiry.id]))

    context = {
        'enquiry': enquiry,
        'enquiry_form': enquiry_form,
        'form_url': reverse_lazy('enquiry:edit', args=[enquiry.id]),
        'type': "Edit",

    }
    return render(request, "enquiry/edit.html", context)


@login_required
def complete_activity(request, pk):
    activity = Activity.objects.get(id=pk)
    activity.complete = True
    activity.save()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@login_required
def notification_mail(request, email, enquiry):
    subject = "Ibis"
    html_content = "<p>Hi " + enquiry.customer.user.first_name + " ,</p><p>Greetings from Eazylink.We are happy to help you find your right career.</p><p> Your application status : " + enquiry.status.name + "</p><p>.For further enquiries please contact us.</p><p>Thank You</p>"

    try:
        msg = EmailMessage(subject, html_content, 'infoeazylink@gmail.com',
                           [email])
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send()
        pass
    except BadHeaderError:
        messages.add_message(request, messages.ERROR, 'Network Issue..')
        return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[enquiry.pk]))

    except socket.gaierror:
        messages.add_message(request, messages.ERROR, 'Mail not send due to bad connection')
        return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[enquiry.pk]))

    except SMTPException:  # Didn't make an instance.
        messages.add_message(request, messages.ERROR, 'Mail not send due to bad connection')
        return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[enquiry.pk]))

    except socket.error:
        messages.add_message(request, messages.ERROR, 'Mail not send due to bad connection')
        return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[enquiry.pk]))


@login_required
def myfollowups(request):
    activities = Activity.objects.filter(Q(assigned_to=request.user.employee) | Q(created_by=request.user),
                                         status=Activity.STATUS_PENDING).order_by('date')
    f = ActivityFilter(request.GET, queryset=activities)
    activities, pagination = paginate(request, queryset=f.qs, per_page=50, default=True)
    context = {
        'activities': activities,
        'pagination': pagination,
        'filter': f,
        'statuses': Activity.STATUSES,
    }
    return render(request, "reminders/follow_ups.html", context)


@login_required
def filterByDropdown(request):
    followUp = request.GET.get('followup')
    enquires = Enquiry.objects.all()
    f = EnquiryFilter(request.GET, queryset=enquires)
    upload_form = UploadFileForm()
    enquiries, pagination = paginate(request, queryset=f.qs, per_page=60, default=True)

    if followUp == 'followup':
        q = enquires.annotate(Count('activities'))
        q = q.filter(status="Interested", activities__count__gte=3)
    if followUp == 'whatsapp':
        q = enquires.filter(status=Enquiry.STATUS_INTERESTED, activities__type__name="Whatsapp")

    if followUp == 'walk_in':
        q = enquires.filter(status=Enquiry.STATUS_INTERESTED, activities__type__name="Walk-In")

    context = {
        'enquiries': q,
        'pagination': pagination,
        'filter': f,
        'count': enquires.count(),
        'upload_form': upload_form,
    }

    return render(request, "enquiry/list.html", context)


@login_required
def walkinList(request):
    enquiries_ids = Activity.objects.filter(type__name='Walk-In').values_list('enquiry__id')
    enquires = Enquiry.objects.filter(Q(campaign__name="Walkin") | Q(id__in=enquiries_ids)).order_by('-created_at')

    f = EnquiryFilter(request.GET, queryset=enquires)
    enquiries, pagination, current_page, per_page = custom_paginate(request, queryset=f.qs, per_page=60, default=True)
    context = {
        'enquiries': enquiries,
        'pagination': pagination,
        'filter': f,
        'count': enquires.count(),
        'current_page': current_page,
        'per_page': per_page
    }
    return render(request, "enquiry/walkinlist.html", context)


def new_registration(request):
    form = NewRegistrationLookupForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        enquiry = Enquiry.objects.filter(
            Q(contact_number=form.cleaned_data['contact']) | Q(contact_number=form.cleaned_data['contact'])).last()
        if enquiry:
            return HttpResponseRedirect(reverse('enquiry:confirm_new_registration', args=[enquiry.id]))
        return HttpResponseRedirect(reverse('enquiry:confirm_new_registration'))

    context = {'form': form}
    return render(request, "enquiry/new_registration_lookup.html", context)


def confirm_new_registration(request, pk=None):
    city_form = CityForm()
    if pk:
        enquiry = get_object_or_404(Enquiry, pk=pk)
        # return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[enquiry.id]))

        form = NewRegistrationForm(request.POST or None, instance=enquiry)
    else:
        form = NewRegistrationForm(request.POST or None)
    if request.method == 'POST':

        if form.is_valid():
            enquiry = form.save()
            # enquiry = Enquiry.objects.filter(
            #     Q(contact_number=form.cleaned_data['contact']) | Q(contact_number=form.cleaned_data['contact'])).last()
            # if enquiry:
            #     return HttpResponseRedirect(reverse('enquiry:edit', args=[enquiry.id]))
            if pk:
                messages.add_message(request, messages.SUCCESS, "Lead Updated!!")
            else:
                messages.add_message(request, messages.SUCCESS, "Lead Created!!")
            return HttpResponseRedirect(reverse('enquiry:new_registration'))

    context = {'enquiry_form': form, 'city_form': city_form, 'pk': pk}
    return render(request, "enquiry/new_registration.html", context)
