from django.contrib import admin
from django.contrib.admin import ModelAdmin
from .models import *

class EnquiryAdmin(ModelAdmin):
    model = Enquiry
    ordering = ('name_of_candidate',)
    list_display = ('name_of_candidate', 'contact_number')
    list_filter = ('email', 'contact_number')

admin.site.register(Enquiry, EnquiryAdmin)
admin.site.register(Activity)
