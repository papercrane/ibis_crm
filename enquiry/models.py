from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from erp_core.models import ErpAbstractBaseModel, BaseCodeSetting
from employee.models import Employee
from erp_core.models import ActivityType


class Enquiry(ErpAbstractBaseModel):

    def newCode():
        return CodeSetting.new_code()

    STATUS_OPEN = 'Open'
    STATUS_INTERESTED = 'Interested'
    STATUS_CLOSED = 'Closed'
    STATUS_JOINED = 'Joined'
    STATUSES = (
        (STATUS_OPEN, "Open"),
        (STATUS_INTERESTED, "Interested"),
        (STATUS_CLOSED, "Closed"),  # Can't add activity
        (STATUS_JOINED, "Joined"),

    )
    code = models.CharField(max_length=20, unique=True, default=newCode)
    source_of_lead = models.CharField(max_length=100, null=True, blank=True)
    date = models.DateField(default=timezone.now)
    employee = models.ForeignKey('employee.Employee', null=True, blank=True,related_name="created_by")
    name_of_candidate = models.CharField(max_length=100)
    contact_number = models.CharField(max_length=20, unique=True)
    location = models.ForeignKey('location.City', null=True, blank=True)
    name_of_college = models.CharField(max_length=255)
    year_of_passout = models.CharField(max_length=7,null=True)
    course = models.ManyToManyField('product.Product', blank=True, related_name="enquiry_courses")
    status = models.CharField(max_length=30, choices=STATUSES, default=STATUS_OPEN, verbose_name='overall_status',
                              null=True, blank=True)
    campaign = models.ForeignKey('crm.Campaign', null=True, blank=True, related_name="enquiry_campaign")
    refered_by_employee = models.ForeignKey('employee.Employee', related_name='refered_by_employee', null=True,
                                            blank=True)
    refered_by_student = models.ForeignKey('student.Student', related_name='refered_by_student', null=True, blank=True)
    special_care = models.BooleanField(default=False)
    remainder_date = models.DateField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    stream = models.TextField(null=True)
    whatsapp_number = models.CharField(max_length=20,null=True)
    assigned_to = models.ForeignKey('employee.Employee', null=True, blank=True, related_name='enquiry_assigned_to')
    interested_course = models.ManyToManyField('product.Batch', blank=True, related_name="enquiry_batches")

    class Meta:
        verbose_name = "Leads"

    def __str__(self):
        return self.name_of_candidate

    def save(self, *args, **kwargs):
        if self.code and self.created_at is None:
            CodeSetting.incrementCountIndex()
        return super(Enquiry, self).save(*args, **kwargs)


class Activity(ErpAbstractBaseModel):
    STATUS_COMPLETED= 'completed'
    STATUS_PENDING= 'pending'
    STATUS_NO_RESPONSE= 'no response'
    STATUSES = (
        (STATUS_COMPLETED, "completed"),
        (STATUS_PENDING, 'pending'),
        (STATUS_NO_RESPONSE, 'no response'),

    )

    date = models.DateField(null=True, blank=True)
    enquiry = models.ForeignKey(Enquiry, on_delete=models.CASCADE, related_name="activities", null=True, blank=True)
    type = models.ForeignKey(ActivityType, on_delete=models.CASCADE,
                             related_name="enquiry_activities")
    created_by = models.ForeignKey(User)
    next_follow_up_date = models.DateField(null=True, blank=True)
    remark = models.TextField(null=True, blank=True)
    assigned_to = models.ForeignKey(Employee, related_name='assigned_to')
    status = models.CharField(max_length=30, choices=STATUSES, default=STATUS_PENDING, blank=True)


class CodeSetting(BaseCodeSetting):
    pass
