from django.db import transaction, IntegrityError
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Count, Q, When, Case, Sum, ExpressionWrapper, F, fields
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages

from enquiry.models import Activity, Enquiry, CodeSetting
from enquiry.forms import EnquiryActivityForm, CodeSettingsForm, UploadFileForm, ActivityTypeForm
from location.models import City
from product.models import Product, Course
from erp_core.models import ActivityType
from employee.models import Employee
from crm.models import Campaign
from datetime import datetime
from student.models import StudentCourse
import re
from django.core.exceptions import ObjectDoesNotExist


# TODO: block editing of send quote


@login_required
@transaction.atomic
# @permission_required('enquiry.add_activity', raise_exception=True)
def create_activity(request, enquiry_pk):
    enquiry = get_object_or_404(Enquiry, id=enquiry_pk)
    activity_type_form = ActivityTypeForm()

    activity_form = EnquiryActivityForm(request.POST or None)
    if request.method == "POST":

        if activity_form.is_valid():
            activity = activity_form.save(commit=False)
            activity.enquiry = enquiry
            activity.created_by = request.user
            activity.save()
            messages.add_message(request, messages.SUCCESS, 'Follow up created successfully')

            return redirect(reverse('enquiry:enquiry_detail', args=[enquiry.id]))

    context = {
        'enquiry': enquiry,
        'activity_form': activity_form,
        'form_url': reverse_lazy('enquiry:add_activity', args=[enquiry.id]),
        'type': 'Add',
        'activity_type_form': activity_type_form
    }
    return render(request, "activity/edit.html", context)


@login_required
@transaction.atomic
@permission_required('enquiry.change_activity', raise_exception=True)
def edit_activity(request, pk):
    activity = get_object_or_404(Activity, id=pk)
    enquiry = activity.enquiry
    activity_form = EnquiryActivityForm(request.POST or None, instance=activity)
    activity_type_form = ActivityTypeForm()
    if request.method == "POST":
        if activity_form.is_valid():
            activity.created_by = request.user
            activity.save()
            messages.add_message(request, messages.SUCCESS, 'Follow up updated successfully')
            return redirect(reverse('enquiry:view_activity', args=[activity.id]))
    context = {
        'enquiry': enquiry,
        'activity_form': activity_form,
        'form_url': reverse_lazy('enquiry:edit_activity', args=[activity.id]),
        'type': 'Edit',
        'activity_type_form': activity_type_form
    }
    return render(request, "activity/edit.html", context)


@login_required
@transaction.atomic
def code_settings(request):
    if CodeSetting.objects.exists():
        code_form = CodeSettingsForm(request.POST or None, instance=CodeSetting.objects.last())
    else:
        code_form = CodeSettingsForm(request.POST or None)

    if request.method == "POST":
        if code_form.is_valid():
            code_form.save()
            return HttpResponseRedirect(reverse('enquiry:list'))

    context = {

        'code_form': code_form,
    }

    return render(request, "enquiry/code_setting.html", context)


@permission_required('enquiry.delete_activity', raise_exception=True)
def delete_activity(request, pk):
    activity = get_object_or_404(Activity, pk=pk)
    activity.delete()
    messages.add_message(request, messages.SUCCESS, 'Follow up deleted successfully')

    return HttpResponseRedirect(reverse('enquiry:enquiry_detail', args=[activity.enquiry.pk]))


def view_activity(request, pk):
    activity = get_object_or_404(Activity, pk=pk)
    context = {
        'activity': activity,
    }
    return render(request, "activity/detail.html", context)


def create_activity_type(request, pk):
    if request.method == "POST":
        activity_type_form = ActivityTypeForm(request.POST)
        activity_type_form.save()
        return HttpResponseRedirect(reverse('enquiry:add_activity', args=[pk]))


@transaction.atomic
@login_required
def import_enquiry(request):
    form = UploadFileForm(request.POST or None, request.FILES or None)
    if request.method == "POST":
        if form.is_valid():
            try:
                file_data = request.FILES['file'].get_records()
                for row in file_data:
                    if not row['SL'] == "":
                        pass
                        if row['CREATED BY EMPLOYEE']:
                            try:
                                employee = Employee.objects.filter(
                                    user__first_name__icontains=row['CREATED BY EMPLOYEE']).first()
                            except ObjectDoesNotExist:
                                employee = None
                        if row['REFERED BY EMPLOYEE']:
                            try:
                                refered_by_employee = Employee.objects.filter(
                                    user__first_name__icontains=row['REFERED BY EMPLOYEE']).first()
                            except ObjectDoesNotExist:
                                refered_by_employee = None
                        if row['EMAIL'] == "" or row['EMAIL'] == " ":
                            row['EMAIL'] = None
                        if row['DATE OF JOINING'] == "" or row['DATE OF JOINING'] == " ":
                            row['DATE OF JOINING'] = str(datetime.today().date())
                        if row['LOCATION'] == "UNKNOWN":
                            pass
                        else:
                            try:
                                location = City.objects.filter(name__icontains=row['LOCATION']).first()
                            except ObjectDoesNotExist:
                                location = None
                                messages.add_message(request, messages.SUCCESS,
                                                     'please add city.The given city not exist')
                        if not row['COURSE'] == "UNKNOWN":
                            try:
                                course = Product.objects.filter(name__icontains=row['COURSE']).first()
                            except ObjectDoesNotExist:
                                course = Product.objects.create(name=row['COURSE'])
                        try:
                            source = Campaign.objects.filter(name__icontains=row['SOURCE']).first()
                        except ObjectDoesNotExist:
                            source = None
                        if row['DATE OF JOINING'] == "" or row['DATE OF JOINING'] == " ":
                            row['DATE OF JOINING'] = None
                        if row['FOLLOW UP 1 DATE'] == "" or row['FOLLOW UP 1 DATE'] == " ":
                            row['FOLLOW UP 1 DATE'] = None
                        if row['FOLLOW UP 2 DATE'] == "" or row['FOLLOW UP 2 DATE'] == " ":
                            row['FOLLOW UP 2 DATE'] = None
                        if row['FOLLOW UP 3 DATE'] == "" or row['FOLLOW UP 3 DATE'] == " ":
                            row['FOLLOW UP 3 DATE'] = None
                        enquiry = Enquiry.objects.create(location=location,
                                                         name_of_candidate=row['NAME OF CANDIDATE'],
                                                         contact_number=row['CONTACT NUMBER'],
                                                         whatsapp_number=row['WHATSAPP NUMBER'],
                                                         name_of_college=row['NAME OF THE COLLEGE'],
                                                         campaign=source,
                                                         employee=employee,
                                                         status=(row['OVERALL STATUS']).capitalize(),
                                                         year_of_passout=row['YEAR OF PASS OUT'],
                                                         date=row['DATE OF JOINING'],
                                                         stream=row['STREAM'],
                                                         refered_by_employee=refered_by_employee,
                                                         special_care=False,
                                                         email=row['EMAIL'])
                        if enquiry:
                            if course:
                                enquiry.course.add(course)
                                type = row['FOLLOW UP 2 TYPE']
                                activity_type = ActivityType.objects.filter(name__icontains=str(type)).first()

                            if row['FOLLOW UP 1 DATE']:
                                Activity.objects.create(enquiry=enquiry, date=row['FOLLOW UP 1 DATE'],
                                                        remark=row['FOLLOW UP 1 REMARKS'], complete=True,
                                                        type=activity_type, assigned_to=row['FOLLOW UP 1 ASSIGNED TO'])
                            if row['FOLLOW UP 2 DATE']:
                                Activity.objects.create(enquiry=enquiry, date=row['FOLLOW UP 2 DATE'],
                                                        remark=row['FOLLOW UP 2 REMARKS'], complete=True,
                                                        type=activity_type, assigned_to=row['FOLLOW UP 2 ASSIGNED TO'])
                            if row['FOLLOW UP 3 DATE']:
                                Activity.objects.create(enquiry=enquiry, date=row['FOLLOW UP 3 DATE'],
                                                        remark=row['FOLLOW UP 3 REMARKS'], complete=True,
                                                        type=activity_type, assigned_to=row['FOLLOW UP 3 ASSIGNED TO'])
            except IntegrityError:
                messages.add_message(request, messages.ERROR,
                                     'Student named ' + str(row['NAME OF CANDIDATE']) + ' with contact number ' + str(
                                         row['CONTACT NUMBER']) + ' is already added.')

    return redirect("enquiry:list")


@login_required
def enrolment_report(request):
    if request.GET.get('month_year'):
        date_for_check = re.findall('\d{4}-\d{2}-\d{2}', str(request.GET.get('month_year')))
    else:
        date_for_check = [str(datetime.today().date()), str(datetime.today().date())]

    products = Course.objects.filter(students__date_of_joining__range=date_for_check)

    closed_admission = Count(Case(When(students__status=StudentCourse.STATUS_DISCONTINUED, then=1)))

    products = products.annotate(course=F('batch__name'),
                                 total_admission=Count('students'), closed_admission=closed_admission).annotate(
        current_admission=ExpressionWrapper(F('total_admission') - F('closed_admission'),
                                            output_field=fields.PositiveIntegerField()))
    total_sum = products.aggregate(Sum('total_admission'), Sum('closed_admission'), Sum('current_admission'))
    products = products.values('course', 'total_admission', 'closed_admission', 'current_admission').order_by('course')

    context = {
        'products': products,
        'total_sum': total_sum,
        'date_for_check': date_for_check,
        'start': datetime.strptime(date_for_check[0], '%Y-%m-%d'),
        'end': datetime.strptime(date_for_check[1], '%Y-%m-%d')

    }
    return render(request, 'enquiry/enrolment_report.html', context)


@login_required
def change_status(request, pk):
    activity = get_object_or_404(Activity, pk=pk)
    activity.status = request.POST.get("status")
    activity.save()
    return redirect(reverse('enquiry:enquiry_detail', args=[activity.enquiry.id]))


@login_required
def enquiry_change_status(request, pk):
    enquiry = get_object_or_404(Enquiry, pk=pk)
    enquiry.status = request.POST.get("status")
    enquiry.save()
    return redirect(reverse('enquiry:enquiry_detail', args=[enquiry.enquiry.id]))


@login_required
def enrolment_report_daily_report(request):
    date_check = request.GET.get('month_year')

    if date_check:
        date_for_check = datetime.strptime(date_check, "%Y-%m-%d").date()
    else:
        date_for_check = datetime.today().date()

    employees = Employee.objects.filter(primary_designation__name="Tele-Caller",
                                        created_by__date=date_for_check).annotate(total_leads=Count('created_by'))

    context = {
        'employees': employees,
        'date_for_check': date_for_check,
    }

    return render(request, 'enquiry/daily_enrolment_report.html', context)
