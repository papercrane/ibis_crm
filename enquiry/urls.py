from django.conf.urls import url
from enquiry import views, enquiry


app_name = "enquiry"

urlpatterns = [
    url(r'^enquiries/$', enquiry.list, name="list"),
    url(r'^enquiries/lead/$', enquiry.mylist, name="mylist"),
    url(r'^enquiries/walkins/$', enquiry.walkinList, name="walkinList"),
    url(r'^enquiries/create/$', enquiry.create, name="create"),
    url(r'^enquiries/code_settings/$', views.code_settings, name='code_settings'),
    url(r'^enquiries/(?P<pk>[^/]+)/delete/$', enquiry.delete, name="delete"),
    url(r'^enquiries/pending_follow_ups/$', enquiry.pending_follow_ups, name="pending_follow_ups"),
    url(r'^enquiry/(?P<pk>[^/]+)/details/$', enquiry.enquiry_detail, name="enquiry_detail"),
    url(r'^enquiry/(?P<pk>[^/]+)/edit/$', enquiry.edit, name="edit"),
    url(r'^enquiry/activity/(?P<pk>[^/]+)/complete/$', enquiry.complete_activity, name="complete_activity"),
    url(r'^registration/$', enquiry.new_registration, name="new_registration"),
    url(r'^confirm/$', enquiry.confirm_new_registration, name="confirm_new_registration"),
    url(r'^confirm/(?P<pk>[^/]+)/$', enquiry.confirm_new_registration, name="confirm_new_registration"),

    # Activity urls
    url(r'^enquiries/(?P<enquiry_pk>[^/]+)/activities/create/$', views.create_activity, name="add_activity"),
    url(r'^activity/(?P<pk>[^/]+)/edit/$', views.edit_activity, name="edit_activity"),
    url(r'^activity/(?P<pk>[^/]+)/delete/$', views.delete_activity, name="delete_activity"),
    url(r'^activity/(?P<pk>[^/]+)/view/$', views.view_activity, name="view_activity"),
    url(r'^enquiries/(?P<pk>[^/]+)/activity_type/$', views.create_activity_type, name="create_activity_type"),
    url(r'^activity/(?P<pk>[^/]+)/change_status/$', views.change_status, name="change_status"),
    url(r'^activity/(?P<pk>[^/]+)/enquiry_change_status/$', views.enquiry_change_status, name="enquiry_change_status"),
    url(r'^import/$', views.import_enquiry, name="import"),
    url(r'^followups/$', enquiry.myfollowups, name="myfollowups"),
    url(r'^enrollment/report/$', views.enrolment_report, name='enrolment_report'),
    url(r'^enrollment/daily/report/$', views.enrolment_report_daily_report, name='enrolment_report_daily_report'),

]