import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// import EditEnquiry from './components/enquiry/edit.vue'
import EnquiryList from './components/enquiry/list.vue'
// import EnquiryDetail from './components/enquiry/detail.vue'

const EditEnquiry = resolve => {
	require.ensure(['./components/enquiry/edit.vue'], () => {
		resolve(require('./components/enquiry/edit.vue'))
	})
}

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * @param  {string}   name     the filename (basename) of the view to load.
 */
// function view(name) {
//     return function(resolve) {
//         require(['./views/' + name + '.vue'], resolve);
//     }
// };

const routes = [
	{ path: '/enquiries', component: EnquiryList, name: 'enquiries' },
	{ path: '/enquiries/create', component: EditEnquiry, name: 'create-enquiry' },
	{ path: '/enquiries/:id', component: System.import('./components/enquiry/detail.vue'), name: 'enquiry' },
	// { path: '/:id', component: EnquiryDetail }
]

export const router = new VueRouter({
	mode: 'history',
	base: '/',
	routes // short for routes: routes
})

// export { router }