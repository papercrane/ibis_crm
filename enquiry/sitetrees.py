from sitetree.utils import item, tree

# Be sure you defined `sitetrees` in your module.
sitetrees = (
    # Define a tree with `tree` function.
    tree('sidebar', items=[
        # Then define items and their children with `item` function.
        item(
            'Lead',
            'enquiry:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="question-circle-o",

        ),

    ]),
    tree('sidebar_ibis_admin', items=[
        # Then define items and their children with `item` function.
        item(
            'Lead',
            'enquiry:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="question-circle-o",

        ),

    ]),


    tree('sidebar_branch_head', items=[

        item(
            'Lead',
            'enquiry:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="question-circle-o",
            # access_by_perms=['project.view_project_list'],

        ),
    ]),

    tree('sidebar_marketing_head', items=[
        # Then define items and their children with `item` function.
        item(
            'Lead',
            'enquiry:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="question-circle-o",
        ),
        item(
            'Pending Follow Ups',
            'enquiry:pending_follow_ups',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="exclamation-triangle",
        ),
        item(
            'My Follow ups',
            'enquiry:myfollowups',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="comments",
        ),
        item(
            'Walk-ins',
            'enquiry:walkinList',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="male",
        ),
    ]),

    tree('sidebar_director', items=[

        item(
            'Lead',
            'enquiry:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="question-circle-o",
            # access_by_perms=['project.view_project_list'],

        ),
    ]),



    tree('sidebar_tele_caller', items=[
        item(
            'Lead',
            'enquiry:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="question-circle-o",
        ),
        item(
            'My Follow up',
            'enquiry:myfollowups',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="comments",
        ),
    ]),

    tree('sidebar_telecaller_head', items=[
        item(
            'Lead',
            'enquiry:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="question-circle-o",
            # access_by_perms=['project.view_project_list'],

        ),
    ]),

    # ... You can define more than one tree for your app.
)
