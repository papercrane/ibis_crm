# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-11-29 09:57
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('enquiry', '0006_merge_20181129_1132'),
        ('enquiry', '0006_auto_20181129_1004'),
    ]

    operations = [
    ]
