import re
from decimal import Decimal

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models.functions import ExtractMonth

from student.models import StudentPayment
from .forms import *
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse, reverse_lazy
from erp_core.pagination import paginate
import datetime
from django.forms import modelformset_factory
from .models import *
from .filters import *
from django.db.models import Sum, Count
from datetime import date, timedelta
from django.db.models.functions import TruncDay
from crm.models import Campaign


# # def add_SalesReimbursement(request, pk):
# #     user = request.user.employee
# #     form = SalesReimbursementForm(
# #         request.POST or None, request.FILES or None, current_user=user)
# #     if request.method == "POST":
# #         if form.is_valid():
# #             salesreimbursementExpenseConfig = form.save(commit=False)
# #             salesreimbursement.employee = get_object_or_404(Employee, id=pk)
# #             salesreimbursement.save()
# #             return HttpResponseRedirect(reverse('expense:view_SalesReimbursement', args=[pk]))
# #     context = {
# #         "form": form,
# #         'form_url': reverse_lazy('expense:add_SalesReimbursement', args=[pk]),
# #         "pk": pk,
# #         "type": "add"
# #     }
# #     return render(request, 'SalesReimbursement/add_SalesReimbursement.html', context)


# # def view_SalesReimbursement(request, pk):
# #     salesreimbursements = SalesReimbursement.objects.filter(employee_id=pk).order_by("-created_at")
# #     salesreimbursements, pagination = paginate(request, queryset=salesreimbursements, per_page=10, default=True)
# #     context = {
# #         "salesreimbursements": salesreimbursements,
# #         "pk": pk,
# #         'pagination': pagination
# #     }
# #     return render(request, 'SalesReimbursement/view_SalesReimbursement.html', context)


# # def edit_SalesReimbursement(request, pk, salesreimbursement_pk):

# #     # target = get_object_or_404(Targets, id=target_pk)
# #     user = request.user.employee
# #     salesreimbursement = SalesReimbursement.objects.get(
# #         id=salesreimbursement_pk)
# #     form = SalesReimbursementForm(
# #         request.POST or None, request.FILES or None, instance=salesreimbursement, current_user=user)
# #     if request.method == "POST":
# #         form.save()
# #         return HttpResponseRedirect(reverse('expense:view_SalesReimbursement', args=[pk]))
# #     context = {
# #         "form": form,
# #         'form_url': reverse_lazy('expense:edit_SalesReimbursement', args=[pk, salesreimbursement_pk]),
# #         "pk": pk,
# #         "salesreimbursement_pk": salesreimbursement_pk,
# #         "type": "edit"
# #     }
# #     return render(request, 'SalesReimbursement/add_SalesReimbursement.html', context)


# # def detail(request, pk):
# #     # quotation = get_object_or_404(Quotation, id=pk)
# #     salesreimbursements = SalesReimbursement.objects.get(id=pk)
# #     context = {
# #         "salesreimbursement": salesreimbursements,
# #         "pk": pk
# #     }
# #     return render(request, "SalesReimbursement/detail.html", context)


# # def approval_SalesReimbursement(request, pk):
# #     date = datetime.datetime.now()

# #     salesreimbursement = SalesReimbursement.objects.get(id=pk)
# #     if salesreimbursement.employee.reporting_officer == request.user.employee:
# #         salesreimbursement.approved_by = request.user.employee
# #         salesreimbursement.approved_date = date

# #         salesreimbursement.save()

# #         month_year = date.strftime("%B %Y")

# #         try:
# #             salary = MonthlySalary.objects.get(paid=False, employee=request.user.employee, month_year=month_year)
# #         except Exception as e:
# #             messages.add_message(request, messages.INFO, e)
# #             return HttpResponseRedirect(reverse('expense:detail', args=[pk]))
# #         salary.sales_reimbursement = salary.sales_reimbursement + salesreimbursement.amount
# #         salary.amount = salary.amount + salesreimbursement.amount
# #         salary.save()

# #     else:
# #         # messages.info(request, 'No Permission to approve')
# #         messages.add_message(request, messages.INFO, 'No Permission to approve')
# #     return HttpResponseRedirect(reverse('expense:detail', args=[pk]))


# # def view_approvalSalesReimbursement(request):
# #     salesreimbursements = SalesReimbursement.objects.filter(
# #         employee__reporting_officer=request.user.employee).distinct().order_by("-created_at")
# #     salesreimbursements, pagination = paginate(request, queryset=salesreimbursements, per_page=20, default=True)
# #     context = {
# #         "salesreimbursements": salesreimbursements,
# #         # "pk": pk,
# #         'pagination': pagination
# #     }
# #     return render(request, 'SalesReimbursement/view_SalesReimbursement.html', context)


# def record_expense_create(request, payee_type):

#     expense_form = ExpenseForm(request.POST or None, request.FILES or None)
#     payee_form = PayeeForm(request.POST or None, payee_type=payee_type)

#     if request.method == "POST":
#         if expense_form.is_valid() and payee_form.is_valid():
#             expense = expense_form.save(commit=False)
#             expense.payee = payee_form.cleaned_data['payee']
#             expense.save()
#             expense_form.save_m2m()
#             return HttpResponseRedirect(reverse('expense:view_expense', args=[expense.pk]))
#     context = {
#         "expense_form": expense_form,
#         'form_url': reverse_lazy('expense:record_expense_create', args=[payee_type]),
#         "type": "add",
#         "payee_form": payee_form,
#     }
#     return render(request, 'expense/record_expense_create.html', context)


# def bulk_expense_create(request, payee_type):

#     ExpenseFormSet = modelformset_factory(Expense, form=ExpenseFormsetForm, extra=0, min_num=2, max_num=100, can_delete=True)
#     expense_formset = ExpenseFormSet(queryset=Expense.objects.none())
#     payee_form = PayeeForm(request.POST or None, payee_type=payee_type)

#     if request.method == "POST":
#         expense_formset = ExpenseFormSet(request.POST)
#         if expense_formset.is_valid() and payee_form.is_valid():
#             # expense.payee = payee_form.cleaned_data['payee']
#             expenses = expense_formset.save(commit=False)
#             for expense in expenses:
#                 expense.payee = payee_form.cleaned_data['payee']
#                 expense.save()

#             return HttpResponseRedirect(reverse('expense:index'))

#     context = {
#         "expense_formset": expense_formset,
#         'form_url': reverse_lazy('expense:bulk_expense_create', args=[payee_type]),
#         "type": "add",
#         "payee_form": payee_form,
#     }
#     return render(request, 'expense/bulk_expense_create.html', context)


# def index(request, payee_type=None):

#     f = ExpenseFilter(request.GET, request=request, queryset=Expense.objects.all().order_by("-created_at"))
#     # expenses = Expense.objects.all().order_by("-created_at")

#     expenses, pagination = paginate(request, queryset=f.qs, per_page=10, default=True)
#     # print("expenses...........", expenses)
#     context = {
#         'expenses': f.qs,
#         'pagination': pagination,
#         'filter': f,
#     }

#     return render(request, "expense/list.html", context)


# def view_expense(request, pk):
#     expense = Expense.objects.get(id=pk)
#     context = {
#         "expense": expense,
#         "pk": pk
#     }
#     return render(request, "expense/details.html", context)


# def edit_Expense(request, pk):
#     expense = Expense.objects.get(id=pk)
#     expense_form = ExpenseForm(request.POST or None, request.FILES or None, instance=expense)
#     if request.method == "POST":
#         expense_form.save()
#         return HttpResponseRedirect(reverse('expense:view_expense', args=[pk]))
#     context = {
#         "expense_form": expense_form,
#         "expense": expense,
#         'form_url': reverse_lazy('expense:edit_Expense', args=[pk]),
#         "pk": pk,
#         "type": "edit"
#     }
#     return render(request, 'expense/record_expense_create.html', context)


# def dashboard(request):
#     return render(request, "expense/dashboard.html")


# def expense_type_list(request):
#     expense_name = request.GET.get('expense_name', False)
#     if expense_name:
#         expense = TypeOfExpense.objects.filter(
#             name__icontains=expense_name
#         ).annotate(
#             text=F('name')
#         ).values('id', 'name')

#         return JsonResponse(list(expense), safe=False)
#     return JsonResponse(list(), safe=False)


# def delete(request, pk):
#     if request.method == "POST":
#         expense = get_object_or_404(Expense, id=pk)
#         expense.delete()
#     return HttpResponseRedirect(reverse('expense:index'))


# def supplier_or_vendor(request, bulk=False):
#     form = ExpenserForm(request.POST or None)
#     if bulk:
#         form_url = reverse('expense:bulk_supplier_or_vendor')
#     else:
#         form_url = reverse('expense:supplier_or_vendor')

#     if request.method == 'POST':
#         if form.is_valid():
#             payee_type = form.cleaned_data['payee_type']
#             if bulk:
#                 return redirect(reverse("expense:bulk_expense_create", args=[payee_type]))
#             else:
#                 return redirect(reverse("expense:record_expense_create", args=[payee_type]))

#     context = {
#         'form': form,
#         'form_url': form_url,
#     }
#     return render(request, "expense/supplier_or_vendor.html", context)

@login_required
def dashboard(request):
    return render(request, "expense/dashboard.html")


@login_required
def expense_index(request, payee_type=None):
    f = ExpenseFilter(request.GET, request=request,
                      queryset=Expense.objects.filter(income_or_expense=True).order_by("-created_at"))
    # expenses = Expense.objects.all().order_by("-created_at")

    expenses, pagination = paginate(request, queryset=f.qs, per_page=10, default=True)
    # print("expenses...........", expenses)
    context = {
        'expenses': expenses,
        'pagination': pagination,
        'filter': f,
    }

    return render(request, "expense/list.html", context)


@login_required
def edit_Expense(request, pk):
    expense = Expense.objects.get(id=pk)
    expense_form = ExpenseForm(request.POST or None, request.FILES or None, instance=expense)
    if request.method == "POST":
        expense_form.save()
        return HttpResponseRedirect(reverse('expense:expense_index'))
    context = {
        "expense_form": expense_form,
        "expense": expense,
        'form_url': reverse_lazy('expense:edit_Expense', args=[pk]),
        "pk": pk,
        "type": "edit"
    }

    return render(request, 'expense/single_expense_create.html', context)


@login_required
def expense_create(request):
    ExpenseFormSet = modelformset_factory(Expense, form=ExpenseFormsetForm, extra=0,
                                          min_num=1, max_num=100, can_delete=True)
    expense_formset = ExpenseFormSet(queryset=Expense.objects.none())

    income = Account.objects.filter(Q(head__name="Capital") | Q(head__name='Undeposited Funds')).aggregate(
        income_total=Sum('amount'))
    expense = Expense.objects.filter(income_or_expense=True).aggregate(expense_total=Sum('amount'))

    if not income['income_total']:
        income['income_total'] = 0

    if not expense['expense_total']:
        expense['expense_total'] = 0

    balance_amount = income['income_total'] - expense['expense_total']

    if request.method == "POST":
        expense_formset = ExpenseFormSet(request.POST)
        if expense_formset.is_valid():
            # expense.payee = payee_form.cleaned_data['payee']
            expenses = expense_formset.save()
            # for expense in expenses:
            # 	expense.payee = payee_form.cleaned_data['payee']
            # 	expense.save()

            return HttpResponseRedirect(reverse('expense:expense_index'))

    context = {
        "expense_formset": expense_formset,
        'form_url': reverse_lazy('expense:expense_create'),
        "type": "add",
        'balance_amount': balance_amount
    }
    return render(request, 'expense/expense_create.html', context)


@login_required
def income_index(request, payee_type=None):
    f = ExpenseFilter(request.GET, request=request,
                      queryset=Expense.objects.filter(income_or_expense=False).order_by("-created_at"))
    # expenses = Expense.objects.all().order_by("-created_at")

    expenses, pagination = paginate(request, queryset=f.qs, per_page=10, default=True)
    # print("expenses...........", expenses)
    context = {
        'expenses': expenses,
        'pagination': pagination,
        'filter': f,
    }

    return render(request, "expense/income_list.html", context)


@login_required
def edit_income(request, pk):
    expense = Expense.objects.get(id=pk)
    expense_form = IncomeForm(request.POST or None, request.FILES or None, instance=expense)
    if request.method == "POST":
        expense_form.save()
        return HttpResponseRedirect(reverse('expense:income_index'))
    context = {
        "expense_form": expense_form,
        "expense": expense,
        'form_url': reverse_lazy('expense:edit_income', args=[pk]),
        "pk": pk,
        "type": "edit"
    }
    return render(request, 'expense/single_income_create.html', context)


@login_required
def income_create(request):
    ExpenseFormSet = modelformset_factory(Expense, form=IncomeFormsetForm, extra=0,
                                          min_num=1, max_num=100, can_delete=True)
    expense_formset = ExpenseFormSet(queryset=Expense.objects.none())

    if request.method == "POST":
        expense_formset = ExpenseFormSet(request.POST)
        if expense_formset.is_valid():
            # expense.payee = payee_form.cleaned_data['payee']
            expenses = expense_formset.save()
            for expense in expenses:
                expense.income_or_expense = False
                expense.save()

            return HttpResponseRedirect(reverse('expense:income_index'))

    context = {
        "expense_formset": expense_formset,
        'form_url': reverse_lazy('expense:income_create'),
        "type": "add",
    }
    return render(request, 'expense/income_create.html', context)


@login_required
def report_dash(request):
    return render(request, 'expense/reportdashboard.html')


@login_required
def expense_income_report(request):
    if request.GET.get('month_year'):
        date_for_check = re.findall('\d{4}-\d{2}-\d{2}', str(request.GET.get('month_year')))
    else:
        date_for_check = [str(datetime.today().date()), str(datetime.today().date())]

    # date_check = request.GET.get('month_year')
    current_month_and_year = datetime.strptime(date_for_check[0], "%Y-%m-%d").date()

    accounts = Account.objects.filter(Q(head__name="Undeposited Funds") | Q(head__name="Capital"))
    expenses = Expense.objects.filter(income_or_expense=True)
    # previous month balance calculation
    if current_month_and_year.month == 1:
        prev_incomes = accounts.filter(date__year=current_month_and_year.year - 1).aggregate(Sum('amount'))
        prev_expenses = expenses.filter(date__year=current_month_and_year.year - 1).aggregate(Sum('amount'))

    else:
        prev_incomes = accounts.exclude(
            date__month=current_month_and_year.month, date__year=current_month_and_year.year).aggregate(Sum('amount'))
        prev_expenses = expenses.exclude(date__month=current_month_and_year.month,
                                         date__year=current_month_and_year.year).aggregate(
            Sum('amount'))
    if not prev_incomes['amount__sum']:
        prev_incomes['amount__sum'] = 0.00

    if not prev_expenses['amount__sum']:
        prev_expenses['amount__sum'] = 0.00

    if prev_expenses['amount__sum'] < prev_incomes['amount__sum']:
        prev_bal = prev_incomes['amount__sum'] - prev_expenses['amount__sum']
    else:
        prev_bal = 0

    # else:
    #     date_for_check = datetime.today()
    #     tdate = datetime.today().date()
    #     crnt_date_for_check = date_for_check

    # prev = date_for_check[0]
    #
    selected_date_incomes = accounts.filter(date__range=date_for_check)
    selected_date_expenses = expenses.filter(date__range=date_for_check)
    if selected_date_expenses and selected_date_incomes:
        selected_date_total_expense = selected_date_expenses.aggregate(Sum('amount'))['amount__sum']
        selected_date_total_income = selected_date_incomes.aggregate(Sum('amount'))['amount__sum']
    elif selected_date_expenses:
        selected_date_total_expense = selected_date_expenses.aggregate(Sum('amount'))['amount__sum']
        selected_date_total_income = 0.00
    elif selected_date_incomes:
        selected_date_total_income = selected_date_incomes.aggregate(Sum('amount'))['amount__sum']
        selected_date_total_expense = 0.00
    else:
        selected_date_total_expense = 0.00
        selected_date_total_income = 0.00

    total_selected_date_income = Decimal(selected_date_total_income) + Decimal(prev_bal)
    selected_date_balance = total_selected_date_income - Decimal(selected_date_total_expense)

    if accounts and expenses:
        total_income = accounts.aggregate(Sum('amount'))['amount__sum']
        total_expense = expenses.aggregate(Sum('amount'))['amount__sum']
    elif accounts:
        total_income = accounts.aggregate(Sum('amount'))['amount__sum']
        total_expense = 0.00
    elif expenses:
        total_expense = expenses.aggregate(Sum('amount'))['amount__sum']
        total_income = 0.00

    else:
        total_expense = 0.00
        total_income = 0.00

    current_balance = Decimal(total_income) - Decimal(total_expense)

    start = datetime.strptime(date_for_check[0], '%Y-%m-%d')
    first = start.replace(day=1)
    if start.month > 1:
        prev_month = start.replace(month=start.month - 1, day=1)
    else:
        prev_month = start.replace(month=12, year=start.year - 1)
    context = {

        'incomes': selected_date_incomes,
        'expenses': selected_date_expenses,
        'current_balance': current_balance,
        'balance': selected_date_balance,
        'date_for_check': date_for_check,
        'start': start,
        'end': datetime.strptime(date_for_check[1], '%Y-%m-%d'),
        'prev_bal': prev_bal,
        'prev_month': prev_month,
        'first': first,
        'total_selected_date_income': total_selected_date_income,
        'selected_date_total_expense': selected_date_total_expense
    }
    return render(request, 'expense/report.html', context)


@login_required
def account_report(request):
    if request.GET.get('month_year'):
        date_for_check = re.findall('\d{4}-\d{2}-\d{2}', str(request.GET.get('month_year')))
    else:
        date_for_check = [str(datetime.today().date()), str(datetime.today().date())]

    funds = AccountHead.objects.filter(accounts__date__range=date_for_check) \
        .annotate(month=ExtractMonth('accounts__date'), total_amount=Sum('accounts__amount')) \
        .values('name', 'total_amount', 'month').order_by('month')
    context = {
        'date_for_check': date_for_check,
        'funds': funds,
        'start': datetime.strptime(date_for_check[0], '%Y-%m-%d'),
        'end': datetime.strptime(date_for_check[1], '%Y-%m-%d'),

    }

    return render(request, 'expense/account_report.html', context)


@login_required
def account_create(request):
    account_form = AccountForm(request.POST or None)

    income = Expense.objects.filter(income_or_expense=False).aggregate(income_total=Sum('amount'))
    account = Account.objects.aggregate(account_total=Sum('amount'))

    total_account = account['account_total']
    total_income = income['income_total']

    if total_income == None:
        total_income = 0
    if total_account == None:
        total_account = 0
    dif = total_income - total_account

    if request.method == "POST":
        if account_form.is_valid():
            account_form.save()

            return HttpResponseRedirect(reverse('expense:account_list'))

    context = {

        "type": "add",
        'form_url': reverse_lazy('expense:account_create'),
        'account_form': account_form,
        'dif': dif

    }
    return render(request, 'expense/account_create.html', context)


@login_required
def account_list(request):
    f = AccountFilter(request.GET, request=request, queryset=Account.objects.all().order_by("-created_at"))

    accounts, pagination = paginate(request, queryset=f.qs, per_page=10, default=True)

    context = {
        'accounts': accounts,
        'pagination': pagination,
        'filter': f,
    }

    return render(request, 'expense/account_list.html', context)


@login_required
def transaction_report(request):
    if request.GET.get('month_year'):
        date_for_check = re.findall('\d{4}-\d{2}-\d{2}', str(request.GET.get('month_year')))
    else:
        date_for_check = [str(datetime.today().date()), str(datetime.today().date())]

    accounts = AccountHead.objects.filter(accounts__date__range=date_for_check).annotate(
        total_sum=Sum('accounts__amount')).order_by('-accounts__head').exclude(name="Undeposited Funds")
    total_accounts = accounts.aggregate(total_accounts=Sum('total_sum'))

    context = {
        'date_for_check': date_for_check,
        'accounts': accounts,
        'total_accounts': total_accounts,
        'start': datetime.strptime(date_for_check[0], '%Y-%m-%d'),
        'end': datetime.strptime(date_for_check[1], '%Y-%m-%d')

    }
    return render(request, 'expense/fund_accqusition_report.html', context)


@login_required
def cash_breakup_report(request):
    if request.GET.get('month_year'):
        date_for_check = re.findall('\d{4}-\d{2}-\d{2}', str(request.GET.get('month_year')))
    else:
        date_for_check = [str(datetime.today().date()), str(datetime.today().date())]

    accounts = AccountHead.objects.filter(accounts__date__range=date_for_check) \
        .annotate(day=TruncDay('accounts__date'), total_amount=Sum('accounts__amount')) \
        .values('name', 'total_amount', 'day').order_by('-day').exclude(name="Undeposited Funds")

    context = {
        'date_for_check': date_for_check,
        'accounts': accounts,
        'start': datetime.strptime(date_for_check[0], '%Y-%m-%d'),
        'end': datetime.strptime(date_for_check[1], '%Y-%m-%d'),

    }

    return render(request, 'expense/cash_breakup.html', context)


@login_required
def cash_received_report(request):
    if request.GET.get('month_year'):
        date_for_check = re.findall('\d{4}-\d{2}-\d{2}', str(request.GET.get('month_year')))
    else:
        date_for_check = [str(datetime.today().date()), str(datetime.today().date())]

    payments = StudentPayment.objects.filter(date__range=date_for_check).annotate(day=TruncDay('date'),
                                                                                  total_amount=Sum('amount')).values(
        'total_amount', 'day').order_by('-day')

    context = {
        'date_for_check': date_for_check,
        'start': datetime.strptime(date_for_check[0], '%Y-%m-%d'),
        'end': datetime.strptime(date_for_check[1], '%Y-%m-%d'),
        'payments': payments,
    }

    return render(request, 'expense/cash_received.html', context)


@login_required
def income_received_report(request):
    if request.GET.get('month_year'):
        date_for_check = re.findall('\d{4}-\d{2}-\d{2}', str(request.GET.get('month_year')))
    else:
        date_for_check = [str(datetime.today().date()), str(datetime.today().date())]

    payments = Expense.objects.filter(date__range=date_for_check).annotate(day=TruncDay('date'),
                                                                           total_amount=Sum('amount')).values(
        'total_amount', 'day').order_by('-day')

    context = {
        'date_for_check': date_for_check,
        'start': datetime.strptime(date_for_check[0], '%Y-%m-%d'),
        'end': datetime.strptime(date_for_check[1], '%Y-%m-%d'),
        'payments': payments,
    }

    return render(request, 'expense/cash_received.html', context)


@login_required
def account_edit(request, pk):
    account_ob = get_object_or_404(Account, pk=pk)
    account_form = AccountForm(request.POST or None, instance=account_ob)

    income = Expense.objects.filter(income_or_expense=False).aggregate(income_total=Sum('amount'))
    account = Account.objects.aggregate(account_total=Sum('amount'))

    total_account = account['account_total']
    total_income = income['income_total']

    if total_income == None:
        total_income = 0
    if total_account == None:
        total_account = 0
    dif = total_income - total_account

    if request.method == "POST":
        if account_form.is_valid():
            account_form.save()

            return HttpResponseRedirect(reverse('expense:account_list'))

    context = {

        "type": "add",
        'form_url': reverse_lazy('expense:account_edit', args=[pk]),
        'account_form': account_form,
        'dif': dif

    }
    return render(request, 'expense/account_create.html', context)


@login_required
def undeposited_fund_create(request):
    account_form = AccountForm(request.POST or None)

    income = Expense.objects.filter(income_or_expense=False).aggregate(income_total=Sum('amount'))
    account = Account.objects.aggregate(account_total=Sum('amount'))

    total_account = account['account_total']
    total_income = income['income_total']

    if total_income == None:
        total_income = 0
    if total_account == None:
        total_account = 0
    dif = total_income - total_account

    if request.method == "POST":
        if account_form.is_valid():
            account_form.save()

            return HttpResponseRedirect(reverse('expense:undeposited_fund_create'))

    context = {

        "type": "add",
        'form_url': reverse_lazy('expense:account_create'),
        'account_form': account_form,
        'dif': dif

    }
    return render(request, 'expense/undeposited_fund_create.html', context)
