from django.apps import AppConfig


class ExpenseConfig(AppConfig):
    name = 'expense'

    def ready(self):
        pass
        # from expense.models import AccountHead, IncomeType
        # AccountHead.objects.update_or_create(name="Handover To Concerned Person")
        # AccountHead.objects.update_or_create(name="Bank Deposit")
        # AccountHead.objects.update_or_create(name="Capital")
        # IncomeType.objects.update_or_create(name="Fee")
