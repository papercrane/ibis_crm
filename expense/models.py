from django.db import models
from erp_core.models import ErpAbstractBaseModel
from django.core.validators import MinValueValidator
from student.models import StudentInstallment


class ExpenseType(ErpAbstractBaseModel):
    name = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name


class IncomeType(ErpAbstractBaseModel):
    name = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name


class Expense(ErpAbstractBaseModel):
    expense_type = models.ForeignKey(ExpenseType, null=True, blank=True)
    income_type = models.ForeignKey(IncomeType, null=True, blank=True)
    description = models.CharField(max_length=255, null=True, blank=True)
    amount = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(0)])
    income_or_expense = models.BooleanField(default=True)
    date = models.DateField(null=True, blank=True)
    voucher_number = models.CharField(max_length=200, blank=True)
    installment = models.ForeignKey(StudentInstallment,null=True,blank=True)

    def __str__(self):
        if self.expense_type:
            return str(self.expense_type) + "-" + str(self.date) + "-" + str(self.amount)
        else:
            return str(self.income_type) + "-" + str(self.date) + "-" + str(self.amount)






class AccountHead(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Account(ErpAbstractBaseModel):
    head = models.ForeignKey(AccountHead,related_name="accounts")
    description = models.CharField(max_length=255, null=True, blank=True)
    amount = models.DecimalField(max_digits=20, decimal_places=2, validators=[MinValueValidator(0)])
    date = models.DateField()
