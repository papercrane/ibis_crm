import django_filters
from django import forms
from django.db.models import Q
from .models import Expense, ExpenseType,IncomeType,Account
from datetime import datetime


class ExpenseFilter(django_filters.FilterSet):
    date = django_filters.DateRangeFilter(label="Date",
                                          widget=forms.TextInput(attrs={'class': 'form-control tdate'}))

    amount = django_filters.NumberFilter(label='Amount', widget=forms.NumberInput(attrs={'class': 'form-control'}))

    expense_type = django_filters.ModelChoiceFilter(
        queryset=ExpenseType.objects.all(),
        method='filter_by_expense_type',
        name="media",
        widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;", }),
        label="Media"
    )
    income_type = django_filters.ModelChoiceFilter(
        queryset=IncomeType.objects.all(),
        method='filter_by_income_type',
        name="media",
        widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;", }),
        label="Media"
    )

    date = django_filters.DateFilter(
        label="Month & year", method="filterby_date", widget=forms.DateInput(attrs={'class': 'form-control tdate'}))

    class Meta:
        model = Expense
        fields = ['amount', ]

    def __init__(self, *args, **kwargs):
        super(ExpenseFilter, self).__init__(*args, **kwargs)

    def filter_by_expense_type(self, queryset, name, value):
        return queryset.filter(
            Q(expense_type__name__icontains=value)
        )

    def filter_by_income_type(self, queryset, name, value):
        return queryset.filter(
            Q(income_type__name__icontains=value)
        )

    def filterby_date(self, queryset, name, value):
        start_date = datetime.strptime(str(value), '%Y-%m-%d')
        return queryset.filter(
            Q(date__month=start_date.month,date__year=start_date.year)
        )


    def order_by_field(self, queryset, name, value):
        return queryset.order_by(value)


class AccountFilter(django_filters.FilterSet):
    date = django_filters.DateFilter(label="Date",widget=forms.TextInput(attrs={'class': 'form-control tdate'}))
    amount = django_filters.NumberFilter(label='Amount', widget=forms.NumberInput(attrs={'class': 'form-control'}))
    class Meta:
        model = Account
        fields = ['date','amount']

    def __init__(self, *args, **kwargs):
        super(AccountFilter, self).__init__(*args, **kwargs)
