from django import forms
from django.forms import ModelForm
from .models import *
from django.apps import apps
from django.contrib.contenttypes.models import ContentType


class ExpenseForm(ModelForm):
    # pass
    # tax = forms.ModelMultipleChoiceField(
    #     required=False,
    #     queryset=Tax.objects.all(),
    #     widget=forms.SelectMultiple(attrs={'id': "tax", 'class': 'form-control1'}),
    # )

    class Meta:
        model = Expense

        widgets = {

            "description": forms.TextInput(attrs={'class': "form-control", "type": "text"}),
            "expense_type": forms.Select(attrs={'id': "expense_type", 'class': 'form-control1', "multiple": False, 'required': "required"}),
            "amount": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            "date": forms.TextInput(attrs={'class': "form-control tdate", 'required': "required"}),
            "voucher_number": forms.TextInput(attrs={'class': 'form-control1'}),
            # "income_or_expense": forms.CheckboxInput(attrs={}),
        }
        fields = ['description', 'expense_type', 'amount', 'date','voucher_number']


class ExpenseFormsetForm(ModelForm):

    class Meta:
        model = Expense

        widgets = {

            "date": forms.TextInput(attrs={'class': "form-control1 tdate", "type": "text", 'required': "required"}),
            "expense_type": forms.Select(attrs={'id': "expense_type", 'class': 'form-control1', "multiple": False, 'required': "required"}),
            "amount": forms.NumberInput(attrs={'class': "form-control1", 'required': "required"}),
            "description": forms.TextInput(attrs={'id': "vendor", 'class': 'form-control1'}),
            "voucher_number": forms.TextInput(attrs={'class': 'form-control1'}),
            # "note": forms.Textarea(attrs={'class': "form-control", 'id': "note", 'rows': 1}),

        }
        fields = ['date', 'expense_type', 'amount', 'description','voucher_number']


class IncomeForm(ModelForm):
    # pass
    # tax = forms.ModelMultipleChoiceField(
    #     required=False,
    #     queryset=Tax.objects.all(),
    #     widget=forms.SelectMultiple(attrs={'id': "tax", 'class': 'form-control1'}),
    # )

    class Meta:
        model = Expense

        widgets = {

            "description": forms.TextInput(attrs={'class': "form-control", "type": "text", 'required': "required"}),
            "income_type": forms.Select(attrs={'id': "expense_type", 'class': 'form-control1', "multiple": False, 'required': "required"}),
            "amount": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            "date": forms.TextInput(attrs={'class': "form-control tdate", 'required': "required"}),
            # "income_or_expense": forms.CheckboxInput(attrs={}),
        }
        fields = ['description', 'income_type', 'amount', 'date']


class IncomeFormsetForm(ModelForm):

    class Meta:
        model = Expense

        widgets = {

            "date": forms.TextInput(attrs={'class': "form-control1 tdate", "type": "text", 'required': "required"}),
            "income_type": forms.Select(attrs={'id': "expense_type", 'class': 'form-control1', "multiple": False, 'required': "required"}),
            "amount": forms.NumberInput(attrs={'class': "form-control1", 'required': "required"}),
            "description": forms.TextInput(attrs={'id': "vendor", 'class': 'form-control1'}),
            # "note": forms.Textarea(attrs={'class': "form-control", 'id': "note", 'rows': 1}),

        }
        fields = ['date', 'income_type', 'amount', 'description']


class AccountForm(ModelForm):

    class Meta:
        model = Account

        widgets = {

            "date": forms.TextInput(attrs={'class': "form-control tdate", "type": "text", 'required': "required"}),
            "amount": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            "head": forms.Select(attrs={'id': "vendor", 'class': 'form-control','required': "required"}),
            "description": forms.TextInput(attrs={'id': "vendor", 'class': 'form-control'}),

        }
        fields = ['date', 'head', 'amount', 'description']



