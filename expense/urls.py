from django.conf.urls import url, include
# from expense.bill.urls import urlpatterns as bill_patterns
from . import views, api

app_name = 'expense'

urlpatterns = [

    url(r'^dashboard/$', views.dashboard, name="dashboard"),
    url(r'^expenses/$', views.expense_index, name='expense_index'),
    url(r'^expense/(?P<pk>[^/]+)/edit/$', views.edit_Expense, name='edit_Expense'),
    url(r'^expense/create/$', views.expense_create, name='expense_create'),
    url(r'^incomes/$', views.income_index, name='income_index'),
    url(r'^income/(?P<pk>[^/]+)/edit/$', views.edit_income, name='edit_income'),
    url(r'^income/create/$', views.income_create, name='income_create'),
    url(r'^expense_income/report/$', views.expense_income_report, name='expense_income_report'),
    url(r'^account/report/$', views.account_report, name='account_report'),
    url(r'^expense_income/dashboard/$', views.report_dash, name='report_dash'),
    url(r'^account/create/$', views.account_create, name='account_create'),
    url(r'^account/list/$', views.account_list, name='account_list'),
    url(r'^transaction/report/$', views.transaction_report, name='transaction_report'),
    url(r'cash/breakup/report/$', views.cash_breakup_report, name='cash_breakup_report'),
    url(r'cash/received/report/$', views.cash_received_report, name='cash_received_report'),
    url(r'income/received/report/$', views.income_received_report, name='income_received_report'),

    url(r'^account/(?P<pk>[^/]+)/edit/$', views.account_edit, name='account_edit'),
    url(r'^account/undeposited_fund_create/$', views.undeposited_fund_create, name='undeposited_fund_create'),

]
