from sitetree.utils import item, tree

sitetrees = (

    tree('sidebar', items=[

        item(
            'Income',
            'expense:account_list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="money",

        ),
        item(
            'Expense',
            'expense:expense_index',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="money",

        ),

        item(
            'Enrollment Report',
            'enquiry:enrolment_report',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="user",

        ),

        item(
            'Transaction Report',
            '#',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="money",

            children=[
                item(
                    'Income/Expense',
                    'expense:expense_income_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),

                item(
                    'Fund acquisition',
                    'expense:transaction_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],
                ),

            ]

        ),
        item(
            'Analytics of cash',
            '#',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="money",

            children=[
                item(
                    'Cash BreakUp',
                    'expense:cash_breakup_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),
                item(
                    'Cash Received',
                    'expense:cash_received_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),

                item(
                    'Summary Fund',
                    'expense:account_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                ),
            ]

        ),

        item(
            'Settings',
            'location:settings',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="indent",

        )
    ]),

    tree('sidebar_branch_head', items=[

        item(
            'Income',
            'expense:account_list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="money",

        ),
        item(
            'Expense',
            'expense:expense_index',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="money",

        ),

        item(
            'Enrollment Report',
            'enquiry:enrolment_report',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="bar-chart",


        ),


        item(
            'Transaction Report',
            '#',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="money",

            children=[
                item(
                    'Income/Expense',
                    'expense:expense_income_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),

                item(
                    'Fund acquisition',
                    'expense:transaction_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],
                ),
            ]

        ),
        item(
            'Analytics of cash',
            '#',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="money",

            children=[
                item(
                    'Cash BreakUp',
                    'expense:cash_breakup_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),
                item(
                    'Cash Received',
                    'expense:cash_received_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),

                item(
                    'Summary Fund',
                    'expense:account_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                ),
            ]

        ),

        item(
            'Settings',
            'location:settings',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="indent",

        )
    ]),

    tree('sidebar_marketing_head', items=[
        # Then define items and their children with `item` function.


        item(
            'Tele caller Daily Report',
            'enquiry:enrolment_report_daily_report',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="bar-chart",

        ),
        item(
            'Course Summary',
            'student:course_monthly_summary',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="bar-chart",

        ),
        item(
            'Settings',
            'location:settings',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="indent",

        ),
    ]),

    tree('sidebar_director', items=[

        item(
            'Income',
            'expense:account_list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="money",

        ),
        item(
            'Expense',
            'expense:expense_index',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="money",

        ),

        item(
            'Enrollment Report',
            'enquiry:enrolment_report',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="user",
        ),
        item(
            'Tele caller Daily Report',
            'enquiry:enrolment_report_daily_report',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="bar-chart",

            ),

        item(
            'Transaction Report',
            '#',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="money",

            children=[
                item(
                    'Income/Expense',
                    'expense:expense_income_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),

                item(
                    'Fund acquisition',
                    'expense:transaction_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],
                ),
                item(
                    'Working Fee',
                    'student:working_fee',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],
                ),
                item(
                    'Summary Working Fee',
                    'student:summary_working_fee',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],
                ),
            ]

        ),
        item(
            'Analytics of cash',
            '#',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="money",

            children=[
                item(
                    'Cash BreakUp',
                    'expense:cash_breakup_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),
                item(
                    'Cash Received',
                    'expense:cash_received_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),

                item(
                    'Summary Fund',
                    'expense:account_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                ),
            ]

        ),


        item(
            'Course Summary',
            'student:course_monthly_summary',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="user",

        ),
        item(
            'Settings',
            'location:settings',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="indent",

        ),
    ]),



    tree('sidebar_ibis_admin', items=[

        item(
            'Income',
            'expense:account_list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="money",

        ),
        item(
            'Expense',
            'expense:expense_index',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="money",

        ),

        item(
            'Enrollment Report',
            'enquiry:enrolment_report',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="user",

        ),

        item(
            'Transaction Report',
            '#',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="money",

            children=[
                item(
                    'Income/Expense',
                    'expense:expense_income_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),

                item(
                    'Fund acquisition',
                    'expense:transaction_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],
                ),
                item(
                    'Working Fee',
                    'student:working_fee',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],
                ),
                item(
                    'Summary Working Fee',
                    'student:summary_working_fee',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],
                ),
            ]

        ),
        item(
            'Analytics of cash',
            '#',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="money",

            children=[
                item(
                    'Cash BreakUp',
                    'expense:cash_breakup_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),
                item(
                    'Cash Received',
                    'expense:cash_received_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                    # access_by_perms=['enquiry.view_quote_list'],

                ),

                item(
                    'Summary Fund',
                    'expense:account_report',
                    in_menu=True,
                    in_sitetree=True,
                    access_loggedin=True,
                    icon_class="money",
                ),
            ]

        ),

        item(
            'Course Summary',
            'student:course_monthly_summary',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="bar-chart",

        ),

        item(
            'Settings',
            'location:settings',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="indent",

        ),

    ]),

)
