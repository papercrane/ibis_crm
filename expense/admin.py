from django.contrib import admin
from .models import Expense, ExpenseType, IncomeType, AccountHead, Account


class ExpenseAdmin(admin.ModelAdmin):
    list_display = ('date','description','amount')
    list_filter = ('date',)
    ordering = ('-date',)

    class Meta:
        model = Expense


admin.site.register(Expense, ExpenseAdmin)
admin.site.register(ExpenseType)
admin.site.register(IncomeType)
admin.site.register(AccountHead)
admin.site.register(Account)
