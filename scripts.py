
import os
import django

if __name__ == '__main__':  
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ibis_crm.settings')

    django.setup()
    from django.core import management
    # from django.apps import apps
    # from student.management.commands import deadlinecrossed

    # management.call_command('reminders', verbosity=0, interactive=False)
    # management.call_command('task_reminder', verbosity=0, interactive=False)
    # management.call_command('project_reminder', verbosity=0, interactive=False)
    management.call_command('deadlinecrossed', verbosity=0, interactive=False)
    management.call_command('enquiryEmployee', verbosity=0, interactive=False)
    

