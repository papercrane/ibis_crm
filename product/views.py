from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import Count, Sum
from django.http import HttpResponseRedirect

from crm.models import Campaign
from easyLink.decorators import group_required
from .forms import *
from django.shortcuts import get_object_or_404, render
from .filters import ProductFilter, SubjectFilter
from django.forms import inlineformset_factory
from product.models import *
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from student.models import StudentCourse, StudentCertificate


@login_required
@group_required('front office admin')
def add_products(request):
    form = ProductsForm(request.POST or None)

    if request.method == "POST":
        if form.is_valid():
            product = form.save(commit=False)
            product.save()
            messages.add_message(request, messages.INFO, 'Program created successfully')
            return HttpResponseRedirect(reverse('product:detail', args=[product.id]))
    context = {
        "form": form,
        "type": "add",
    }
    return render(request, 'product/add_products.html', context)


@login_required
def view_products(request):
    page_number = request.GET.get('page', 1)
    if request.user.employee.primary_designation.name == 'Programme Director':
        queryset = Product.objects.filter(program_director=request.user.employee)
    else:
        queryset = Product.objects.all()
    f = ProductFilter(request.GET, queryset=queryset.order_by("-created_at"))
    paginator = Paginator(f.qs, 20)

    try:
        products = paginator.page(page_number)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)
    context = {
        "products": products,
        'filter': f,

    }
    return render(request, 'product/view_products.html', context)


@login_required
def detail(request, pk):
    product = Product.objects.get(id=pk)
    f = SubjectFilter(request.GET, queryset=Subject.objects.all().order_by("-created_at"))
    course_form = CourseForm()
    subject_form = SubjectForm()
    context = {
        "product": product,
        "pk": pk,
        "course_form": course_form,
        "subject_form": subject_form,
        "filter": f
    }
    return render(request, "product/detail.html", context)


@login_required
def edit_product(request, product_pk):
    product = Product.objects.get(id=product_pk)
    form = ProductsForm(request.POST or None, instance=product)

    if request.method == "POST":
        if form.is_valid():
            product = form.save(commit=False)
            # if 'total_fee' in form.changed_data:
            #     product.calculateGST()
            product.save()

            return HttpResponseRedirect(reverse('product:detail', args=[product.id]))
    context = {
        "form": form,
        "type": "edit",
    }
    return render(request, 'product/add_products.html', context)


@login_required
def code_settings(request):
    if CodeSetting.objects.exists():
        code_form = CodeSettingsForm(request.POST or None, instance=CodeSetting.objects.last())
    else:
        code_form = CodeSettingsForm(request.POST or None)

    if request.method == "POST":
        if code_form.is_valid():
            code_form.save()
            return HttpResponseRedirect(reverse('product:view_products'))

    context = {
        'code_form': code_form,
    }

    return render(request, "product/code_setting.html", context)


@login_required
@group_required('front office admin')
def program_delete(request, pk):
    if request.method == 'POST':
        if StudentCourse.objects.filter(course__batch__program__pk=pk).count():
            messages.add_message(request, messages.INFO,
                                 'You cannot delete the programme because you have students enrolled for this.')
        else:
            product = get_object_or_404(Product, pk=pk)
            product.delete()
    return HttpResponseRedirect(reverse('product:view_products'))


@login_required
def dashboard(request):
    return render(request, "product/dashboard.html")


@login_required
def add_course(request, pk):
    program = get_object_or_404(Product, pk=pk)
    course_form = CourseForm(request.POST)
    if course_form.is_valid():
        course = course_form.save(commit=False)
        course.program = program
        course.save()
    return HttpResponseRedirect(reverse('product:detail', args=[pk]))


@login_required
def add_subject(request, pk):
    product = get_object_or_404(Product, pk=pk)
    subject_form = SubjectForm(request.POST)
    if subject_form.is_valid():
        subject = subject_form.save(commit=False)
        subject.program = product
        subject.save()
    return HttpResponseRedirect(reverse('product:detail', args=[pk]))


@login_required
def edit_course(request, course_pk):
    course = Batch.objects.get(id=course_pk)
    form = CourseForm(request.POST or None, instance=course)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            # for batch in course.batches.all():
            #     batch.code = course.abbreviation + '-' + batch.duration + '-' + batch.time + '-'+batch.start_date.strftime('%d%m%y')
            #     batch.save()

            return HttpResponseRedirect(reverse('product:detail', args=[course.program.id]))
    context = {
        "form": form,
        "type": "edit",
        "course":course
    }
    return render(request, 'product/edit_courses.html', context)


@login_required
@group_required('front office admin')
def delete_course(request, pk):
    course = get_object_or_404(Batch, pk=pk)
    if request.method == 'POST':
        if StudentCourse.objects.filter(course__batch__pk=pk).count():
            messages.add_message(request, messages.INFO,
                                 'You cannot delete this course because you have students enrolled for this.')
        else:
            course.delete()
    return HttpResponseRedirect(reverse('product:detail', args=[course.program.id]))


@login_required
def edit_subject(request, subject_pk):
    subject = Subject.objects.get(id=subject_pk)
    form = SubjectForm(request.POST or None, instance=subject)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('product:detail', args=[subject.program.id]))
    context = {
        "form": form,
        "type": "edit",
    }
    return render(request, 'product/edit_subject.html', context)


@login_required
@group_required('front office admin')
def delete_subject(request, pk):
    if request.method == 'POST':
        subject = get_object_or_404(Subject, pk=pk)
        subject.delete()
    return HttpResponseRedirect(reverse('product:detail', args=[subject.program.id]))


@login_required
@group_required('front office admin')
def add_installments(request, pk):
    course = get_object_or_404(Course, pk=pk)
    InstallmentFormSet = inlineformset_factory(Course, CourseInstallment, form=CourseInstallmentForm, extra=0,
                                               min_num=1, can_delete=True)
    installment_formset = InstallmentFormSet(instance=course)
    if request.method == "POST":
        # total = 0
        installment_formset = InstallmentFormSet(request.POST, instance=course)
        if installment_formset.is_valid():
            installment_formset.save()
            return HttpResponseRedirect(reverse('product:course_detail', args=[course.batch.pk]))

    context = {
        "installment_formset": installment_formset,
        'form_url': reverse_lazy('product:add_installments', args=[course.pk]),
        "type": "add",
        "course": course,
    }
    return render(request, "product/installments.html", context)


@login_required
def source_summary_report(request):
    products = Product.objects.annotate(total_count=Count('enquiry_courses__enquiry__my_courses'))
    sources = Campaign.objects.annotate(total_sources=Count('enquiry_campaign__enquiry__my_courses'))
    leads = sources.aggregate(total_leads=Sum('total_sources'))
    context = {
        'sources': sources,
        'leads': leads,
        'products': products

    }

    return render(request, "product/source_summary_report.html", context)


@login_required
def add_batch(request, pk):
    batch_form = BatchForm(request.POST)
    if batch_form.is_valid():
        course = get_object_or_404(Batch, pk=pk)
        batch = batch_form.save(commit=False)
        batch.batch = course
        batch.save()
    return HttpResponseRedirect(reverse('product:course_detail', args=[pk]))


@login_required
def course_detail(request, pk):
    course = Batch.objects.get(id=pk)
    batch_form = BatchForm()
    context = {
        "course": course,
        "form": batch_form,

    }
    return render(request, "product/course_detail.html", context)


@login_required
def edit_batch(request, pk):
    batch = Course.objects.get(id=pk)
    form = BatchForm(request.POST or None, instance=batch)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('product:course_detail', args=[batch.batch.id]))
    context = {
        "form": form,
        "type": "edit",
        "batch": batch

    }
    return render(request, 'product/edit_batch.html', context)


@login_required
@group_required('front office admin')
def delete_batch(request, pk):
    course = get_object_or_404(Course, pk=pk)
    if request.method == 'POST':
        if StudentCourse.objects.filter(course__pk=pk).count():
            messages.add_message(request, messages.INFO,
                                 'You cannot delete this course because you have students enrolled for this.')
        else:
            course.installments.all().delete()
            course.delete()
    return HttpResponseRedirect(reverse('product:course_detail', args=[course.batch.id]))


@login_required
def add_certification(request, pk):
    form = CourseCertificationForm(request.POST or None, request.FILES or None)
    course = get_object_or_404(Batch, pk=pk)
    if request.method == "POST" and form.is_valid():
        course_certificate = form.save(commit=False)
        course_certificate.course = course
        course_certificate.save()
        return HttpResponseRedirect(reverse('product:detail', args=[course_certificate.course.program.pk]))
    context = {'form': form, 'form_url': reverse('product:add_certification', args=[pk]), 'course': course,
               'certifications': course.certifications.all()}
    return render(request, 'product/add_certification.html', context)


@login_required
def edit_certification(request, pk):
    certificate = get_object_or_404(CourseCertification, pk=pk)
    form = CourseCertificationForm(request.POST or None, request.FILES or None, instance=certificate)
    if request.method == 'POST' and form.is_valid():
        form.save(commit=True)
        return HttpResponseRedirect(reverse('product:detail', args=[certificate.course.program.pk]))
    context = {'form': form, 'form_url': reverse('product:edit_certification', args=[pk]), 'certificate': certificate}
    return render(request, 'product/edit_certification.html', context)


@login_required
@group_required('front office admin')
def delete_certification(request, pk):
    certificate = get_object_or_404(CourseCertification, pk=pk)
    if StudentCertificate.objects.filter(certificate=certificate):
        messages.add_message(request, messages.INFO,
                             'You cannot delete this certificate because you have students with these certificate.')
    else:
        certificate.delete()
    return HttpResponseRedirect(reverse('product:add_certification', args=[certificate.course.program.pk]))



@login_required
def add_subjects(request, pk):
    course = get_object_or_404(Course, pk=pk)
    SubjectFormSet = inlineformset_factory(Course, Subject, form=CourseSubjectForm, extra=0,min_num=1, can_delete=True)
    subject_formset = SubjectFormSet(instance=course)
    if request.method == "POST":

        subject_formset = SubjectFormSet(request.POST, instance=course)
        if subject_formset.is_valid():
            subject_formset.save()
            return HttpResponseRedirect(reverse('product:course_detail', args=[course.batch.pk]))

    context = {
        "subject_formset": subject_formset,
        'form_url': reverse_lazy('product:add_subjects', args=[course.pk]),
        "type": "add",
        "course": course,
    }
    return render(request, "product/subjects.html", context)
