from django import forms
from django.forms import ModelForm

from employee.models import Employee
from .models import Product, Course, Subject, CourseInstallment, Batch, CourseCertification
from .models import CodeSetting


class ProductsForm(ModelForm):
    class Meta:
        model = Product

        widgets = {

            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "accademic_eligibility": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "no_of_certification": forms.NumberInput(attrs={'class': "form-control"}),
            "certification_details": forms.Textarea(attrs={'class': "form-control"}),
            "placement_details": forms.Textarea(attrs={'class': "form-control"}),
            "abbreviation": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "program_director": forms.Select(attrs={'class': "form-control", 'required': "required"}),

        }
        fields = ['name', 'accademic_eligibility', 'no_of_certification',
                  'certification_details', 'placement_details', 'abbreviation', 'program_director']

    def __init__(self, *args, **kwargs):
        super(ProductsForm, self).__init__(*args, **kwargs)
        self.fields['program_director'].queryset = Employee.objects.filter(
            primary_designation__name='Programme Director')


class CodeSettingsForm(ModelForm):
    class Meta:
        model = CodeSetting

        widgets = {
            "prefix": forms.TextInput(attrs={'class': "form-control", 'id': "prefix", 'required': "required"}),
            "count_index": forms.TextInput(attrs={'class': "form-control", 'id': "count_index"}),
            "no_of_characters": forms.TextInput(attrs={'class': "form-control", 'id': "no_of_characters"}),
        }

        fields = ['prefix', 'count_index', 'no_of_characters']


class CourseForm(ModelForm):
    class Meta:
        model = Batch
        widgets = {
            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "abbreviation": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "enrollment_abbreviation": forms.TextInput(attrs={'class': "form-control", 'required': "required"})

        }
        fields = ['name','abbreviation','enrollment_abbreviation']


class SubjectForm(ModelForm):
    class Meta:
        model = Subject

        widgets = {
            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            # "program": forms.Select(attrs={'class': "form-control", 'required': "required"}),
        }
        fields = ['name']


class CourseInstallmentForm(ModelForm):
    class Meta:
        model = CourseInstallment

        widgets = {
            "percentage": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "no_of_days": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "title": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
        }
        fields = ['percentage', 'no_of_days', 'title']


class BatchForm(ModelForm):
    class Meta:
        model = Course
        widgets = {
            "duration": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "time": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "start_date": forms.TextInput(attrs={'class': "form-control tdate", 'required': "required"}),
            "total_fee": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            "course_fee": forms.NumberInput(attrs={'class': "form-control"}),
            # "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),

        }
        fields = ['duration', 'time', 'start_date', 'total_fee', 'course_fee']


class CourseCertificationForm(ModelForm):
    class Meta:
        model = CourseCertification

        widgets = {
            'name': forms.TextInput(attrs={'class': "form-control"}),
            'test_name': forms.TextInput(attrs={'class': "form-control"}),
            'header_image': forms.FileInput(attrs={'class': "form-control"}),

        }
        fields = ['name', 'test_name', 'header_image']

class CourseSubjectForm(ModelForm):

    class Meta:
        model = Subject

        widgets = {
            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),

        }
        fields = ['name',]
