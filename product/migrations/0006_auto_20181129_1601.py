# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-11-29 10:31
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import erp_core.models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0005_remove_course_no_of_installments'),
    ]

    operations = [
        migrations.CreateModel(
            name='Batch',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(blank=True, max_length=40, null=True)),
                ('count', models.PositiveIntegerField(default=0)),
                ('program', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='batches', to='product.Product')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, erp_core.models.UpdateMixin),
        ),
        migrations.RemoveField(
            model_name='course',
            name='name',
        ),
        migrations.RemoveField(
            model_name='course',
            name='program',
        ),
        migrations.AlterField(
            model_name='course',
            name='total_fee',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=20, verbose_name='Course Fee (INR)'),
        ),
    ]
