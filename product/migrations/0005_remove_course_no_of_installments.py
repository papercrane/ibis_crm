# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-11-27 03:50
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0004_auto_20181127_0915'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='course',
            name='no_of_installments',
        ),
    ]
