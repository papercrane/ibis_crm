# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2019-01-16 06:30
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('employee', '0002_auto_20181127_0921'),
        ('product', '0013_remove_product_program_director'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='program_director',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='program', to='employee.Employee'),
        ),
    ]
