# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2019-01-14 06:34
from __future__ import unicode_literals

from django.db import migrations, models
import erp_core.models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0009_auto_20181219_1030'),
    ]

    operations = [
        migrations.CreateModel(
            name='CourseCertification',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('name', models.CharField(max_length=100)),
                ('test_name', models.CharField(max_length=100)),
                ('header_image', models.ImageField(upload_to='course_certificate/%Y/%m/%d')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, erp_core.models.UpdateMixin),
        ),
    ]
