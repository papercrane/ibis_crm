# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-11-30 06:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0007_auto_20181129_1603'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='batch',
            options={'verbose_name': 'Course', 'verbose_name_plural': 'Courses'},
        ),
        migrations.AlterModelOptions(
            name='course',
            options={'verbose_name': 'Batch', 'verbose_name_plural': 'Batches'},
        ),
        migrations.AlterField(
            model_name='batch',
            name='name',
            field=models.CharField(default=0, max_length=100),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='course',
            name='total_fee',
            field=models.DecimalField(decimal_places=2, max_digits=20, verbose_name='Course Fee (INR)'),
        ),
    ]
