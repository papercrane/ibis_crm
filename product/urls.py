from django.conf.urls import url
from product import views

app_name = "product"


urlpatterns = [
    url(r'^programs/add/$', views.add_products, name='add_products'),
    url(r'^course/(?P<course_pk>[^/]+)/edit/$',
        views.edit_course, name='edit_course'),
    url(r'^programs/$', views.view_products, name='view_products'),
    url(r'^course/(?P<pk>[^/]+)/$', views.course_detail, name='course_detail'),
    url(r'^program/(?P<pk>[^/]+)/$', views.detail, name='detail'),
    url(r'^programs/edit_program/(?P<product_pk>[^/]+)/$',
        views.edit_product, name='edit_product'),
    url(r'^programs/edit_subject/(?P<subject_pk>[^/]+)/$',
        views.edit_subject, name='edit_subject'),
    url(r'^programs/code/settings/$', views.code_settings, name='code_settings'),
    url(r'^programs/(?P<pk>[^/]+)/delete/$', views.program_delete, name='delete'),
    url(r'^dashboard/$', views.dashboard, name='dashboard'),
    url(r'^program/(?P<pk>[^/]+)/add/course/$', views.add_course, name='add_course'),
    url(r'^subject/(?P<pk>[^/]+)/$', views.add_subject, name='add_subject'),
    url(r'^course/(?P<pk>[^/]+)/delete/$', views.delete_course, name='delete_course'),
    url(r'^programs/(?P<pk>[^/]+)/subject/delete/$', views.delete_subject, name='delete_subject'),
    url(r'^course/(?P<pk>[^/]+)/installments/$', views.add_installments, name='add_installments'),
    url(r'^source_summary_report/$', views.source_summary_report, name='source_summary_report'),
    url(r'^course/(?P<pk>[^/]+)/add/batch/$', views.add_batch, name='add_batch'),
    url(r'^batch/(?P<pk>[^/]+)/edit/$', views.edit_batch, name='edit_batch'),
    url(r'^batch/(?P<pk>[^/]+)/delete/$', views.delete_batch, name='delete_batch'),
    url(r'^course/(?P<pk>[^/]+)/certificate/add/$', views.add_certification, name='add_certification'),
    url(r'^certificate/(?P<pk>[^/]+)/edit/$', views.edit_certification, name='edit_certification'),
    url(r'^certificate/(?P<pk>[^/]+)/delete/$', views.delete_certification, name='delete_certification'),
    url(r'^course/(?P<pk>[^/]+)/subjects/$', views.add_subjects, name='add_subjects'),


]
