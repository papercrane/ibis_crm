import django_filters
from django import forms
from django.db.models import Q
from product.models import Product, Subject
from student.models import Student


class ProductFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))
    accademic_eligibility = django_filters.CharFilter(lookup_expr='icontains',
                                                      widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Product
        fields = ['name', ]

    def order_by_field(self, queryset, name, value):
        return queryset.order_by(value)


class SubjectFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Subject
        fields = ['name', ]

    def order_by_field(self, queryset, name, value):
        return queryset.order_by(value)


class StudentFilter(django_filters.FilterSet):
    code = django_filters.CharFilter(name="code", lookup_expr='icontains',
                                     widget=forms.TextInput(attrs={'class': 'form-control'}))
    name = django_filters.CharFilter(label="Name", method="filter_by_name",
                                     widget=forms.TextInput(attrs={'class': 'form-control'}))

    contact_number = django_filters.CharFilter(label="Contact Number", method="filter_by_contact_number",
                                               widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Student
        fields = ['code', ]

    def filter_by_name(self, queryset, name, value):
        return queryset.filter(
            Q(enquiry__name_of_candidate__icontains=value)
        )

    def filter_by_contact_number(self, queryset, name, value):
        return queryset.filter(enquiry__contact_number=value)
