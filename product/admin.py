from django.contrib import admin

# Register your models here.
from product.models import Product, Course, Subject, CourseInstallment, Batch, CourseCertification

admin.site.register(Product)
admin.site.register(Course)
admin.site.register(Subject)
admin.site.register(CourseInstallment)
admin.site.register(Batch)
admin.site.register(CourseCertification)

