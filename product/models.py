import base64

import requests
from django.db import models
from django.db.models import Sum

from employee.models import Employee
from erp_core.models import BaseCodeSetting
from erp_core.models import ErpAbstractBaseModel


class Product(ErpAbstractBaseModel):
    name = models.CharField(max_length=255)
    accademic_eligibility = models.CharField(max_length=255, null=True, blank=True)
    no_of_certification = models.IntegerField(null=True, blank=True)
    certification_details = models.TextField(null=True, blank=True)
    placement_details = models.TextField(null=True, blank=True)
    abbreviation = models.CharField(max_length=10, null=True, blank=True)
    program_director = models.OneToOneField(Employee, null=True, related_name='program')

    def __str__(self):
        return self.name


class Course(ErpAbstractBaseModel):
    TWO_MONTH = '2M'
    THREE_MONTH = '3M'
    SIX_MONTH = '6M'
    TWELVE_MONTH = '12M'
    TWENTY_FOUR_DAYS = '24D'
    FOUR_DAYS = '4D'
    TWENTY_DAYS = '20D'
    TWO_DAYS = '2D'

    DURATIONS = (
        (TWO_MONTH, "2 Months"),
        (THREE_MONTH, "3 Months"),
        (SIX_MONTH, "6 Months"),
        (TWELVE_MONTH, "12 Months"),
        (TWENTY_FOUR_DAYS, "24 Days"),
        (FOUR_DAYS, "4 Days"),
        (TWENTY_DAYS, "20 Days"),
        (TWO_DAYS, "2 Days"),

    )

    NINE_ELEVEN = '9'
    NINE_ONE = 'M'
    ELEVEN_ONE = '11'
    ONE_THIRTY_THREE_THIRTY = '1.30'
    THREE_THIRTY_FIVE_THIRTY = '3.30'
    ONE_THIRTY_FIVE_THIRTY = 'A'
    TIMES = (
        (NINE_ELEVEN, "9 AM to 11AM"),
        (ELEVEN_ONE, "11 AM to 1PM"),
        (ONE_THIRTY_THREE_THIRTY, "1.30PM to 3.30PM"),
        (THREE_THIRTY_FIVE_THIRTY, "3.30PM to 5.30PM"),
        (NINE_ONE, "9 AM to 1PM"),
        (ONE_THIRTY_FIVE_THIRTY, "1.30PM to 5.30PM"),
    )

    code = models.CharField(max_length=100)
    batch = models.ForeignKey('product.Batch', related_name="batches")
    duration = models.CharField(max_length=40, choices=DURATIONS)
    time = models.CharField(max_length=40, choices=TIMES)
    start_date = models.DateField()
    total_fee = models.DecimalField("Course Fee (INR)", max_digits=20, decimal_places=2)
    course_fee = models.DecimalField("Course Fee (USD)", max_digits=20, decimal_places=2, null=True, blank=True)
    count = models.PositiveIntegerField(default=0)

    def save(self, *args, **kwargs):
        if self.code == '' or self.code == None:
            self.code = Course.codeGenerator(self)
        return super(Course, self).save(*args, **kwargs)

    def codeGenerator(self):
        course_code = self.batch.abbreviation + '-' + self.duration + '-' + self.time + '-'+self.start_date.strftime('%d%m%y')
        return course_code

    def __str__(self):
        return self.batch.name + '(' + self.code + ')-' + str(self.start_date)

    class Meta:
        verbose_name = "Batch"
        verbose_name_plural = "Batches"
        ordering = ["-start_date",]


class Subject(ErpAbstractBaseModel):
    name = models.CharField(max_length=40)
    program = models.ForeignKey(Course, related_name='subjects', null=True)

    def __str__(self):
        return str(self.name)


class CodeSetting(BaseCodeSetting):
    pass


class CourseInstallment(ErpAbstractBaseModel):
    percentage = models.PositiveIntegerField()
    no_of_days = models.PositiveIntegerField()
    title = models.CharField(max_length=100)
    course = models.ForeignKey(Course, related_name="installments")

    def __str__(self):
        return self.course.batch.name


class Batch(ErpAbstractBaseModel):
    name = models.CharField(max_length=100)
    program = models.ForeignKey(Product, related_name="courses")
    count = models.PositiveIntegerField(default=0)
    abbreviation = models.CharField(max_length=10, null=True)
    enrollment_abbreviation = models.CharField(max_length=20, null=True)


    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Course"
        verbose_name_plural = "Courses"


class CourseCertification(ErpAbstractBaseModel):
    name = models.CharField(max_length=100)
    test_name = models.CharField(max_length=100)
    header_image = models.ImageField(upload_to='course_certificate/%Y/%m/%d')
    course = models.ForeignKey(Batch, related_name='certifications')

    def __str__(self):
        return self.test_name

    def get_image_as64(self):
        return base64.b64encode(requests.get('http://127.0.0.1:8000' + self.header_image.url).content)
