from sitetree.utils import item, tree

# Be sure you defined `sitetrees` in your module.
sitetrees = (
    # Define a tree with `tree` function.
    tree('sidebar', items=[
        # Then define items and their children with `item` function.

        item(
            'Program',
            'product:view_products',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="book",

            children=[
                # item(
                #     'Add Program',
                #     'product:add_products',
                #     in_menu=True,
                #     in_sitetree=True,
                #     access_loggedin=True,
                #     icon_class="print",
                #     # access_by_perms=['enquiry.view_quote_list'],

                # ),
                # item(
                #     'Program List',
                #     'product:view_products',
                #     in_menu=True,
                #     in_sitetree=True,
                #     access_loggedin=True,
                #     icon_class="print",
                #     # access_by_perms=['enquiry.view_quote_list'],

                # ),
                # #  item(
                #     'Add HSN',
                #     'admin:gst_hsn_changelist',
                #     in_menu=True,
                #     in_sitetree=True,
                #     access_loggedin=True,
                #     icon_class="print",
                #     # access_by_perms=['enquiry.view_quote_list'],

                # ),



            ]

        )

    ]),

    tree('sidebar_branch_head', items=[
        # Then define items and their children with `item` function.
        item(
            'Program',
            'product:view_products',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="book",
            # access_by_perms=['project.view_project_list'],

        ),
    ]),

    tree('sidebar_marketing_head', items=[
        # Then define items and their children with `item` function.
        item(
            'Program',
            'product:view_products',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="book",
            # access_by_perms=['project.view_project_list'],

        ),
    ]),

    tree('sidebar_director', items=[
        # Then define items and their children with `item` function.
        item(
            'Program',
            'product:view_products',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="book",
            # access_by_perms=['project.view_project_list'],

        ),
    ]),



    tree('sidebar_ibis_admin', items=[
        # Then define items and their children with `item` function.
        item(
            'Program',
            'product:view_products',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="book",
            # access_by_perms=['project.view_project_list'],

        ),
    ]),

    tree('sidebar_programme_director', items=[
        # Then define items and their children with `item` function.
        item(
            'Program',
            'product:view_products',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="book",

        ),

    ]),

)
