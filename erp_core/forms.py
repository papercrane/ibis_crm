from django import forms
from django.forms import ModelForm
from .models import ActivityType


class ActivityTypeForm(ModelForm):

    class Meta:
        model = ActivityType
        widgets = {
            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
        }
        fields = ['name']
