import Vue from 'vue'
// import Cookies from 'js-cookie'
// var csrftoken = Cookies.get('csrftoken')

// import axios from 'axios'
// axios.defaults.headers.common['X-CSRFToken'] = csrftoken

export default {
	getQuotations(url) {
		// setTimeout(() => cb(_products), 100)
		return Vue.http.get(url)
	},

	getQuotationOptions() {
		// setTimeout(() => cb(_products), 100)
		return Vue.http.get('/api/quotations/choices')
	},

	// get(id) {
	// 	return Vue.http.get('/api/enquiries/' + id + '/')
	// },

	format_quotation(quotation) {
		return {
			'code':quotation.code,
			'status': quotation.status,
			'reference': quotation.reference,
			'reason_for_lost': quotation.reason_for_lost,
			'customer': quotation.customer.id,
			//'items': this.format_items(enquiry.items),
			//'activities': enquiry.activities.length ? enquiry.activities : null,
			'amount': quotation.amount
		}
	},

	// format_items(items) {
	// 	if (items.length == 0)
	// 		return null
	// 	var enquiry_items = items.map((item) => {
	// 		let i = Object.assign({}, item)
	// 		i['product'] = item.product.id
	// 		return i
	// 	})
	// 	// return items
	// 	return enquiry_items
	// },

	post(url, quotation) {
		console.log(quotation)
		var data = this.format_quotation(quotation)
		return Vue.http.post(url, data)
	},

	// delete(url) {
	// 	return Vue.http.delete(url)
	// }
	// // buyProducts (products, cb, errorCb) {
	//   setTimeout(() => {
	//     // simulate random checkout failure.
	//     (Math.random() > 0.5 || navigator.userAgent.indexOf('PhantomJS') > -1)
	//       ? cb()
	//       : errorCb()
	//   }, 100)
	// }
}
