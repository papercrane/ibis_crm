import Vue from 'vue'
import Vuex from 'vuex'
// import * as actions from './actions'
// import * as getters from './getters'
// import cart from './modules/cart'
import quotation from './modules/quotation'
// import products from './modules/products'
// import createLogger from '../../../src/plugins/logger'

Vue.use(Vuex)


export default new Vuex.Store({
	// actions,
	state:{
		loading: false,
	},
	mutations:{
		loading (state) {
			state.loading = true
		},

		loaded (state) {
			state.loading = false
		},

		failed (state) {
			state.loading = false
		},
	},
	// getters,
	modules: {
		quotationModel: quotation,
		// products
	},
	strict: true,
	// plugins: debug ? [createLogger()] : []
})