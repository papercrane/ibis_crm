import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// import EditEnquiry from './components/enquiry/edit.vue'
import EnquiryList from './components/list.vue'
// import EnquiryDetail from './components/enquiry/detail.vue'

const EditEnquiry = resolve => {
	require.ensure(['./components/edit.vue'], () => {
		resolve(require('./components/edit.vue'))
	})
}
const EnquiryDetail = resolve => {
	require.ensure(['./components/detail.vue'], () => {
		resolve(require('./components/detail.vue'))
	})
}

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * @param  {string}   name     the filename (basename) of the view to load.
 */
// function view(name) {
//     return function(resolve) {
//         require(['./views/' + name + '.vue'], resolve);
//     }
// };

const routes = [
	{ path: '/enquiries', component: EnquiryList, name: 'enquiries' },
	{ path: '/enquiries/create', component: EditEnquiry, name: 'create-enquiry' },
	// { path: '/enquiries/:id', component: System.import('./components/detail.vue'), name: 'enquiry' },
	{ path: '/enquiries/:id', component: EnquiryDetail, name: 'enquiry' },
	// { path: '/:id', component: EnquiryDetail }
]

export const router = new VueRouter({
	mode: 'history',
	base: '/',
	routes // short for routes: routes
})

// export { router }