import dj_database_url

from .prod_base import *

db_from_env = dj_database_url.config(conn_max_age=500)
DATABASES['default'].update(db_from_env)
DATABASES['default']['TEST'] = {'NAME': DATABASES['default']['NAME']}

EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_HOST_USER = 'app79264830@heroku.com'
EMAIL_HOST_PASSWORD = 'sw9ufpe00919'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
