from .prod_base import *

ALLOWED_HOSTS = ['139.59.12.241']
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ibis_crm_db',
        'USER': 'papercranepguser',
        'PASSWORD': 'mAd@123@',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

MAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'academyibis@gmail.com'
EMAIL_HOST_PASSWORD = 'mAd@123@'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
