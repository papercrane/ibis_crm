# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2019-03-18 10:04
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0003_auto_20190318_1533'),
    ]

    operations = [
        migrations.AlterField(
            model_name='designation',
            name='office',
            field=models.ForeignKey(default=uuid.UUID('0546cec2-2b1e-4c77-a6b3-97ce4703a43a'), on_delete=django.db.models.deletion.CASCADE, to='location.Office'),
        ),
    ]
