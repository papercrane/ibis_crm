import uuid

from django.contrib.auth.models import Permission
from django.db import models

from erp_core.models import ErpAbstractBaseModel
from location.models import Location, Office


class Division(ErpAbstractBaseModel):
    name = models.CharField(max_length=100)

    department = models.ForeignKey('Department', on_delete=models.CASCADE)

    division = models.ForeignKey('self', on_delete=models.CASCADE, related_name="parent_division", null=True,
                                 blank=True)

    def __str__(self):
        return str(self.name)


class Department(ErpAbstractBaseModel):
    name = models.CharField(max_length=100)

    def __str__(self):
        return str(self.name)


class Grade(ErpAbstractBaseModel):
    name = models.CharField(max_length=100)

    def __str__(self):
        return str(self.name)


class Designation(ErpAbstractBaseModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    name = models.CharField(max_length=100)
    minimum_working_hours = models.IntegerField(null=True, blank=True)

    zone = models.ForeignKey('location.Zone', on_delete=models.CASCADE, null=True, blank=True)
    location = models.ForeignKey('location.Location', on_delete=models.CASCADE, default=Location.objects.get(id=1).pk)

    office = models.ForeignKey('location.Office', on_delete=models.CASCADE, default=Office.objects.all().last().pk)

    department = models.ForeignKey(Department, on_delete=models.CASCADE, null=True, blank=True)

    division = models.ForeignKey(Division, on_delete=models.CASCADE, null=True, blank=True)
    grade = models.ForeignKey(Grade, on_delete=models.CASCADE, null=True, blank=True)
    permissions = models.ManyToManyField(Permission, blank=True)

    def __str__(self):
        return self.name+ '-' + str(self.location.city)

    def save(self, *args, **kwargs):
        # auth.models.Group.objects.create(name=self.name)
        super(Designation, self).save(*args, **kwargs)

# class BasicPayDaily(ErpAbstractBaseModel):
#     designation = models.ForeignKey(Designation, on_delete=models.CASCADE)
#     grade = models.ForeignKey(Grade, on_delete=models.CASCADE)
#     amount = models.DecimalField(max_digits=20, decimal_places=2)
#
#     def __str__(self):
#         return str(self.designation)
# # Create your models here.
#
#
# class ModeOfDelivery(ErpAbstractBaseModel):
#     TYPES = (
#             ('self', "SELF"),
#             ('3PL', "3PL"),
#             ('dispatch', "DISPATCH"),
#     )
#
#     type = models.CharField(max_length=30, choices=TYPES, default=TYPES[0])
#
#     def __str__(self):
#         return self.type
