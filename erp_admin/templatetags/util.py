from django import template

register = template.Library()




@register.simple_tag
def compare_uuid(arg1, arg2):
    if str(arg1) == str(arg2):
        return "selected"
    return ""


