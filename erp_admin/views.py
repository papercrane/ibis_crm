import datetime
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from enquiry.models import Enquiry
from employee.models import Employee
from student.models import Student, StudentCourse
from crm.models import Campaign
from product.models import Product
from student.models import StudentInstallment
from django.db.models import Count, Q
from django.db.models import Sum
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse


# Create your views here.
@login_required
def dashboard(request):
    if request.user.groups.filter(Q(name__iexact='director') | Q(name__iexact='front office admin')).count() == 1:
        dateToday = datetime.date.today()
        date_1 = datetime.datetime.strptime(str(dateToday), "%Y-%m-%d")
        next_day = date_1 + datetime.timedelta(days=1)
        end_date = date_1 + datetime.timedelta(days=2)
        installmentObj = StudentInstallment.objects.filter(
            Q(status__startswith='Not') | Q(status__startswith='Partially'))
        pendingPayment = installmentObj.filter(
            Q(due_date=end_date) | Q(due_date=next_day) | Q(due_date=date_1), student__loan=False)[
                         :10]
        overDue = installmentObj.filter(due_date__lt=date_1, student__loan=False)
        total_amount = (overDue.aggregate(Sum('student__total_amount')))
        paid_amount = (overDue.aggregate(Sum('paid_amount')))
        if paid_amount['paid_amount__sum'] == None:
            paid_amount['paid_amount__sum'] = 0
        if total_amount['student__total_amount__sum'] == None:
            total_amount['student__total_amount__sum'] = 0
        context = {
            'pendingPaymentCount': pendingPayment.count(),
            'pendingPayment': pendingPayment,
            'overDue': overDue[:10],
            'overDueCount': overDue.count(),
            'dueAmount': total_amount['student__total_amount__sum'] - paid_amount['paid_amount__sum'],
            'pendingDate': str(end_date)[0:10],
        }
        if request.user.groups.filter(name__iexact='director').count() == 1:
            products = Product.objects.annotate(
                total_students=Count('courses__batches__students', filter=Q(students__date_of_joining__year=dateToday.year)))
            context['products'] = products
            return render(request, "erp_admin/directordashboard.html", context)

        return render(request, "erp_admin/frontofficer.html", context)

    elif request.user.groups.filter(Q(name__iexact='student')):
        return HttpResponseRedirect(reverse('student:student_profile'))

    elif request.user.employee.primary_designation.name == 'Programme Director':
        students = StudentCourse.objects.filter(program_director=request.user.employee).count()
        return render(request, "erp_admin/program_director_dashboard.html", {'students': students})
    else:
        context = {
            'leads': Enquiry.objects.count(),
            'students': Student.objects.count(),
            'courses': Product.objects.count(),
            'employees': Employee.objects.count(),
            'campaigns': Campaign.objects.count()
        }
        return render(request, "erp_admin/dashboard.html", context)


def showPendingPay(request):
    dateToday = datetime.date.today()
    date_1 = datetime.datetime.strptime(str(dateToday), "%Y-%m-%d")
    next_day = date_1 + datetime.timedelta(days=1)
    end_date = date_1 + datetime.timedelta(days=2)
    installmentObj = StudentInstallment.objects.filter(
        Q(status__startswith='Not') | Q(status__startswith='Partially', student__loan=False))
    pendingPayment = installmentObj.filter(
        Q(due_date=end_date) | Q(due_date=next_day) | Q(due_date=date_1), student__loan=False)
    context = {
        'pendingPaymentCount': pendingPayment.count(),
        'pendingPayment': pendingPayment,
    }
    return render(request, "erp_admin/frontofficerPayments.html", context)


def showOverDue(request):
    dateToday = datetime.date.today()
    date_1 = datetime.datetime.strptime(str(dateToday), "%Y-%m-%d")
    installmentObj = StudentInstallment.objects.filter(Q(status__startswith='Not') | Q(status__startswith='Partially'),
                                                       student__loan=False).order_by('-due_date')
    overDue = installmentObj.filter(due_date__lt=date_1, student__loan=False)
    context = {
        'overDue': overDue,
    }
    return render(request, "erp_admin/frontofficerOverDue.html", context)
