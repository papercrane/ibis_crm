from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.db.models import functions, Value as V
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404, render
from django.contrib.auth.models import Group
from erp_core.pagination import paginate
from location.models import Location
from .models import *
from .forms import *
from employee.filters import EmployeeFilter
from django.core.mail import EmailMessage, BadHeaderError
from django.contrib.auth.tokens import default_token_generator
from django.contrib import messages
from django.contrib.auth.models import User


@login_required
def index(request):
    if request.user.employee.primary_designation.name == "Marketing Head":
        queryset = Employee.objects.filter(reporting_officer=request.user.employee)
    else:
        queryset = Employee.objects.exclude(code="#admin")
    f = EmployeeFilter(request.GET, queryset=queryset.order_by('code'))
    employees, pagination = paginate(request, queryset=f.qs, per_page=20, default=True)
    branch = request.GET.get('branch',False)
    if branch:
        branch = get_object_or_404(Location,pk=branch).city.name
    context = {
        'employees': employees,
        'pagination': pagination,
        'filter': f,
        'branch':branch

    }
    return render(request, "employee/index.html", context)


@login_required
def select_employee(request):
    employee_name = request.GET.get('employee_name', False)
    if employee_name:
        employees = Employee.objects.filter(
            user__first_name__icontains=employee_name
        ).annotate(
            text=functions.Concat('user__first_name', V(', '), 'primary_designation__name')
        ).values('id', 'text')
        return JsonResponse(list(employees), safe=False)
    return JsonResponse(list(), safe=False)

@login_required
# @permission_required('employee.add_employee', raise_exception=True)
def create(request):
    form1 = UserForm(request.POST or None)
    form2 = EmployeeForm(request.POST or None, request.FILES or None)
    form3 = AddressForEmployeeForm(request.POST or None)
    form4 = ContactForEmployeeForm(request.POST or None)

    if request.method == "POST":

        if form1.is_valid() and form2.is_valid() and form3.is_valid() and form4.is_valid():
            user = form1.save(commit=False)
            user.save()
            employee = form2.save(commit=False)
            employee.user = user
            employee.save()
            address = form3.save(commit=False)
            address.employee = employee
            address.save()
            contact = form4.save(commit=False)
            contact.employee = employee
            contact.save()
            form2.save_m2m()

            group1, created = Group.objects.get_or_create(name='employee')

            user.groups.add(group1)
            if employee.primary_designation:
                for pr in employee.primary_designation.permissions.all():
                    employee.user.user_permissions.add(pr)
            return HttpResponseRedirect(reverse('employee:profile', args=[employee.code]))

    context = {
        'form1': form1,
        'form2': form2,
        'form3': form3,
        'form4': form4,
        'form_url': reverse_lazy('employee:create')

    }

    return render(request, 'employee/create.html', context)


@login_required
def edit(request, pk):
    employee = get_object_or_404(Employee, code=pk)
    photo = employee.photo

    if request.method == "POST":
        form1 = UserEditForm(request.POST, instance=employee.user)
        form2 = EmployeeForm(request.POST, request.FILES, instance=employee)
        form3 = AddressForEmployeeForm(request.POST, instance=employee.employee_address)
        form4 = ContactForEmployeeForm(request.POST, instance=employee.employee_contact)

        if form1.is_valid() and form2.is_valid() and form3.is_valid() and form4.is_valid():

            user = form1.save(commit=False)
            user.save()
            employee = form2.save(commit=False)
            employee.user = user
            if employee.photo == "":
                employee.photo = photo
            employee.save()
            address = form3.save(commit=False)
            address.employee = employee
            address.save()
            contact = form4.save(commit=False)
            contact.employee = employee
            contact.save()
            form2.save_m2m()
            return HttpResponseRedirect(reverse('employee:profile', args=[employee.code]))

    else:

        form1 = UserEditForm(instance=employee.user)
        form2 = EmployeeForm(instance=employee)
        form3 = AddressForEmployeeForm(instance=employee.employee_address)
        form4 = ContactForEmployeeForm(instance=employee.employee_contact)

    context = {

        'form1': form1,
        'form2': form2,
        'form3': form3,
        'form4': form4,
        'edit': "edit",
        'form_url': reverse_lazy('employee:edit', args=[employee.code])

    }

    return render(request, 'employee/create.html', context)


@login_required
# @permission_required('employee.delete_employee', raise_exception=True)
def delete(request, pk):
    employee = get_object_or_404(Employee, code=pk)
    # employee = Employee.objects.get(code=pk)
    employee.delete()
    return HttpResponseRedirect(reverse('employee:list'))


@login_required
# @permission_required('employee.view_employee_details', raise_exception=True)
def profile(request, code):
    employee = get_object_or_404(Employee, code=code)
    return render(request, 'employee/profile.html', {'employee': employee})


def password_change(request):
    if request.method == "POST":
        uname = request.POST.get('uname', False)
        associated_users = User.objects.filter(username=uname)
        if associated_users.exists():
            user = User.objects.get(username=uname)
            # uid64 = urlsafe_base64_encode(force_bytes(user.pk))
            email = user.email
            subject = "Change Password"
            token = default_token_generator.make_token(user)
            link = "https://calm-retreat-87173.herokuapp.com/password/confirm/" + str(user.id) + "/" + token + "/"
            html_content = '<p>You are receving this email beacause you requested a password reset for your user account at IBIS CRM.</p>' + \
                           '<p>please go to the following page and choose a new password</p>' + '<p>' + link + '</p>'

            if subject and email:
                try:

                    msg = EmailMessage(subject, html_content, 'info@ibisacademy.in', [email])
                    msg.content_subtype = "html"  # Main content is now text/html
                    msg.send()
                except BadHeaderError:
                    return HttpResponse('Invalid header found.')
                messages.add_message(request, messages.ERROR, "Email has been sent to " + email +
                                     " email address. Please check its inbox to continue reseting password.")

                return HttpResponseRedirect('/accounts/login')
            else:
                messages.add_message(request, messages.ERROR, 'Make sure all fields are entered and valid.')
                return HttpResponseRedirect('/accounts/login')
        else:
            messages.add_message(request, messages.ERROR, 'No user is associated with this User Name')

    return render(request, 'employee/password_change.html', {})


# For confirm the student password before change


def password_confirmation(request, id, tk):
    us = User.objects.filter(id=id)
    if us:
        user = User.objects.get(id=id)
    if not us:
        return HttpResponseRedirect('/login')

    if request.method == "POST":

        password1 = request.POST.get('password1', False)
        password2 = request.POST.get('password2', False)

        if password1 and password2:
            if password1 != password2:
                messages.add_message(request, messages.ERROR, 'password_mismatch')
            else:
                if user is not None and default_token_generator.check_token(user, tk):
                    user.set_password(password1)
                    user.save()
                    messages.add_message(request, messages.ERROR, 'Password has been reset')

                    return HttpResponseRedirect('/accounts/login')
                else:
                    messages.error(request, 'The reset password link is no longer valid.')

    return render(request, 'employee/password_set.html', {'id': id, 'tk': tk})
