from django.conf.urls import include, url
from employee import views

app_name = "employee"


urlpatterns = [
    url(r'^employee/', include([
        url(r'^$', views.index, name="list"),
        url(r'^select_employee/$', views.select_employee, name="select_employee"),
        url(r'^create/$', views.create, name='create'),
        url(r'^(?P<pk>[^/]+)/edit/', views.edit, name='edit'),
        url(r'^(?P<pk>[^/]+)/delete/', views.delete, name='delete'),
        url(r'^(?P<code>[^/]+)/profile/', views.profile, name='profile'),

    ])),
    url('^change-password/$', views.password_change, name="password_reset"),
    url(r'^password/confirm/(?P<id>[-\w]+)/(?P<tk>[-\w]+)/$', views.password_confirmation, name="password_confirmation"),


]
