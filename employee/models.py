from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from address.models import *
from company.models import *
from erp_core.models import ErpAbstractBaseModel


class AddressForEmployee(Address):
    employee = models.OneToOneField(
        'Employee', on_delete=models.CASCADE, related_name="employee_address")

    def __str__(self):
        return str(self.employee)


class ContactForEmployee(Contact):
    employee = models.OneToOneField(
        'Employee', on_delete=models.CASCADE, related_name="employee_contact")


class Employee(ErpAbstractBaseModel):
    SEX = (
        ('Female', 'Female'),
        ('Male', 'Male'),
    )

    MARITALSTATUS = (
        ('Single', 'Single'),
        ('Married', 'Married'),
    )

    BLOODGROUP = (
        ('A+', 'A+'),
        ('A-', 'A-'),
        ('AB+', 'AB+'),
        ('AB-', 'AB-'),
        ('B+', 'B+'),
        ('B-', 'B-'),
        ('O+', 'O+'),
        ('O-', 'O-'),
    )
    code = models.CharField(max_length=255, unique=True)
    photo = models.FileField(upload_to='prof/%Y/%m/%d', blank=True, null=True)
    dob = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=8, choices=SEX, blank=True, null=True)
    marital_status = models.CharField(max_length=10, choices=MARITALSTATUS, blank=True, null=True)
    blood_group = models.CharField(max_length=3, choices=BLOODGROUP, blank=True, null=True)
    languages_known = models.CharField(max_length=255, blank=True, null=True)
    birth_place = models.CharField(max_length=100, blank=True, null=True)
    cast = models.CharField(max_length=100, null=True, blank=True)
    religion = models.CharField(max_length=100, null=True, blank=True)
    citizenship = models.CharField(max_length=100, blank=True, null=True)
    status = models.BooleanField(default=True)

    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)

    primary_designation = models.ForeignKey(
        Designation, on_delete=models.CASCADE)

    secondary_designation = models.ManyToManyField(
        Designation, related_name="employee_secondary_designation", blank=True)

    reporting_officer = models.ForeignKey(
        'employee.Employee', on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        permissions = (
            ("view_employee_list", "Can view employee list"),
            ("view_employee_details", "Can view employee details"),
        )

    def get_absolute_url(self):
        return reverse('employee:profile', args=[self.code])

    def __str__(self):
        return str(self.user.first_name) + " (" + str(self.code) + ")"


class EmployeeCode(ErpAbstractBaseModel):

    character_set = models.CharField(max_length=100)


