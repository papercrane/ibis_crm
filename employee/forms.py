from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm
from employee.models import Employee, AddressForEmployee, ContactForEmployee


class UserForm(UserCreationForm):
    class Meta:
        model = User
        widgets = {"username": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
                   "email": forms.TextInput(attrs={'class': "form-control", 'type': "email", 'required': "required"}),
                   "first_name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
                   "password1": forms.TextInput(
                       attrs={'class': "form-control", 'id': "exampleInputPassword1", 'placeholder': "Password"}),
                   "password2": forms.TextInput(
                       attrs={'class': "form-control", 'required': "required", 'placeholder': "Password"}),
                   }
        fields = ['username', 'email', 'first_name', 'password1', 'password1']


class UserEditForm(UserChangeForm):
    class Meta:
        model = User
        widgets = {"username": forms.TextInput(attrs={'class': "form-control"}),
                   "email": forms.TextInput(attrs={'class': "form-control", 'type': "email"}),
                   "first_name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
                   "password": forms.TextInput(attrs={'class': "form-control"}),
                   }
        fields = ['username', 'email', 'first_name', 'password']


class EmployeeForm(ModelForm):
    reporting_officer = forms.ModelChoiceField(
        required=False,
        label="Reports To",
        queryset=Employee.objects.none(),
        widget=forms.Select(attrs={'class': 'form-control', 'width': "100%"}),
    )

    class Meta:
        model = Employee
        widgets = {"code": forms.TextInput(attrs={'class': "form-control", 'required': "required", 'id': "code"}),
                   "dob": forms.DateInput(attrs={'class': "form-control", "id": "pldate", 'required': "required"}),
                   "gender": forms.Select(attrs={'class': "form-control", "data-toggle": "select"}),
                   "marital_status": forms.Select(attrs={'class': "form-control", "data-toggle": "select"}),
                   "blood_group": forms.Select(attrs={'class': "form-control", "data-toggle": "select"}),
                   "languages_known": forms.TextInput(attrs={'class': "form-control"}),
                   "citizenship": forms.TextInput(attrs={'class': "form-control"}),
                   "photo": forms.FileInput(attrs={'class': "form-control"}),
                   "primary_designation": forms.Select(
                       attrs={'class': "form-control", "data-toggle": "select", 'required': "required"}),

                   }
        fields = ['code', 'dob', 'gender', 'marital_status', 'blood_group', 'languages_known',
                  'citizenship', 'photo', 'primary_designation', 'reporting_officer']

    

class UserChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'first_name')


class AddressForEmployeeForm(ModelForm):
    class Meta:
        model = AddressForEmployee

        widgets = {"addressline1": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
                   "addressline2": forms.TextInput(attrs={'class': "form-control"}),
                   "area": forms.TextInput(attrs={'class': "form-control", }),
                   "city": forms.Select(attrs={'class': "form-control", }),
                   "country": forms.Select(attrs={'class': "form-control", }),
                   "state": forms.Select(attrs={'class': "form-control", }),
                   "zipcode": forms.NumberInput(attrs={'class': "form-control", }),

                   }
        fields = ['addressline1', 'addressline2', 'area', 'zipcode', 'city', 'state', 'country']


class ContactForEmployeeForm(ModelForm):
    class Meta:
        model = ContactForEmployee

        widgets = {"phone": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
                   "alternate_phone": forms.NumberInput(attrs={'class': "form-control", }),
                   }
        fields = ['phone', 'alternate_phone']
