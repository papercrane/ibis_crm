from django import template

register = template.Library()


@register.filter
def calculateTime(arg1):
    if arg1 > 0:
        return str(int(arg1 / 3600)) + " hour:" + str(int(arg1 / 60) % 60) + " minute"
    else:
        return 0

@register.filter
def TotalEnquiries(arg1):
    return Enquiries.object.filter(items__product__attributes__area_of_interest__in=stream).count
