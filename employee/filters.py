import django_filters
from django import forms
from django.db.models import Q

from employee.models import Employee
from company.models import Designation
from location.models import Location


class EmployeeFilter(django_filters.FilterSet):
    code = django_filters.CharFilter(lookup_expr='icontains', widget=forms.TextInput(attrs={'class': 'form-control'}))
    name = django_filters.CharFilter(lookup_expr='icontains', label="Name Contains", name="user__first_name",
                                     widget=forms.TextInput(attrs={'class': 'form-control'}))

    primary_designation = django_filters.ModelChoiceFilter(
        queryset=Designation.objects.all(),
        method='filter_by_designation',
        label='Designation',
        widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"})
    )
    branch = django_filters.ModelChoiceFilter(queryset=Location.objects.all(),
                                              widget=forms.Select(attrs={'class': 'form-control'}),
                                              method='filter_by_branch', label='Branch')

    class Meta:
        model = Employee
        fields = ['code']

    def filter_by_designation(self, queryset, name, value):
        return queryset.filter(
            Q(primary_designation=value)
        )

    def filter_by_branch(self, queryset, name, value):
        return queryset.filter(Q(primary_designation__location=value))
