from django.shortcuts import get_object_or_404, render
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError, transaction
from .forms import CityForm
from django.contrib import messages
from django.core.urlresolvers import reverse_lazy, reverse
from django.http import HttpResponseRedirect


@transaction.atomic
# @login_required
def add_city(request):
    if request.method == "POST":
        city_form = CityForm(request.POST)
        if city_form.is_valid():
            city_form.save()
            messages.add_message(request, messages.SUCCESS, 'City Created!!')
            if request.user.is_authenticated:
                return HttpResponseRedirect(reverse('enquiry:create'))
            return HttpResponseRedirect(reverse('enquiry:confirm_new_registration'))
    return HttpResponseRedirect(reverse('enquiry:create'))


@login_required
def settings(request):

    context = {


    }
    return render(request, "erp_admin/settings_dashboard.html", context)
