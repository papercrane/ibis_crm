import uuid

from django.db import models

from erp_core.models import ErpAbstractBaseModel


# from project.models import *


class Level(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return str(self.name)


class Zone(models.Model):
    name = models.CharField(max_length=100, unique=True)
    level = models.ForeignKey(Level, on_delete=models.CASCADE)

    zone = models.ForeignKey('self', on_delete=models.CASCADE, related_name="parent_zone", null=True, blank=True)

    def __str__(self):
        return str(self.name)


class Country(models.Model):
    name = models.CharField(max_length=100, unique=True)

    zone = models.ForeignKey(Zone, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return str(self.name)


class State(models.Model):
    name = models.CharField(max_length=100, unique=True)
    country = models.ForeignKey(Country, on_delete=models.CASCADE, default=True, related_name="states")

    # tin_number = models.PositiveIntegerField(default=0, null=True, blank=True)
    # code = models.CharField(max_length=10, null=True, blank=True)
    # student_code = models.CharField(max_length=10, null=True, blank=True)
    # student_count = models.PositiveIntegerField(default=0, null=True, blank=True)

    def __str__(self):
        return str(self.name)


class City(models.Model):
    name = models.CharField(max_length=100, unique=True)
    state = models.ForeignKey(State, on_delete=models.CASCADE, default=True, related_name="cities")
    code = models.CharField(max_length=20, unique=True, null=True, blank=True)

    def __str__(self):
        return str(self.name)


class Location(models.Model):
    name = models.CharField(max_length=100, unique=True)

    country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True, blank=True)

    state = models.ForeignKey(State, on_delete=models.CASCADE, null=True, blank=True)

    city = models.ForeignKey(City, on_delete=models.CASCADE)
    # tin = models.CharField(max_length=255, null=True, blank=True)
    gstin = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name +' - '+ self.city.name


class Office(ErpAbstractBaseModel):
    name = models.CharField(max_length=255, unique=True)
    buildingno = models.CharField(max_length=100, null=True, blank=True)
    phone = models.CharField("Phone Number", max_length=20, null=True, blank=True)
    alternate_phone = models.CharField("Alternate Phone Number", max_length=20, null=True, blank=True)
    fax_no = models.PositiveIntegerField(null=True, blank=True)

    location = models.ForeignKey(Location, on_delete=models.CASCADE,default=Location.objects.get(id=1).pk)

    def __str__(self):
        return self.name + '-' + str(self.location.city)
