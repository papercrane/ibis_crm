# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-12-04 08:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('location', '0002_auto_20181126_1411'),
    ]

    operations = [
        migrations.AddField(
            model_name='location',
            name='gstin',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
