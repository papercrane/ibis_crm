from django import forms
from django.forms import ModelForm
from location.models import City


class CityForm(ModelForm):

    class Meta:
        model = City
        widgets = {
            "name": forms.TextInput(attrs={'class': "form-control", 'required': "required", 'id': "name"}),
            "state": forms.Select(attrs={'class': "form-control", 'required': "required", 'id': "state",'style': "width: 100%;"}),


        }
        fields = ['name', 'state']
