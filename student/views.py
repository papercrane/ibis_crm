import datetime as date
import re
from datetime import timedelta, datetime
from decimal import Decimal

from django.contrib.auth.decorators import login_required, permission_required
from django.core.mail import EmailMessage
from django.db.models import F, Count, OuterRef, Subquery, Sum
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.contrib import messages
from easy_pdf.views import PDFTemplateView

from call_recorder.serializers import CallSerializer
from easyLink.decorators import group_required
from employee.models import Employee
from enquiry.models import Enquiry
from product.models import Course, CourseInstallment, Batch, CourseCertification, Subject, Product
from student.filters import StudentFilter, StudentInstallmentFilter, StudentCourseFilter
from enquiry.forms import StudentEnquiryForm
from erp_core.pagination import paginate, custom_paginate
from student.forms import StudentForm, ContactForStudentForm, AddressForStudentForm, StudentEditForm, InstallmentForm, \
    DiscountForm, StudentInstallmentForm, StudentPayForm, StudentCourseForm, MarkSheetForm, UploadFileForm, \
    MarkSheetEditForm, CertificateEditForm, QualificationEditForm, StudentCourseSubjectForm, StudentTranscriptEditForm, \
    ExtraPayForm, EditExtraPayForm
from student.models import StudentInstallment, Student, StudentCourse, StudentPaymentDetail, StudentPayment, \
    AddressForStudent, ContactForStudent, StudentCertificate, StudentSubject
from expense.models import Expense, IncomeType
from django.forms import inlineformset_factory, formset_factory
from django.utils import timezone
from django.contrib.auth.models import User, Group
from django.core.urlresolvers import reverse_lazy, reverse
from django.db import transaction, IntegrityError, models
from rest_framework import generics, status
from .serializers import CertificateSerializer
from rest_framework.decorators import api_view
from rest_framework import viewsets
from rest_framework.response import Response
from easy_pdf.rendering import render_to_pdf
from django.template.loader import get_template
from django.template import Context
from django.core.mail import EmailMultiAlternatives


@transaction.atomic
@login_required
@group_required('front office admin')
def create(request, pk):
    enquiry = get_object_or_404(Enquiry, pk=pk)
    form1 = StudentEnquiryForm(request.POST or None, instance=enquiry)
    form2 = StudentForm(request.POST or None, request.FILES or None)
    form4 = ContactForStudentForm(request.POST or None, initial={'phone': enquiry.contact_number})
    form3 = AddressForStudentForm(request.POST or None)
    course_formset = formset_factory(StudentCourseForm, extra=1)
    form5 = StudentCourseForm(request.POST or None, program=enquiry.course.all().values_list('pk', flat=True),
                              course=enquiry.interested_course.all().values_list('pk', flat=True))

    if request.method == "POST":
        # form5 = form5(request.POST)
        if form2.is_valid() and form3.is_valid() and form4.is_valid() and form5.is_valid() and form1.is_valid():
            try:
                enquiry = form1.save()
                enquiry.contact_number = request.POST.get('phone', False)
                enquiry.whatsapp_number = request.POST.get('phone', False)
                student = form2.save(commit=False)
                student.enquiry = enquiry
                student.save()
                form2.save_m2m()
                user, created = User.objects.get_or_create(username=student.enquiry.email,
                                                           defaults={'email': student.enquiry.email,
                                                                     'first_name': student.enquiry.name_of_candidate})
                user.set_password(student.aadharno)
                user.save()
                student.user = user
                student.save()
                group1, created = Group.objects.get_or_create(name='student')
                user.groups.add(group1)

                student.enquiry.status = Enquiry.STATUS_JOINED
                student.date_of_joining = timezone.now().date()
                student.enquiry.save()

                address = form3.save(commit=False)
                address.student = student
                address.save()

                contact = form4.save(commit=False)
                contact.student = student
                contact.save()

                course_obj = get_object_or_404(Course, id=form5['course__pk'].data)
                StudentCourse(date_of_joining=form5.cleaned_data.get('date_of_joining'), student=student,
                              course=course_obj, fees=course_obj.total_fee,
                              total_amount=course_obj.total_fee).save()

                messages.add_message(request, messages.SUCCESS, 'Student created Successfully.')
                return HttpResponseRedirect(reverse('student:detail', args=[student.pk]))

            except IntegrityError as e:
                print(e)
                messages.add_message(request, messages.INFO, 'This Email already exists.Please choose different Email')
                return HttpResponseRedirect(reverse('student:create', args=[pk]))

    context = {
        'form1': form1,
        'form2': form2,
        'form3': form3,
        'form4': form4,
        'form5': form5,
        'enquiry': enquiry,
        'form_url': reverse_lazy('student:create', args=[pk]),

    }

    return render(request, 'student/create.html', context)


@login_required
@permission_required('student.change_student', raise_exception=True)
def edit(request, pk):
    student = get_object_or_404(Student, pk=pk)
    form1 = StudentEnquiryForm(request.POST or None, instance=student.enquiry)
    form2 = StudentEditForm(request.POST or None, request.FILES or None, instance=student)
    form3 = AddressForStudentForm(request.POST or None, instance=student.student_address)
    form4 = ContactForStudentForm(request.POST or None, instance=student.student_contact)

    if request.method == "POST" and form2.is_valid() and form3.is_valid() and form4.is_valid() and form1.is_valid():
        form1.save()
        if 'phone' in form4.changed_data:
            student.enquiry.contact_number = request.POST.get('phone', False)
            student.enquiry.whatsapp_number = request.POST.get('phone', False)
            student.enquiry.save()
        # student.enquiry.name_of_candidate = request.POST('name_of_candidate')
        # if 'contact_number' in form1.changed_data:
        #     student.student_contact.contact_number = request.POST('contact_number')
        # if 'email' in form1.changed_data

        student = form2.save()
        address = form3.save(commit=False)
        address.student = student
        address.save()
        contact = form4.save(commit=False)
        contact.student = student
        contact.save()
        # student.details_filled = False

        # if student.enquiry.name_of_candidate and student.gender and student.enquiry.name_of_college and \
        #         student.enquiry.year_of_passout and \
        #         student.photo and student.enquiry.campaign and address.addressline1 and \
        #         address.area and address.zipcode and address.country and address.state and \
        #         address.city and contact.phone and contact.alternate_phone:
        #     student.details_filled = True
        student.save()
        messages.add_message(request, messages.SUCCESS, 'Updated!!')
        return HttpResponseRedirect(reverse('student:detail', args=[student.pk]))

    context = {

        'form1': form1,
        'form2': form2,
        'form3': form3,
        'form4': form4,
        'edit': "edit",
        'student': student,
        'form_url': reverse_lazy('student:edit', args=[student.pk])
    }

    return render(request, 'student/create.html', context)


@login_required
@permission_required('student.delete_student', raise_exception=True)
@group_required('front office admin')
def delete(request, pk):
    if request.method == "POST":
        student = get_object_or_404(Student, pk=pk)
        StudentCourse.objects.filter(student__pk=pk).delete()
        StudentInstallment.objects.filter(student__student__pk=pk).delete()
        StudentPayment.objects.filter(student__pk=pk).delete()
        StudentPaymentDetail.objects.filter(student_payment__student__pk=pk).delete()
        student.user.delete()
        student.delete()
        messages.add_message(request, messages.SUCCESS, 'Deleted!!')
        return HttpResponseRedirect(reverse('student:list'))


@login_required
@group_required('front office admin')
def loanstatus(request, pk):
    student = get_object_or_404(StudentCourse, pk=pk)
    status = request.POST.get("loanstatus")
    if status == 'not taken':
        student.loan = True
    elif status == 'taken':
        student.loan = False
    student.save()
    return HttpResponse(student)


@login_required
def detail(request, pk):
    student = get_object_or_404(Student, pk=pk)
    context = {
        'student': student,
        'statuses': StudentCourse.STATUSES
    }
    return render(request, 'student/detail.html', context)


@login_required
def student_profile(request):
    student = get_object_or_404(Student, user=request.user)
    context = {
        'student': student,
        # 'statuses': StudentCourse.STATUSES
    }
    return render(request, 'student/studentdetail.html', context)


@login_required
def index(request):
    students = Student.objects.all().order_by('-code')
    f = StudentFilter(request.GET, queryset=students)
    students, pagination, current_page, per_page = custom_paginate(request, queryset=f.qs, per_page=20, default=True)
    upload_form = UploadFileForm()

    context = {
        'students': students,
        'pagination': pagination,
        'filter': f,
        'upload_form': upload_form,
        'current_page': current_page,
        'per_page': per_page

    }
    return render(request, "student/index.html", context)


@login_required
def detail_index(request):
    students = StudentCourse.objects.annotate(balance_amount=F('total_amount') - F(
        'received_amount'))
    f = StudentCourseFilter(request.GET, queryset=students)
    students, pagination, current_page, per_page = custom_paginate(request, queryset=f.qs, per_page=20, default=True)

    context = {
        'students': students,
        'pagination': pagination,
        'filter': f,
        'current_page': current_page,
        'per_page': per_page
    }
    return render(request, "student/indexdetail.html", context)


@login_required
@group_required('front office admin')
def installments(request, pk):
    student = get_object_or_404(Student, pk=pk)
    installment_form = InstallmentForm()
    context = {
        'student': student,
        'installment_form': installment_form,
    }
    return render(request, 'student/installments.html', context)


def send_payment_email(student_installment, current_amount, email):
    html_content = "<p>Hi " + str(
        student_installment.student.student) + ",</p><p>We hope you’re doing well. Your payment of Rs " + str(
        current_amount) + " is received on " + str(
        student_installment.paid_date) + ".</p><p>With Regards ,</p><p>IBIS Groups</p>"
    subject = "Payment Received"
    try:

        msg = EmailMessage(subject, html_content, 'info@ibisacademy.in', [email])
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send(fail_silently=False)
    except Exception as e:
        pass


@login_required
@group_required('front office admin')
def make_payment(request, pk, installment):
    student = get_object_or_404(Student, pk=pk)
    student_installments = StudentInstallment.objects.get(student=student, pk=installment)
    installment = get_object_or_404(StudentInstallment, pk=installment)
    if request.method == "POST":
        installment_form = InstallmentForm(request.POST, instance=installment)
        last_paid_amount = installment.paid_amount
        installment = installment_form.save()

        if installment.paid_amount > 0:
            now_paid = installment.paid_amount
            if last_paid_amount:
                installment.paid_amount += last_paid_amount
            if installment.total_amount == installment.paid_amount:
                installment.status = StudentInstallment.PAID
            else:
                installment.status = StudentInstallment.PARTIALLY_PAID

            installment.save()
            remark = "Fee,Student Code:" + str(student.code)
            income_type = IncomeType.objects.get(name="Fee")

            Expense.objects.create(income_type=income_type, date=installment.paid_date, amount=now_paid,
                                   income_or_expense=False, description=remark)

        messages.add_message(request, messages.SUCCESS, 'Payment updated Successfully.')
        html_content = "<p>Hi " + str(
            student_installments.student) + ",</p><p>We hope you’re doing well. Your payment of Rs " + str(
            now_paid) + " is recieved on " + str(
            student_installments.paid_date) + ".</p><p>With Regards</p><p>IBIS Groups</p>"
        subject = "Payment Received"

        if student_installments.student.enquiry.email != None:
            send_payment_email(html_content, subject, student_installments.student.enquiry.email)
        else:
            pass

        return HttpResponseRedirect(reverse('student:payments', args=[pk]))

    context = {
        'student': student,
        'installment': installment,

    }
    return render(request, 'student/installments.html', context)


@login_required
@group_required('front office admin')
def add_discount(request, pk):
    student = get_object_or_404(Student, pk=pk)
    if request.method == "POST":
        discount_form = DiscountForm(request.POST, instance=student)
        discount_form.save()
        if student.discount:
            student.save()

        return HttpResponseRedirect(reverse('student:detail', args=[pk]))


@login_required
@group_required('front office admin')
def student_installments(request, pk):
    student = get_object_or_404(StudentCourse, pk=pk)
    InstallmentFormSet = inlineformset_factory(StudentCourse, StudentInstallment, form=StudentInstallmentForm,
                                               extra=1,
                                               min_num=1, can_delete=True)

    installment_formset = InstallmentFormSet(instance=student)
    if request.method == "POST":
        total = 0
        installment_formset = InstallmentFormSet(request.POST, instance=student)
        if installment_formset.is_valid():
            installment_formset.save()

            for installment in installments:
                total = +installment.total_amount
            return HttpResponseRedirect(reverse('student:payments', args=[student.pk]))

    context = {
        "installment_formset": installment_formset,
        'form_url': reverse_lazy('student:student_installments', args=[student.pk]),
        "type": "add",
        "student": student,
    }
    return render(request, "student/create_installments.html", context)


@login_required
@group_required('front office admin')
def student_payments(request, studentcourse_pk):
    studentcourse = get_object_or_404(StudentCourse, pk=studentcourse_pk)

    if request.method == 'POST':

        installment = get_object_or_404(StudentInstallment, pk=request.GET['payment_id'])
        previous_amount = installment.paid_amount

        installment_form = InstallmentForm(request.POST, instance=installment)

        if installment_form.is_valid():
            new_installment = installment_form.save(commit=False)

            if new_installment.amount_to_be_paid - new_installment.paid_amount == 0:
                new_installment.status = StudentInstallment.PAID
            elif new_installment.paid_amount:
                new_installment.status = StudentInstallment.PARTIALLY_PAID

            current_amount = new_installment.paid_amount - previous_amount

            if current_amount <= 0:
                messages.add_message(request, messages.INFO,
                                     'Current paid amount should be greater than previous amount.')
                return HttpResponseRedirect(reverse('student:student_payments', args=[studentcourse_pk]))

            new_installment.save()

            studentcourse.received_amount += current_amount
            if studentcourse.status == StudentCourse.STATUS_DISCONTINUED:
                studentcourse.closed_amount = studentcourse.total_amount - studentcourse.received_amount
            studentcourse.save()

            try:
                income = get_object_or_404(Expense, date=new_installment.paid_date, installment=new_installment)
                income.amount += current_amount
                income.save()

            except:
                income_type, created = IncomeType.objects.get_or_create(name="Fee")
                remark = "Fee,Student Code:" + str(studentcourse.student.code)
                Expense.objects.create(income_type=income_type, date=new_installment.paid_date, amount=current_amount,
                                       income_or_expense=False, description=remark, installment=new_installment)

            # if studentcourse.student.user.email:
            #         send_payment_email(new_installment, current_amount, studentcourse.student.user.email)

            invoice = studentcourse
            if studentcourse.student.user.email:
                balance_amount = studentcourse.total_amount - studentcourse.received_amount
                attachement_content = {'invoice': invoice, 'balance_amount': balance_amount, 'date': new_installment.paid_date, 'current_amount': current_amount}
                pdf_attachment = render_to_pdf("student/payment_receipt.html", attachement_content, encoding='utf-8')
                htmly = get_template('student/email.html')
                html_content = htmly.render({'invoice': invoice, 'balance_amount': balance_amount, 'date': new_installment.paid_date, 'current_amount': current_amount})
                msg = EmailMultiAlternatives("Payment Receipt", "Payment Receipt", "academyibis@gmail.com", [studentcourse.student.user.email])
                msg.attach_alternative(html_content, "text/html")
                msg.attach("receipt.pdf", pdf_attachment, 'application/pdf')
                msg.send()
                messages.add_message(request, messages.SUCCESS, "Receipt send successfully")
                invoice.send_mail = True
                # invoice.save()
            else:
                messages.add_message(request, messages.ERROR, "Student does not have an Email ID!")

            try:
                student_payment = StudentPayment.objects.create(date=new_installment.paid_date,
                                                                amount=current_amount,
                                                                student=installment.student.student)
                StudentPaymentDetail.objects.create(student_payment=student_payment,
                                                    student_installment=new_installment,
                                                    amount_splitted=current_amount)
            except IntegrityError as e:

                student_payment = get_object_or_404(StudentPayment, date=new_installment.paid_date,
                                                    student=new_installment.student.student)

                try:
                    payment_detail = get_object_or_404(StudentPaymentDetail, student_payment=student_payment,
                                                       student_installment=new_installment)
                    payment_detail.amount_splitted += current_amount
                    payment_detail.save()

                except:

                    StudentPaymentDetail.objects.create(student_payment=student_payment,
                                                        student_installment=new_installment,
                                                        amount_splitted=current_amount)

                student_payment.amount += current_amount
                student_payment.save()

            messages.add_message(request, messages.INFO, 'Payment updated successfully.')

            return HttpResponseRedirect(reverse('student:student_payments', args=[studentcourse_pk]))

    installments = StudentInstallment.objects.filter(student__pk=studentcourse_pk).order_by('date')
    balance = installments.aggregate(balance=Sum('amount_to_be_paid') - Sum('paid_amount'))
    installment_form = InstallmentForm()
    extra_pay_form = ExtraPayForm()
    context = {
        'student': studentcourse.student,
        'installment_form': installment_form,
        'student_course_pk': studentcourse_pk,
        'installments': installments,
        'balance': balance,
        'extra_pay_form': extra_pay_form
    }
    return render(request, 'student/installments.html', context)


@login_required
@group_required('front office admin')
# @permission_required('employee.change_employee', raise_exception=True)
def student_edit(request, pk):
    student = get_object_or_404(Student, code=pk)
    form1 = StudentEnquiryForm(request.POST or None, instance=student.enquiry)
    form2 = StudentEditForm(request.POST or None, request.FILES or None, instance=student)
    form3 = AddressForStudentForm(request.POST or None, instance=student.student_address)
    form4 = ContactForStudentForm(request.POST or None, instance=student.student_contact)

    if request.method == "POST" and form1.is_valid() and form2.is_valid() and form3.is_valid() and form4.is_valid():
        form1.save()
        student = form2.save()
        address = form3.save(commit=False)
        address.student = student
        address.save()
        contact = form4.save(commit=False)
        contact.student = student
        contact.save()

        return HttpResponseRedirect(reverse('student:detail', args=[student.pk]))

    context = {
        'form1': form1,
        'form2': form2,
        'form3': form3,
        'form4': form4,
        'student': student,
        'form_url': reverse_lazy('student:student_edit', args=[student.code])
    }

    return render(request, 'student/student_edit.html', context)


@login_required
@group_required('front office admin')
def create_first_installment(request, student_course_pk):
    if request.method == "POST":
        # student = get_object_or_404(Student, pk=student_pk)
        student_course = get_object_or_404(StudentCourse, pk=student_course_pk)
        course_installments = CourseInstallment.objects.filter(course=student_course.course)
        for course_installment in course_installments:
            try:
                date = datetime.strptime(request.POST['date'], '%Y-%m-%d') + timedelta(
                    days=course_installment.no_of_days)
            except:
                messages.add_message(request, messages.ERROR, 'Please enter a valid date!')
                return HttpResponseRedirect(reverse('student:student_payments', args=[student_course_pk]))
            StudentInstallment.objects.create(student=student_course,
                                              amount_to_be_paid=(
                                                                        student_course.total_amount * course_installment.percentage) / 100,
                                              date=date, paid_amount=0.0, due_date=date,
                                              percentage=course_installment.percentage)
    return HttpResponseRedirect(reverse('student:student_payments', args=[student_course_pk]))


@login_required
@group_required('front office admin')
def pay(request, pk):
    student_course = get_object_or_404(StudentCourse, pk=pk)
    InstallmentFormSet = inlineformset_factory(StudentCourse, StudentInstallment, form=StudentPayForm, extra=0,
                                               min_num=1, can_delete=True)
    installment_formset = InstallmentFormSet(request.POST or None, instance=student_course,
                                             queryset=student_course.installments.exclude(
                                                 status=StudentInstallment.PAID).order_by('date'))

    if request.method == "POST":
        if installment_formset.is_valid():
            installment_split_list = []
            installment_amount_list = []
            installment_date_list = []
            installment_amount_set_list = []
            for insta in student_course.installments.exclude(status=StudentInstallment.PAID).order_by('date'):
                installment_amount_list.append(insta.paid_amount)

            start_count = 0
            total_amount = 0
            installments = installment_formset.save(commit=False)

            if installments:
                for form in installments:
                    prev_amount = installment_amount_list[start_count]

                    if (form.amount_to_be_paid - form.paid_amount == 0):
                        form.status = StudentInstallment.PAID
                    elif form.paid_amount:
                        form.status = StudentInstallment.PARTIALLY_PAID

                    current_amount = form.paid_amount - prev_amount
                    form.save()

                    installment_split_list.append(current_amount)
                    start_count += start_count

                    if form.paid_date:
                        installment_date_list.append(form.paid_date)
                        installment_amount_set_list.append(current_amount)

                insta_count = 0

                for inst in len(installment_amount_set_list):
                    try:
                        student_payment = StudentPayment.objects.create(date=installment_date_list[inst],
                                                                        amount=installment_amount_set_list[inst],
                                                                        student=student_course.student)
                    except IntegrityError as e:
                        payment = StudentPayment.objects.get(paid_date=installment_date_list[inst])
                        payment.amount = total_amount
                        payment.save()

                for stud in installments:
                    if installment_split_list[insta_count] > 0:
                        payment = StudentPaymentDetail.objects.create(student_payment=student_payment,
                                                                      student_installment=stud,
                                                                      amount_splitted=installment_split_list[
                                                                          insta_count])
                    insta_count += insta_count

            return HttpResponseRedirect(reverse('student:student_payments', args=[student_course.pk]))
    context = {
        'installment_formset': installment_formset,
        'pk': pk,
    }
    return render(request, 'student/pay.html', context)


@login_required
def working_fee(request):
    student_courses = StudentCourse.objects.values('course__batch__name').annotate(total_turn_over=Sum('total_amount'),
                                                                                   total_closed_amount=Sum(
                                                                                       'closed_amount'),
                                                                                   total_received_amount=Sum(
                                                                                       'received_amount')).values(
        'course__batch__name', 'total_turn_over', 'total_closed_amount', 'total_received_amount',
        'course__batch__program__name'
    ).annotate(current_turn_over=
               F('total_turn_over') - F(
                   'total_closed_amount'), balance=F('current_turn_over') - F('total_received_amount')).order_by(
        'course__batch__program__name')
    context = {
        'student_courses': student_courses,
    }
    return render(request, 'student/working_fee.html', context)


@transaction.atomic
@login_required
@group_required('front office admin')
def changeStatus(request, pk):
    student = get_object_or_404(StudentCourse, pk=pk)
    old_status = student.status
    student.status = request.POST.get("status")
    if (student.status == StudentCourse.STATUS_DISCONTINUED) and (old_status != student.status):
        student.closed_amount = student.total_amount - student.received_amount

    student.save()
    return HttpResponse(student)


@transaction.atomic
@login_required
@group_required('front office admin')
def setDiscount(request, pk):
    student = get_object_or_404(StudentCourse, pk=pk)
    student.discount = Decimal(request.POST.get("discount"))
    if student.discount > student.fees:
        messages.add_message(request, messages.INFO, "Discount should not be greater than fees")
    else:
        student.total_amount = student.fees - student.discount
        # student_installments= student.installments.all()
        # # foo = somevalue
        # previous = next_ = None
        # l = len(student_installments)
        # for index, obj in enumerate(student_installments):
        #     obj.amount_to_be_paid = (student.total_amount * obj.percentage) / 100
        #     if obj.amount_to_be_paid == obj.paid_amount:
        #         obj.status = StudentInstallment.PAID
        #     elif obj.paid_amount == 0:
        #         obj.status = StudentInstallment.NOT_PAID
        #     elif

            #
            # if obj == foo:
            #     if index > 0:
            #         previous = student_installments[index - 1]
            #     if index < (l - 1):
            #         next_ = student_installments[index + 1]
        # st_ins = iter(student.installments.all())
        # st_ins.__next__()
        # for installment in student.installments.all():
        #     installment.amount_to_be_paid = (student.total_amount * installment.percentage) / 100
        #     if installment.amount_to_be_paid == installment.paid_amount:
        #         installment.status = StudentInstallment.PAID
        #     elif installment.amount_to_be_paid < installment.paid_amount:
        #         installment.status = StudentInstallment.PAID
        #         diff = installment.paid_amount - installment.amount_to_be_paid
        #         installment.next().paid_amount += diff
        #         installment.next.save()
        #     elif installment.paid_amount == 0:
        #         installment.status = StudentInstallment.NOT_PAID
        #     else:
        #         installment.status = StudentInstallment.PARTIALLY_PAID
        #
        #     installment.save()
    student.save()
    return HttpResponseRedirect(reverse('student:detail', args=[student.student.pk]))


@login_required
@group_required('front office admin')
def deleteStudentCourse(request, pk):
    student_course = get_object_or_404(StudentCourse, pk=pk)
    if student_course.installments.exclude(status=StudentInstallment.NOT_PAID).count():
        messages.add_message(request, messages.INFO,
                             'You cannot delete the course because you have paid the fees.')
    else:
        student_course.delete()
    return HttpResponseRedirect(reverse('student:detail', args=[student_course.student.pk]))


@login_required
@group_required('front office admin')
def addStudentCourse(request, pk):
    form = StudentCourseForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        student = get_object_or_404(Student, pk=pk)
        course_obj = get_object_or_404(Course, id=form['course__pk'].data)
        StudentCourse.objects.create(course=course_obj, student=student, fees=course_obj.total_fee,
                                     total_amount=course_obj.total_fee, date_of_joining=form['date_of_joining'].data)
        return HttpResponseRedirect(reverse('student:detail', args=[pk]))
    context = {
        'form': form
    }
    return render(request, 'student/add_student_course.html', context)


@login_required
def fee_reminder(request):
    today = date.date.today()
    end_week = today + date.timedelta(7)
    installments = StudentInstallment.objects.filter(date__gte=timezone.now().date(), date__lte=end_week).exclude(
        student__status=StudentCourse.STATUS_DISCONTINUED)
    f = StudentInstallmentFilter(request.GET, queryset=installments)
    installments, pagination = paginate(request, queryset=f.qs, per_page=50, default=True)
    context = {
        'installments': installments,
        'pagination': pagination,
        'filter': f,
    }
    return render(request, 'reminders/fee_remainder.html', context)


@login_required
@group_required('front office admin')
def editStudentCourse(request, pk):
    student_course = get_object_or_404(StudentCourse, pk=pk)
    # form = StudentCourseForm(instance=student_course,request.POST)
    form = StudentCourseForm(request.POST or None, initial={'course__pk': student_course.course.pk,
                                                            'date_of_joining': student_course.date_of_joining})

    if request.method == "POST" and form.is_valid():
        student_course.date_of_joining = form['date_of_joining'].data
        student_course.course = Course.objects.get(pk=form['course__pk'].data)
        student_course.save()
        return HttpResponseRedirect(reverse('student:detail', args=[student_course.student.pk]))
    context = {
        'form': form
    }
    return render(request, 'student/add_student_course.html', context)


@login_required
@group_required('front office admin')
# @permission_required('student.change_student', raise_exception=True)
def student_profile_edit(request, pk):
    student = get_object_or_404(Student, pk=pk)
    form1 = StudentEnquiryForm(request.POST or None, instance=student.enquiry)
    form2 = StudentEditForm(request.POST or None, request.FILES or None, instance=student)
    form3 = AddressForStudentForm(request.POST or None, instance=student.student_address)
    form4 = ContactForStudentForm(request.POST or None, instance=student.student_contact)

    if request.method == "POST" and form1.is_valid() and form2.is_valid() and form3.is_valid() and form4.is_valid():
        form1.save()
        student = form2.save()
        address = form3.save(commit=False)
        address.student = student
        address.save()
        contact = form4.save(commit=False)
        contact.student = student
        contact.save()
        student.details_filled = False
        if student.enquiry.name_of_candidate and student.gender and student.enquiry.name_of_college and \
                student.enquiry.year_of_passout and \
                student.photo and student.enquiry.campaign and address.addressline1 and \
                address.area and address.zipcode and address.country and address.state and \
                address.city and contact.phone and contact.alternate_phone:
            student.details_filled = True
            student.save()
        return HttpResponseRedirect(reverse('student:student_profile'))

    context = {
        'form1': form1,
        'form2': form2,
        'form3': form3,
        'form4': form4,
        'editstudent': 'editstudent',
        'student': student,
        'form_url': reverse_lazy('student:student_profile_edit', args=[student.pk])
    }

    return render(request, 'student/create.html', context)


@login_required
@group_required('front office admin')
def viewStudentCourse(request, pk):
    student_course = get_object_or_404(StudentCourse, pk=pk)

    context = {
        'student_course': student_course
    }
    return render(request, 'student/student_course_detail.html', context)


@login_required
def course_monthly_summary(request):
    if request.GET.get('month_year'):
        date_for_check = re.findall('\d{4}-\d{2}-\d{2}', str(request.GET.get('month_year')))
    else:
        date_for_check = [str(datetime.today().date()), str(datetime.today().date())]

    courses = Batch.objects.filter(batches__students__date_of_joining__range=date_for_check).annotate(
        total_students=Count('batches__students'))

    context = {
        'date_for_check': date_for_check,
        'start': datetime.strptime(date_for_check[0], '%Y-%m-%d'),
        'end': datetime.strptime(date_for_check[1], '%Y-%m-%d'),
        'courses': courses,
    }
    return render(request, 'student/course_monthly_summary.html', context)


class InvoicePDFView(PDFTemplateView):
    template_name = "student/invoice_pdf.html"

    def get_context_data(self, **kwargs):
        pk = kwargs.get('pk', False)
        # location = kwargs.get('location', False)

        return super(InvoicePDFView, self).get_context_data(
            pagesize="A4",
            title="Invoice",
            invoice=StudentPaymentDetail.objects.get(pk=pk),
            # location=Location.objects.get(pk=location)
        )


@login_required
def student_invoice(request, studentcourse_pk):
    students = StudentPaymentDetail.objects.filter(student_installment__student__student__pk=studentcourse_pk).order_by(
        '-id')

    try:

        context = {
            'students': students,
            'student_name': students.last().student_installment
        }
        return render(request, "student/invoice.html", context)

    except:
        messages.add_message(request, messages.ERROR, 'No Invoices!')

    return HttpResponseRedirect(reverse('student:detail', args=[studentcourse_pk]))


@transaction.atomic
@login_required
def import_student(request):
    form = UploadFileForm(request.POST or None, request.FILES or None)
    if request.method == "POST":
        if form.is_valid():
            try:
                file_data = request.FILES['file'].get_records()
                for row in file_data:
                    # student_course = StudentCourse.objects.get(enrollment_code=row['ENROLMENT NO.'])
                    # student_course.status = row['STATUS'].capitalize()
                    # student_course.save()
                    # print (row['NAME OF THE CANDIDATE'])
                    print(row['Course'])

                    if row['COURSE CODE']:
                        is_enquiry = Enquiry.objects.filter(email=row['MAIL ID']).exists()

                        if is_enquiry:

                            enquiry = Enquiry.objects.filter(email=row['MAIL ID']).last()


                        else:
                            enquiry = Enquiry.objects.filter(contact_number=row['CONTACT NO.']).last()
                            if enquiry:
                                if not enquiry.email:
                                    enquiry.email = row['MAIL ID']
                                    enquiry.save()
                            else:
                                enquiry = Enquiry.objects.create(name_of_candidate=row['NAME OF THE CANDIDATE'],
                                                                 email=row['MAIL ID'],
                                                                 contact_number=row['CONTACT NO.'],
                                                                 status=Enquiry.STATUS_JOINED)

                        user, created = User.objects.get_or_create(username=enquiry.email, email=enquiry.email,
                                                                   defaults={'first_name': enquiry.name_of_candidate})
                        if created:
                            user.set_password(enquiry.contact_number)
                            user.save()
                        student, created = Student.objects.get_or_create(enquiry=enquiry, defaults={'user': user})
                        print(row['NAME OF THE CANDIDATE'])
                        course = Course.objects.get(batch__name=row['Course'], start_date=row['Course Date'])
                        employee = Employee.objects.get(user__first_name=row['EMPLOYEE'])
                        student_course = StudentCourse.objects.get_or_create(student=student,
                                                                             enrollment_code=row['ENROLMENT NO.'],
                                                                             course_code=row['COURSE CODE'],
                                                                             date_of_joining=row['DATE OF JOINING'],
                                                                             course=course,
                                                                             status=(row['STATUS']).capitalize(),
                                                                             total_amount=row['PAYMENT AMOUNT'],
                                                                             fees=row['PAYMENT AMOUNT'],
                                                                             program_director=employee)
                        # if student_course.status == StudentCourse.STATUS_DISCONTINUED:
                        #     student_course.closed_amount = student_course.total_amount - student_course.received_amount
                        #     student_course.save()

                        AddressForStudent.objects.get_or_create(student=student)
                        ContactForStudent.objects.get_or_create(student=student, defaults={'phone': row['CONTACT NO.']})



            except IntegrityError:
                print(row['NAME OF THE CANDIDATE'])
                messages.add_message(request, messages.ERROR, 'The student already exists')

    return redirect("student:list")


@login_required
def certificate_detail(request, pk):
    certificates = StudentCertificate.objects.filter(student__pk=pk)
    context = {
        'certificates': certificates,
    }
    return render(request, "student/student_certificates.html", context)


@login_required
def CertificatePDFView(request, pk):
    course = StudentCertificate.objects.get(pk=pk)
    context = {
        'certificates': course,
    }
    return render(request, "student/certificate_pdf.html", context)


@login_required
def my_students(request):
    students = StudentCourse.objects.filter(program_director=request.user.employee).order_by('-id')
    f = StudentCourseFilter(request.GET, queryset=students)
    students, pagination = paginate(request, queryset=f.qs, per_page=20, default=True)
    context = {
        'students': students,
        'pagination': pagination,
        'filter': f,
    }
    return render(request, "program_director/students.html", context)


@login_required
def mark_sheet(request, pk):
    student = get_object_or_404(StudentCourse, pk=pk)
    form = MarkSheetForm(request.POST or None, course=student.course.batch.pk)
    if request.method == 'POST' and form.is_valid():
        student_certificate = form.save(commit=False)
        student_certificate.student = student
        student_certificate.save()

    context = {
        'certifications': student.certificates.all(),
        'form': form,
    }
    return render(request, "student/marksheet.html", context)


@login_required
def mark_sheet_edit(request, pk):
    certificate = get_object_or_404(StudentCertificate, pk=pk)
    form = MarkSheetEditForm(request.POST or None, instance=certificate)

    if request.method == 'POST':

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('student:marksheet', args=[certificate.student.pk]))

    context = {
        'certificate': certificate,
        'form': form,
    }
    return render(request, "program_director/certificate_edit.html", context)


@login_required
def edit_certificate(request, pk):
    certificate = get_object_or_404(StudentCertificate, pk=pk)
    form = CertificateEditForm(request.POST or None, instance=certificate)
    qualification_form = QualificationEditForm(request.POST or None, request.FILES or None,
                                               instance=certificate.student.student)

    if request.method == 'POST':

        if form.is_valid() and qualification_form.is_valid():
            form.save()
            qualification_form.save()
            return HttpResponseRedirect(reverse('student:certificate_pdf', args=[certificate.pk]))

    context = {
        'certificate': certificate,
        'qualification_form': qualification_form,
        'form': form,
    }
    return render(request, "student/edit_certificate.html", context)


@transaction.atomic
@login_required
def import_pays(request):
    form = UploadFileForm(request.POST or None, request.FILES or None)
    if request.method == "POST":
        if form.is_valid():
            try:
                file_data = request.FILES['file'].get_records()
                for row in file_data:
                    if row['COURSE CODE']:
                        student_course = StudentCourse.objects.get(enrollment_code=row['ENROLMENT NO.'],
                                                                   course_code=row['COURSE CODE'])
                        new_installment = StudentInstallment.objects.create(student=student_course,
                                                                            amount_to_be_paid=row['PAYMENT AMOUNT'],
                                                                            date=row['PAYMENT DATE'],
                                                                            paid_amount=row['AMOUNT PAID'],
                                                                            due_date=row['PAYMENT DATE'],
                                                                            paid_date=row['PAYMENT DATE'],
                                                                            percentage=100)

                        if new_installment.amount_to_be_paid - new_installment.paid_amount == 0:
                            new_installment.status = StudentInstallment.PAID
                        elif new_installment.paid_amount:
                            new_installment.status = StudentInstallment.PARTIALLY_PAID

                        new_installment.save()

                        student_course.received_amount += row['AMOUNT PAID']
                        if student_course.status == StudentCourse.STATUS_DISCONTINUED:
                            student_course.closed_amount = student_course.total_fee - student_course.received_amount
                        student_course.save()

                        try:
                            income = get_object_or_404(Expense, date=new_installment.paid_date,
                                                       installment=new_installment)
                            income.amount += row['AMOUNT PAID']
                            income.save()

                        except:
                            income_type, created = IncomeType.objects.get_or_create(name="Fee")
                            remark = "Fee,Student Code:" + str(student_course.student.code)
                            Expense.objects.create(income_type=income_type, date=new_installment.paid_date,
                                                   amount=row['AMOUNT PAID'],
                                                   income_or_expense=False, description=remark,
                                                   installment=new_installment)

                        try:
                            student_payment = StudentPayment.objects.create(date=new_installment.paid_date,
                                                                            amount=row['AMOUNT PAID'],
                                                                            student=student_course.student)
                            StudentPaymentDetail.objects.create(student_payment=student_payment,
                                                                student_installment=new_installment,
                                                                amount_splitted=row['AMOUNT PAID'])
                        except IntegrityError as e:
                            student_payment = get_object_or_404(StudentPayment, date=new_installment.paid_date,
                                                                student=new_installment.student.student)

                        try:
                            payment_detail = get_object_or_404(StudentPaymentDetail, student_payment=student_payment,
                                                               student_installment=new_installment)
                            payment_detail.amount_splitted += row['AMOUNT PAID']
                            payment_detail.save()
                            messages.add_message(request, messages.INFO, 'Payment updated successfully.')

                        except:
                            StudentPaymentDetail.objects.create(student_payment=student_payment,
                                                                student_installment=new_installment,
                                                                amount_splitted=row['AMOUNT PAID'])

                            student_payment.amount += row['AMOUNT PAID']
                            student_payment.save()

            except Exception as e:
                print(e)
                messages.add_message(request, messages.INFO, 'exception occured.')
                print(row['NAME OF THE CANDIDATE'])
    return redirect("student:list")


@login_required
def student_course_subjects(request, studentcourse_pk):
    course = get_object_or_404(StudentCourse, pk=studentcourse_pk)
    course_form = StudentTranscriptEditForm(request.POST or None, instance=course)
    # form = StudentCourseSubjectForm(program_director=request.user.employee)
    SubjectFormSet = inlineformset_factory(StudentCourse, StudentSubject, form=StudentCourseSubjectForm, extra=0,
                                           min_num=1, can_delete=True)
    subject_formset = SubjectFormSet(request.POST or None, instance=course,
                                     form_kwargs={'program_director': request.user.employee})
    if request.method == "POST":

        if subject_formset.is_valid() and course_form.is_valid():
            course_form.save()
            subjects = subject_formset.save(commit=False)
            for subject in subjects:
                subject.calculate_mca_pe()
                subject.save()

            subject_formset.save()

            # course.calculate_aggregate()
            course.save()
            messages.add_message(request, messages.SUCCESS, 'Success!!')
            if course.subjects.all():
                return HttpResponseRedirect(reverse('student:course_certificate', args=[course.pk]))
            else:
                return HttpResponseRedirect(reverse('student:my_students'))

    context = {
        "subject_formset": subject_formset,
        'form_url': reverse_lazy('student:student_course_subjects', args=[course.pk]),
        "type": "add",
        "course": course,
        "course_form": course_form
    }
    return render(request, "student/student_course_subjects.html", context)


@login_required()
def course_certificate(request, studentcourse_pk):
    course = get_object_or_404(StudentCourse, pk=studentcourse_pk)
    subjects = course.subjects.all().order_by('updated_at')

    context = {
        'course': course,
        'subjects': subjects

    }
    return render(request, "student/transcript_certificate.html", context)


@login_required()
def course_certificate_edit(request, studentcourse_pk):
    course = get_object_or_404(StudentCourse, pk=studentcourse_pk)
    form = StudentTranscriptEditForm(request.POST or None, instance=course)

    if request.method == 'POST':

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('student:course_certificate', args=[course.pk]))

    context = {
        'course': course,
        'form': form,
    }
    return render(request, "student/transcript_certificate_edit.html", context)


@transaction.atomic
@login_required
def extraPay(request, studentcourse_pk):
    extra_pay_form = ExtraPayForm(request.POST or None)
    studentcourse = get_object_or_404(StudentCourse, pk=studentcourse_pk)
    if extra_pay_form.is_valid():
        student_installment = extra_pay_form.save(commit=False)
        student_installment.amount_to_be_paid = student_installment.paid_amount
        student_installment.student = studentcourse
        student_installment.status = StudentInstallment.PAID
        student_installment.date = student_installment.paid_date

        studentcourse.received_amount += student_installment.paid_amount
        studentcourse.fees += student_installment.paid_amount
        studentcourse.total_amount += student_installment.paid_amount
        studentcourse.save()
        student_installment.save()

        try:
            income = get_object_or_404(Expense, date=student_installment.paid_date, installment=student_installment)
            income.amount += student_installment.paid_amount
            income.save()

        except:
            income_type, created = IncomeType.objects.get_or_create(name="Fee")
            remark = str(student_installment.remark) + ":" + str(studentcourse.student.code)
            Expense.objects.create(income_type=income_type, date=student_installment.paid_date,
                                   amount=student_installment.paid_amount,
                                   income_or_expense=False, description=remark,
                                   installment=student_installment)

        # if studentcourse.student.user.email:
        #     send_payment_email(new_installment, current_amount, studentcourse.student.user.email)

        try:
            student_payment = get_object_or_404(StudentPayment, date=student_installment.paid_date,
                                                student=studentcourse.student)

            try:
                payment_detail = get_object_or_404(StudentPaymentDetail, student_payment=student_payment,
                                                   student_installment=student_installment)
                payment_detail.amount_splitted += student_installment.paid_amount
                payment_detail.save()

            except:

                StudentPaymentDetail.objects.create(student_payment=student_payment,
                                                    student_installment=student_installment,
                                                    amount_splitted=student_installment.paid_amount)

            student_payment.amount += student_installment.paid_amount
            student_payment.save()

        except:
            student_payment = StudentPayment.objects.create(date=student_installment.paid_date,
                                                            amount=student_installment.paid_amount,
                                                            student=studentcourse.student)
            StudentPaymentDetail.objects.create(student_payment=student_payment,
                                                student_installment=student_installment,
                                                amount_splitted=student_installment.paid_amount)

        messages.add_message(request, messages.INFO, 'Payment updated successfully.')

        return HttpResponseRedirect(reverse('student:student_payments', args=[studentcourse_pk]))

    installments = StudentInstallment.objects.filter(student__pk=studentcourse_pk).order_by('date')
    balance = installments.aggregate(balance=Sum('amount_to_be_paid') - Sum('paid_amount'))
    installment_form = InstallmentForm()
    extra_pay_form = ExtraPayForm()
    context = {
        'student': studentcourse.student,
        'installment_form': installment_form,
        'student_course_pk': studentcourse_pk,
        'installments': installments,
        'balance': balance,
        'extra_pay_form': extra_pay_form
    }
    return render(request, 'student/installments.html', context)


@transaction.atomic
@login_required
def edit_extraPay(request, student_installment_pk):
    student_installment = get_object_or_404(StudentInstallment, pk=student_installment_pk)
    extra_pay_form = EditExtraPayForm(request.POST or None, instance=student_installment)

    studentcourse = student_installment.student
    if extra_pay_form.is_valid() and request.method == "POST":
        student_installment = extra_pay_form.save()

        # if 'paid_amount' in extra_pay_form.changed_data and 'paid_date' in extra_pay_form.changed_data:
        #     old_paid_amount = extra_pay_form.initial['paid_amount']
        #     old_paid_date = extra_pay_form.initial['paid_date']
        #     student_installment = extra_pay_form.save()
        #     student_installment.date = student_installment.paid_date
        #
        #     studentcourse.received_amount = studentcourse.received_amount - old_paid_amount + student_installment.paid_amount
        #     if studentcourse.received_amount > studentcourse.total_amount:
        #         studentcourse.fees = studentcourse.fees - old_paid_amount + student_installment.paid_amount
        #         studentcourse.total_amount = studentcourse.total_amount - old_paid_amount + student_installment.paid_amount
        #         # student_installment.amount_to_be_paid = student_installment.paid_amount
        #
        #     if student_installment.amount_to_be_paid > student_installment.paid_amount:
        #
        #         student_installment.status = StudentInstallment.PARTIALLY_PAID
        #
        #     elif student_installment.amount_to_be_paid == student_installment.paid_amount:
        #
        #         student_installment = StudentInstallment.PAID
        #     studentcourse.save()
        #     student_installment.save()
        #
        #     try:
        #         income = get_object_or_404(Expense, date=old_paid_date, installment=student_installment)
        #         income.amount = income.amount - old_paid_amount + student_installment.paid_amount
        #         income.date = student_installment.paid_date
        #         if 'remark' in extra_pay_form.changed_data:
        #             income.description = str(student_installment.remark) + ":" + str(studentcourse.student.code)
        #
        #         income.save()
        #
        #     except:
        #         pass
        #     # if studentcourse.student.user.email:
        #     #     send_payment_email(new_installment, current_amount, studentcourse.student.user.email)
        #
        #     try:
        #         student_payment = get_object_or_404(StudentPayment, date=old_paid_date,
        #                                             student=studentcourse.student)
        #
        #         try:
        #             payment_detail = get_object_or_404(StudentPaymentDetail, student_payment=student_payment,
        #                                                student_installment=student_installment)
        #             payment_detail.amount_splitted = payment_detail.amount_splitted - old_paid_amount + student_installment.paid_amount
        #             payment_detail.save()
        #
        #         except:
        #             pass
        #
        #         student_payment.amount = student_payment.amount - old_paid_amount + student_installment.paid_amount
        #         student_payment.save()
        #
        #     except:
        #         pass

        # elif 'paid_amount' in extra_pay_form.changed_data:
        #     old_paid_amount = extra_pay_form.initial['paid_amount']
        #     student_installment = extra_pay_form.save()
        #
        #     studentcourse.received_amount = studentcourse.received_amount - old_paid_amount + student_installment.paid_amount
        #     if studentcourse.received_amount > studentcourse.total_amount:
        #         studentcourse.fees = studentcourse.fees - old_paid_amount + student_installment.paid_amount
        #         studentcourse.total_amount = studentcourse.total_amount - old_paid_amount + student_installment.paid_amount
        #         # student_installment.amount_to_be_paid = student_installment.paid_amount
        #
        #     if student_installment.amount_to_be_paid > student_installment.paid_amount:
        #         student_installment.status = StudentInstallment.PARTIALLY_PAID
        #     elif student_installment.amount_to_be_paid == student_installment.paid_amount:
        #         student_installment.status = StudentInstallment.PAID
        #     studentcourse.save()
        #     student_installment.save()
        #
        #     try:
        #         income = get_object_or_404(Expense, date=student_installment.paid_date, installment=student_installment)
        #         income.amount = income.amount - old_paid_amount + student_installment.paid_amount
        #         if 'remark' in extra_pay_form.changed_data:
        #             income.description = str(student_installment.remark) + ":" + str(studentcourse.student.code)
        #
        #         income.save()
        #
        #     except:
        #         pass
        #     # if studentcourse.student.user.email:
        #     #     send_payment_email(new_installment, current_amount, studentcourse.student.user.email)
        #
        #     try:
        #         student_payment = get_object_or_404(StudentPayment, date=student_installment.paid_date,
        #                                             student=studentcourse.student)
        #
        #         try:
        #             payment_detail = get_object_or_404(StudentPaymentDetail, student_payment=student_payment,
        #                                                student_installment=student_installment)
        #             payment_detail.amount_splitted = payment_detail.amount_splitted - old_paid_amount + student_installment.paid_amount
        #             payment_detail.save()
        #
        #         except:
        #             pass
        #
        #         student_payment.amount = student_payment.amount - old_paid_amount + student_installment.paid_amount
        #         student_payment.save()
        #
        #     except:
        #         pass

        if 'paid_date' in extra_pay_form.changed_data:
            old_paid_date = extra_pay_form.initial['paid_date']
            # student_installment = extra_pay_form.save()
            # student_installment.date = student_installment.paid_date
            student_installment.save()

            try:
                income = get_object_or_404(Expense, date=old_paid_date, installment=student_installment)
                income.date = student_installment.paid_date
                if 'remark' in extra_pay_form.changed_data:
                    income.description = str(student_installment.remark) + ":" + str(studentcourse.student.code)

                income.save()
                student_payment = get_object_or_404(StudentPayment, date=old_paid_date, student=studentcourse.student)
                student_payment.date = student_installment.paid_date
                student_payment.save()


            except:
                pass
            # if studentcourse.student.user.email:
            #     send_payment_email(new_installment, current_amount, studentcourse.student.user.email)

        # elif 'amount_to_be_paid' in extra_pay_form.changed_data:
        #     student_installment = extra_pay_form.save(commit=False)
        #     if student_installment.amount_to_be_paid > student_installment.paid_amount:
        #         student_installment.status = StudentInstallment.PARTIALLY_PAID
        #     elif student_installment.amount_to_be_paid == student_installment.paid_amount:
        #         student_installment.status = StudentInstallment.PAID
        #     student_installment.save()
        #
        # else:
        #     student_installment = extra_pay_form.save()
        #
        #     if 'remark' in extra_pay_form.changed_data:
        #         income = get_object_or_404(Expense, date=student_installment.paid_date, installment=student_installment)
        #         income.description = str(student_installment.remark) + ":" + str(studentcourse.student.code)
        #         income.save()

        #     student_installment = extra_pay_form.save()
        #
        if 'remark' in extra_pay_form.changed_data:
            income = get_object_or_404(Expense, date=student_installment.paid_date, installment=student_installment)
            income.description = str(student_installment.remark) + ":" + str(studentcourse.student.code)
            income.save()

        messages.add_message(request, messages.SUCCESS, 'Update successful.')

        return HttpResponseRedirect(reverse('student:student_payments', args=[studentcourse.pk]))

    context = {
        'student_installment_pk': student_installment_pk,
        'extra_pay_form': extra_pay_form,
        'studentcourse_pk': studentcourse.pk,
        'student': student_installment.student,
        'installment': student_installment
    }
    return render(request, 'student/edit_extra_pay.html', context)


@login_required
def summary_working_fee(request):
    students = StudentCourse.objects.filter(course__batch__program=OuterRef('pk')).values('course__batch__program')
    # newest = newest.aggregate(total=Sum('total_amount'))
    total_turn_over = students.annotate(total_turn_over=Sum('total_amount')).values('total_turn_over')
    total_closed_amount = students.annotate(total_closed_amount=Sum('closed_amount')).values('total_closed_amount')
    total_received_amount = students.annotate(
        total_received_amount=Sum('received_amount')).values('total_received_amount')
    # current_turn_over = students.annotate(current_turn_over=F(Sum('total_amount')) - F(Sum('closed_amount'))).values('current_turn_over')

    p = Product.objects.annotate(total_turn_over=Subquery(total_turn_over),
                                 total_closed_amount=Subquery(total_closed_amount),
                                 total_received_amount=Subquery(total_received_amount))
    # student_courses = StudentCourse.objects.values('course__batch__name').annotate(total_turn_over=Sum('total_amount'),
    #                                                                                total_closed_amount=Sum(
    #                                                                                    'closed_amount'),
    #                                                                                total_received_amount=Sum(
    #                                                                                    'received_amount')).values(
    #     'course__batch__name', 'total_turn_over', 'total_closed_amount', 'total_received_amount',
    #     'course__batch__program__name'
    # ).annotate(current_turn_over=
    #            F('total_turn_over') - F(
    #                'total_closed_amount'), balance=F('current_turn_over') - F('total_received_amount')).order_by(
    #     'course__batch__program__name')
    context = {
        'products': p,
    }
    return render(request, 'student/summary_working_fee.html', context)


@login_required
def payment_history(request, student_pk):
    student = get_object_or_404(Student, pk=student_pk)
    student_payments = student.my_courses.aggregate(Sum('total_amount'), Sum('received_amount'))
    payment_history = StudentPayment.objects.filter(student__pk=student_pk)
    context = {'payment_history': payment_history, 'student': student, 'student_payments': student_payments}
    return render(request, 'student/payment_history.html', context)


@login_required
def installment_payment_history(request, installment_pk):
    studentinstallment = StudentInstallment.objects.filter(pk=installment_pk).last()
    payment_history = StudentPaymentDetail.objects.filter(student_installment__pk=installment_pk)
    context = {'payment_history': payment_history, 'studentinstallment': studentinstallment}
    return render(request, 'student/installment_payment_history.html', context)


@login_required
def mark_sheet_delete(request, pk):
    certificate = get_object_or_404(StudentCertificate, pk=pk)

    if request.method == 'POST':
        certificate.delete()
        return HttpResponseRedirect(reverse('student:marksheet', args=[certificate.student.pk]))




def certificate_verification(request):
    crtnum = request.POST.get('your-certificate')
    if request.method == 'POST':
        course = StudentCertificate.objects.get(certificate_number=crtnum)
        context = {
            'certificates': course,
        }
        return render(request, "student/certificate_detail_new.html", context)

    return render(request, 'student/certificate_verification.html')


class CertificateDetail(viewsets.ViewSet):
    def detail(self, request):
        crtnum = request.POST.get('your-certificate')
        queryset = StudentCertificate.objects.get(certificate_number='IES/NDT/TCR/18/00001')
        serializer = CertificateSerializer(queryset, many=False)
        return Response(serializer.data)