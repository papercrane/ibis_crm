from django.core.mail import EmailMessage
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.utils import timezone

from student.models import StudentInstallment


def send_email(html_content, subject, email):
    try:
        msg = EmailMessage(subject, html_content, 'academyibis@gmail.com',
                           [email])
        msg.content_subtype = "html"  # Main content is now text/html
        msg.send(fail_silently=False)
    except Exception as e:
        print(e)


class Command(BaseCommand):
    help = 'Reminds his/her job has crossed the deadline'

    def handle(self, *args, **options):
        now = timezone.now().date()
        student_installments = StudentInstallment.objects.filter(Q(installment__due_date=now) |
                                                                 Q(installment__due_date=now + timezone.timedelta(
                                                                     days=3)) | Q(
            installment__due_date=now + timezone.timedelta(days=2)),
                                                                 Q(installment__due_date=now + timezone.timedelta(
                                                                     days=1)),
                                                                 Q(installment__due_date__lte=now), status=StudentInstallment.NOT_PAID
                                                                 )
        for student_installment in student_installments:
            student = student_installment.student.enquiry
            html_content = "<p>Hi " + student.name_of_candidate + ",</p><p>I hope you’re well. This is just to remind you that payment for your course will be due on " + student_installment.installment.due_date + ".</p><p>With Regards</p><p>IBIS Groups</p>"
            subject = "Payment Due"
            send_email(html_content, subject, student.email)

        self.stdout.write(self.style.SUCCESS('Successfully'))
