from employee.models import Employee
from enquiry.models import Enquiry
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    def handle(self, *args, **options):
        enquiries = Enquiry.objects.filter(employee=None, assigned_to=None)

        for enquiry in enquiries:
            print(enquiry)
            enquiry.employee = Employee.objects.filter(user__first_name__icontains='shinoj').first()
            enquiry.assigned_to = enquiry.employee
            enquiry.save()
        self.stdout.write(self.style.SUCCESS('Successfully'))
