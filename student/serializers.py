from rest_framework import serializers
from .models import StudentCertificate


class CertificateSerializer(serializers.ModelSerializer):

    class Meta:
        model = StudentCertificate
        fields = ('student', 'validity', 'certificate_number')