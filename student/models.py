import math
from _decimal import Decimal
from decimal import getcontext, ROUND_UP

from django.db import models
from django.db.models import Q, Sum
from django.utils import timezone
from address.models import Address, Contact
from employee.models import Employee
from erp_core.models import ErpAbstractBaseModel, BaseCodeSetting
from enquiry.models import Enquiry
from product.models import Course, CourseCertification, Subject
from django.contrib.auth.models import User


class Student(ErpAbstractBaseModel):
    SEX = (
        ('Female', 'Female'),
        ('Male', 'Male'),
    )

    # @property
    def newCode():
        return StudentCodeSetting.new_code()

    code = models.CharField(max_length=20, default=newCode, unique=True)
    photo = models.FileField(upload_to='stud/%Y/%m/%d', blank=True, null=True)
    enquiry = models.OneToOneField(Enquiry, null=True, blank=True, related_name='enquiry')
    gender = models.CharField(max_length=8, choices=SEX, blank=True, null=True)
    details_filled = models.BooleanField(default=False)
    remark = models.CharField(max_length=255, null=True, blank=True)
    highest_qualification = models.CharField(max_length=255, blank=True, null=True)
    aadharno = models.CharField(max_length=255, blank=True, null=True)
    certificate = models.FileField(upload_to='certificate/%Y/%m/%d', blank=True, null=True)
    parent_name = models.CharField(max_length=255, blank=True, null=True)
    placement = models.BooleanField(default=False)
    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True, null=True, blank=True)
    outside_kerala = models.CharField(null=True, blank=True, max_length=25)

    def __str__(self):
        return self.enquiry.name_of_candidate

    def save(self, *args, **kwargs):
        if self.code and self.created_at is None:
            StudentCodeSetting.incrementCountIndex()

        return super(Student, self).save(*args, **kwargs)

    # def studCodeGenerator(self):
    #     location_code = self.enquiry.location
    #     # time_code = self.time
    #     start_date_code = self.start_date
    #     year = str(start_date_code)[2:4]
    #     stud_code = 'I' + str(location_code) + str(year)
    #     return stud_code


class AddressForStudent(Address):
    student = models.OneToOneField(Student, on_delete=models.CASCADE, related_name="student_address")

    def __str__(self):
        return str(self.student)


class ContactForStudent(Contact):
    student = models.OneToOneField(Student, on_delete=models.CASCADE, related_name="student_contact")

    def __str__(self):
        return str(self.student)


class StudentCodeSetting(BaseCodeSetting):
    pass


class StudentInstallment(ErpAbstractBaseModel):
    NOT_PAID = 'Not paid'
    PAID = 'Paid'
    PARTIALLY_PAID = 'Partially paid'
    STATUSES = (
        (NOT_PAID, "Not paid"),
        (PAID, "Paid"),
        (PARTIALLY_PAID, "Partially paid"),

    )
    student = models.ForeignKey('student.StudentCourse', related_name="installments")
    # installment = models.ForeignKey(CourseInstallment, related_name="course_installments", null=True, blank=True)
    date = models.DateField(default=timezone.now)
    due_date = models.DateField(default=timezone.now)
    paid_date = models.DateField(null=True, blank=True)
    paid_amount = models.DecimalField(max_digits=30, decimal_places=2, null=True, blank=True)
    remark = models.CharField(max_length=200, null=True, blank=True)
    amount_to_be_paid = models.DecimalField(max_digits=30, decimal_places=2, default=0.00)
    status = models.CharField(max_length=30, choices=STATUSES, default=NOT_PAID, blank=True)
    percentage = models.PositiveIntegerField(default=0)

    def __str__(self):
        return str(self.student)

    def balanceAmount(self):
        if self.amount_to_paid and self.paid_amount:
            return self.amount_to_paid - self.paid_amount
        elif not self.paid_amount:
            return self.amount_to_paid
        else:
            return 0


class StudentCourse(models.Model):
    STATUS_OPEN = 'Open'
    STATUS_COMPLETED = 'Completed'
    STATUS_DISCONTINUED = 'Discontinued'
    STATUS_ONGOING = 'Ongoing'
    STATUS_SEAT_BLOCKING = 'Seat Blocking'
    STATUSES = (
        (STATUS_OPEN, "Open"),
        (STATUS_COMPLETED, "Completed"),
        (STATUS_DISCONTINUED, "Discontinued"),
        (STATUS_ONGOING, "Ongoing"),
        (STATUS_SEAT_BLOCKING, "Seat Blocking"),

    )

    enrollment_code = models.CharField(max_length=20)
    course_code = models.CharField(max_length=50)
    student = models.ForeignKey(Student, related_name="my_courses")
    course = models.ForeignKey(Course, related_name='students')
    fees = models.PositiveIntegerField()
    discount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    total_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    no_of_installments = models.PositiveIntegerField(null=True, blank=True)
    status = models.CharField(max_length=30, choices=STATUSES, default=STATUS_OPEN)
    date_of_joining = models.DateField(null=True, blank=True)
    loan = models.BooleanField(default=False)
    closed_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    received_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    program_director = models.ForeignKey(Employee, null=True, related_name='students')
    date_of_completion = models.DateField(null=True, blank=True)

    def __str__(self):
        return str(self.student)

    def save(self, *args, **kwargs):
        self.course_code = self.course.code
        if not self.enrollment_code:
            # self.course.batch.count = self.course.batch.count + 1
            if self.course.batch.name == 'IELTS(spoken)':
                student_count = StudentCourse.objects.filter(course__batch__name='IELTS').count() + 1
            elif (self.course.batch.name == 'BLS') or (self.course.batch.name == 'BLS/ACLS'):
                student_count = StudentCourse.objects.filter(
                    course__batch__name='BLS').count()
                student_count = student_count + StudentCourse.objects.filter(
                    course__batch__name='BLS/ACLS').count() + 1
            elif (self.course.batch.program.name == 'MEP') or (
                    self.course.batch.program.name == 'Hospital Administration & Management') or (
                    self.course.batch.program.name == 'Nursing Coaching Courses') or (
                    self.course.batch.program.name == 'Logistics And Supply Chain Management'):
                student_count = StudentCourse.objects.filter(
                    course__batch__program=self.course.batch.program).count() + 1

            elif self.course.batch.name == "Diploma In QA/QC Mechanical Engineering" or self.course.batch.name == 'QA/QC Mechanical Engg. (Asnt Only)':
                student_count = StudentCourse.objects.filter(
                    course__batch__name='Diploma In QA/QC Mechanical Engineering').count()

                student_count = student_count + StudentCourse.objects.filter(
                    course__batch__name='QA/QC Mechanical Engg. (Asnt Only)').count() + 1

            else:
                student_count = StudentCourse.objects.filter(course__batch=self.course.batch).count() + 1
            self.course.batch.count = student_count

            self.enrollment_code = "IA" + self.course.batch.enrollment_abbreviation + str(
                format(student_count, '0' + str(3) + 'd'))
            self.course.batch.save()
            self.course.save()
        self.program_director = self.course.batch.program.program_director
        return super(StudentCourse, self).save(*args, **kwargs)

    def cumulative_gpa(self):
        mark = self.subjects.aggregate(tpe=Sum('points_earned'), tmca=Sum('max_credit_awarded'))
        if mark['tpe']:
            result = mark['tpe'] / mark['tmca']
            result = Decimal(result).quantize(Decimal('.01'), rounding=ROUND_UP)
        else:
            result = 0.00
        return result

    def ceu_awarded(self):
        ceu = self.subjects.aggregate(tcha=Sum('class_attendance_hours'))
        if ceu['tcha']:
            return (ceu['tcha'] / 10)
        return (0.00)

        # def calculate_aggregate(self):
        #     total_contact_hours = 0
        #     total_class_attendance_hours = 0
        #     total_grade = 0
        #     total_max_credit_awarded = 0
        #     total_mark_received = 0
        #     totalpoints_earned = 0
        #     for subject in self.subjects.all():
        #         total_contact_hours += subject.total_contact_hours
        #         total_class_attendance_hours += subject.class_attendance_hours
        #         total_mark_received += subject.mark_received
        #         total_grade += subject.grade
        #         total_max_credit_awarded += subject.max_credit_awarded
        #         totalpoints_earned += subject.points_earned

        # self.cue_awarded = (total_class_attendance_hours / 10)
        # self.cumulative_gpa = totalpoints_earned / total_max_credit_awarded

        return self

    def contact_hours_calculate_aggregate(self):
        total_contact_hours = 0

        for subject in self.subjects.all():
            total_contact_hours += subject.total_contact_hours

        return total_contact_hours

    def mark_received_calculate_aggregate(self):
        total_mark_received = 0

        for subject in self.subjects.all():
            total_mark_received += subject.mark_received

        return total_mark_received

    def class_attendance_hours_calculate_aggregate(self):
        total_class_attendance_hours = 0

        for subject in self.subjects.all():
            total_class_attendance_hours += subject.class_attendance_hours

        return total_class_attendance_hours

    def grade_calculate_aggregate(self):
        total_grade = 0

        for subject in self.subjects.all():
            total_grade += subject.grade

        return total_grade

    def max_credit_awarded_calculate_aggregate(self):
        total_max_credit_awarded = 0

        for subject in self.subjects.all():
            total_max_credit_awarded += subject.max_credit_awarded

        return total_max_credit_awarded

    def points_earned_awarded_calculate_aggregate(self):
        total_points_earned = 0

        for subject in self.subjects.all():
            total_points_earned += subject.points_earned

        return total_points_earned


class StudentPayment(models.Model):
    date = models.DateField()
    amount = models.DecimalField(max_digits=30, decimal_places=2)
    student = models.ForeignKey(Student, related_name="my_payments")

    class Meta:
        unique_together = ('student', 'date',)


class StudentPaymentDetail(models.Model):
    student_payment = models.ForeignKey(StudentPayment, related_name="student_payments")
    student_installment = models.ForeignKey(StudentInstallment)
    amount_splitted = models.DecimalField(max_digits=30, decimal_places=2)
    created_at = models.DateTimeField(auto_now_add=True)
    invoice_code = models.CharField(max_length=100)

    def save(self, *args, **kwargs):
        # invoice_code_setting = InvoiceCodeSetting.objects.last()
        # year_count = StudentPaymentDetail.objects.filter(created_at__year=timezone.now().date().year).count()
        #
        # if (timezone.now().strftime('%m') == '12') and (timezone.now().strftime('%d') == '05' and year_count == 0):
        #     invoice_code_setting.count_index = 1
        # else:
        #     invoice_code_setting.count_index += 1
        #
        # self.invoice_code = invoice_code_setting.prefix + str(timezone.now().strftime('%Y/%m/%d')) + '/' + str(
        #     format(invoice_code_setting.count_index, '0' + str(invoice_code_setting.no_of_characters) + 'd'))
        # invoice_code_setting.save()

        return super(StudentPaymentDetail, self).save(*args, **kwargs)

    def findgst(self):
        total = self.amount_splitted
        gst = (total * 9) / 100
        return (gst)

    def findbalance(self):
        total = self.amount_splitted
        gst = (total * 18) / 100
        withoutgst = total - gst
        return (withoutgst)


class InvoiceCodeSetting(BaseCodeSetting):
    pass
    # count = models.PositiveIntegerField(default=0)


class StudentCertificate(ErpAbstractBaseModel):
    certificate_number = models.CharField(max_length=50, null=True)
    student = models.ForeignKey(StudentCourse, related_name='certificates')
    certificate = models.ForeignKey(CourseCertification)
    training_date_from = models.DateField(null=True)
    training_date_to = models.DateField(null=True)
    training_hours = models.PositiveIntegerField(null=True)
    issue_date = models.DateField(null=True)
    validity = models.DateField(null=True)
    general = models.DecimalField(max_digits=4, default=0.00, decimal_places=2)
    specific = models.DecimalField(max_digits=4, default=0.00, decimal_places=2)
    practical = models.DecimalField(max_digits=4, default=0.00, decimal_places=2)
    aggregate = models.DecimalField(max_digits=4, default=0.00, decimal_places=2)
    work_experience = models.CharField(max_length=20, null=True)

    def __str__(self):
        return str(self.student)


class StudentSubject(ErpAbstractBaseModel):
    student = models.ForeignKey(StudentCourse, related_name='subjects')
    subject = models.ForeignKey(Subject, related_name='student_subject')
    total_contact_hours = models.DecimalField(max_digits=4, default=0.00, decimal_places=2)
    class_attendance_hours = models.DecimalField(max_digits=4, default=0.00, decimal_places=2)
    mark_received = models.DecimalField(max_digits=4, default=0.00, decimal_places=2)
    grade = models.DecimalField(max_digits=4, default=0.00, decimal_places=2)
    letter_grade = models.CharField(max_length=2, blank=True, null=True)
    max_credit_awarded = models.DecimalField(max_digits=4, default=0.00, decimal_places=2)
    points_earned = models.DecimalField(max_digits=4, default=0.00, decimal_places=2)

    def __str__(self):
        return str(self.student) + str(self.subject)

    def calculate_mca_pe(self):
        if self.subject.name != "Corporate Communication":
            self.max_credit_awarded = (self.total_contact_hours / 10)

        if self.mark_received >= 96.5 and self.mark_received <= 100:
            self.grade = 4
            self.letter_grade = "A+"

        elif self.mark_received >= 89.5 and self.mark_received < 96.5:
            self.grade = 3.7
            self.letter_grade = "A"

        elif self.mark_received >= 86.5 and self.mark_received < 89.5:
            self.grade = 3.3
            self.letter_grade = "B+"

        elif self.mark_received >= 79.5 and self.mark_received < 86.5:
            self.grade = 3.0
            self.letter_grade = "B"


        elif self.mark_received >= 76.5 and self.mark_received < 79.5:
            self.grade = 2.7
            self.letter_grade = "C+"

        elif self.mark_received >= 69.5 and self.mark_received < 76.5:
            self.grade = 2.0
            self.letter_grade = "C"

        elif self.mark_received >= 66.5 and self.mark_received < 69.5:
            self.grade = 1.7
            self.letter_grade = "D+"


        elif self.mark_received < 66.5 and self.mark_received >= 59.5:
            self.grade = 1.0
            self.letter_grade = "D"

        elif (self.subject.name == "Corporate Communication") or (self.subject.name == "CORPORATE COMMUNICATION"):
            self.grade = 0
            self.letter_grade = ""
            self.max_credit_awarded = 0


        else:
            self.grade = 0
            self.letter_grade = "F"

        if self.subject.name != "Corporate Communication":
            self.points_earned = self.max_credit_awarded * Decimal(self.grade)

        return self
