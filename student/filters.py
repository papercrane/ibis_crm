import django_filters
from django import forms
from student.models import Student, StudentInstallment, StudentCourse
from django_filters import widgets
from django.db.models import Q
from product.models import Product, Course, Batch
from datetime import datetime


class StudentFilter(django_filters.FilterSet):
    code = django_filters.CharFilter(name="code", lookup_expr='icontains',
                                     widget=forms.TextInput(attrs={'class': 'form-control'}))
    name = django_filters.CharFilter(label="Name", method="filter_by_name",
                                     widget=forms.TextInput(attrs={'class': 'form-control'}))

    program = django_filters.ModelChoiceFilter(
        queryset=Product.objects.all(),
        method='filter_by_program', widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"}))

    course = django_filters.ModelChoiceFilter(
        queryset=Batch.objects.all(),
        method='filter_by_course', widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"}))

    email = django_filters.CharFilter(name="email",method='filter_by_email')

    class Meta:
        model = Student
        fields = ['code', ]

    def filter_by_name(self, queryset, name, value):
        return queryset.filter(
            Q(enquiry__name_of_candidate__icontains=value)
        ).distinct()

    def filter_by_program(self, queryset, name, value):
        return queryset.filter(my_courses__course__batch__program=value).distinct()


    def filter_by_course(self, queryset, name, value):
        return queryset.filter(my_courses__course__batch=value).distinct()

    def filter_by_email(self, queryset, name, value):
        return queryset.filter(user__username__icontains=value).distinct()


class StudentInstallmentFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(label="Name", method="filter_by_name",
                                     widget=forms.TextInput(attrs={'class': 'form-control'}))

    course = django_filters.ModelChoiceFilter(
        queryset=Course.objects.all(),
        method='filter_by_course', widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"}))

    class Meta:
        model = StudentInstallment
        fields = ['name', 'course']

    def filter_by_name(self, queryset, name, value):
        return queryset.filter(
            Q(student__enquiry__name_of_candidate__icontains=value)
        )

    def filter_by_course(self, queryset, name, value):
        return queryset.filter(student__course__pk=value)

    def filter_by_program(self, queryset, name, value):
        return queryset.filter(student__my_courses_course__program__name=value)


class StudentCourseFilter(django_filters.FilterSet):

    enrollment_code = django_filters.CharFilter(name="enrollment_code", lookup_expr='icontains',
                                                widget=forms.TextInput(attrs={'class': 'form-control'}))

    course = django_filters.ModelChoiceFilter(

        queryset=Batch.objects.all(),
        name="course",
        widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;", }),
        label="Course", method="filter_by_course"
    )
    name = django_filters.CharFilter(label="Name", method="filter_by_name",
                                     widget=forms.TextInput(attrs={'class': 'form-control'}))
    status = django_filters.ChoiceFilter(choices=StudentCourse.STATUSES,
                                         widget=forms.Select(attrs={'class': 'form-control', 'style': "width: 100%;"}))

    date_of_joining = django_filters.DateFilter(
        label="date", method="filterby_date", widget=forms.DateInput(attrs={'class': 'form-control tdate'}))

    contact_number = django_filters.CharFilter(label="contact_number", method="filter_by_contactnumber",
                                               widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Student
        fields = ['code', ]

    def filterby_date(self, queryset, name, value):
        start_date = datetime.strptime(str(value), '%Y-%m-%d')
        return queryset.filter(
            Q(date_of_joining__month=start_date.month, date_of_joining__year=start_date.year)
        )

    def filter_by_name(self, queryset, name, value):
        return queryset.filter(
            Q(student__enquiry__name_of_candidate__icontains=value)
        )

    def filter_by_contactnumber(self, queryset, name, value):
        return queryset.filter(
            Q(student__enquiry__contact_number__icontains=value)
        )

    def filter_by_course(self, queryset, name, value):

        return queryset.filter(course__batch__name=value)
