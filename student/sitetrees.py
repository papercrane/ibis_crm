

from sitetree.utils import item, tree

# Be sure you defined `sitetrees` in your module.
sitetrees = (
    # Define a tree with `tree` function.
    tree('sidebar', items=[
        # Then define items and their children with `item` function.
        item(
            'Student',
            'student:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="user",
        ),
    ]),

    tree('sidebar_student', items=[
            # Then define items and their children with `item` function.
            item(
                'Student',
                'student:student_profile',
                in_menu=True,
                in_sitetree=True,
                access_loggedin=True,
                icon_class="user",
            ),

            item(
                'Logout',
                'account_logout',
                in_menu=True,
                in_sitetree=True,
                access_loggedin=True,
                icon_class="sign-out",
                ),
        ]),

    tree('sidebar_branch_head', items=[
        # Then define items and their children with `item` function.
        item(
            'Student',
            'student:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="user",
            # access_by_perms=['project.view_project_list'],

        ),

    ]),

    tree('sidebar_marketing_head', items=[
        # Then define items and their children with `item` function.
        item(
            'Student',
            'student:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="user",
            # access_by_perms=['project.view_project_list'],

        ),


    ]),

    tree('sidebar_director', items=[
        # Then define items and their children with `item` function.
        item(
            'Student',
            'student:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="user",
            # access_by_perms=['project.view_project_list'],

        ),
        item(
            'Fee Reminder',
            'student:fee_reminder',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="hourglass",

        ),

    ]),

    tree('sidebar_programme_director', items=[
        # Then define items and their children with `item` function.
        item(
            'Student',
            'student:my_students',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="user",

        ),

    ]),

    tree('sidebar_ibis_admin', items=[
        # Then define items and their children with `item` function.
        item(
            'Student',
            'student:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="user",
            # access_by_perms=['project.view_project_list'],

        ),

    ]),
    tree('sidebar_compliance_executive', items=[
        # Then define items and their children with `item` function.
        item(
            'Student',
            'student:list',
            in_menu=True,
            in_sitetree=True,
            access_loggedin=True,
            icon_class="user",
        ),

    ]),
    # ... You can define more than one tree for your app.
)
