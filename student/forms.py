from django import forms
from django.forms import ModelForm
from product.models import Course, CourseCertification, Subject
from student.models import Student, AddressForStudent, ContactForStudent, StudentInstallment, StudentCertificate, \
    StudentSubject, StudentCourse


class StudentForm(ModelForm):
    # selected_course = forms.SelectMultiple(attrs={'class': 'form-control js-basic-multiple', 'multiple': "TRUE"})

    # def __init__(self, *args, **kwargs):
    #     courses = kwargs.pop('course', None)
    #     super(StudentForm, self).__init__(*args, **kwargs)
    #     self.fields['selected_course'] = ModelMultipleChoiceField(
    #         queryset=Course.objects.filter(program__id__in=[course.id for course in courses]))
    # self.fields['selected_course'].initial = [course.id for course in courses]

    class Meta:
        model = Student
        widgets = {
            # "code": forms.TextInput(attrs={'class': "form-control", 'required': "required", 'id': "code"}),
            "photo": forms.FileInput(attrs={'class': "form-control"}),
            "gender": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "highest_qualification": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "aadharno": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "parent_name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            # "status": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "certificate": forms.FileInput(attrs={'class': "form-control"}),
            "placement": forms.CheckboxInput(attrs={}),
            "outside_kerala": forms.TextInput(attrs={'class': "form-control"}),
        }
        fields = ['photo', 'gender', 'highest_qualification', 'aadharno', 'parent_name', 'certificate', 'placement',
                  'outside_kerala']


class AddressForStudentForm(ModelForm):
    class Meta:
        model = AddressForStudent

        widgets = {"addressline1": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
                   "addressline2": forms.TextInput(attrs={'class': "form-control"}),
                   "area": forms.TextInput(attrs={'class': "form-control", }),
                   "city": forms.Select(attrs={'class': "form-control", }),
                   "country": forms.Select(attrs={'class': "form-control", }),
                   "state": forms.Select(attrs={'class': "form-control", }),
                   "zipcode": forms.NumberInput(attrs={'class': "form-control", }),
                   }
        fields = ['addressline1', 'addressline2', 'area', 'zipcode', 'city', 'state', 'country']


class ContactForStudentForm(ModelForm):
    class Meta:
        model = ContactForStudent

        widgets = {"phone": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
                   "alternate_phone": forms.NumberInput(attrs={'class': "form-control", }),
                   }
        fields = ['phone', 'alternate_phone']


class StudentEditForm(ModelForm):
    class Meta:
        model = Student

        widgets = {
            "photo": forms.FileInput(attrs={'class': "form-control"}),
            "gender": forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "highest_qualification": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "aadharno": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "parent_name": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "certificate": forms.FileInput(attrs={'class': "form-control"}),
            "placement": forms.CheckboxInput(attrs={}),
            "outside_kerala": forms.TextInput(attrs={'class': "form-control"}),
        }
        fields = ['photo', 'gender', 'highest_qualification', 'aadharno', 'parent_name', 'certificate', 'placement',
                  'outside_kerala']


class EditExtraPayForm(ModelForm):
    class Meta:
        model = StudentInstallment

        widgets = {
            "paid_date": forms.DateInput(attrs={'class': "form-control tdate", 'required': "required"}),
            # "paid_amount": forms.NumberInput(attrs={'class': "form-control"}),
            "date": forms.DateInput(attrs={'class': "form-control tdate", 'required': "required"}),

            "remark": forms.TextInput(attrs={'class': "form-control"}),
            # "amount_to_be_paid": forms.NumberInput(attrs={'class': "form-control"}),

        }
        fields = ['paid_date', 'remark', "date"]


class InstallmentForm(ModelForm):
    class Meta:
        model = StudentInstallment

        widgets = {
            # "paid_date": forms.DateInput(attrs={'class': "form-control tdate"},format=settings.DATE_INPUT_FORMATS),
            "paid_date": forms.DateInput(attrs={'class': "form-control tdate", 'required': "required"}),
            "paid_amount": forms.NumberInput(attrs={'class': "form-control"}),
            "date": forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            "amount_to_be_paid": forms.NumberInput(attrs={'class': "form-control"}),
            "remark": forms.TextInput(attrs={'class': "form-control"}),

        }
        fields = ['paid_date', 'paid_amount', 'date', 'amount_to_be_paid', 'remark']


class DiscountForm(ModelForm):
    pass
    # class Meta:
    #     model = Student
    #     widgets = {"discount": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
    #               "remark": forms.TextInput(attrs={'class': "form-control"}),
    #               "no_of_installments": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),

    #                }
    #     fields = ['discount','remark','no_of_installments']


class StudentInstallmentForm(ModelForm):
    class Meta:
        model = StudentInstallment

        widgets = {
            "date": forms.TextInput(attrs={'class': "form-control tdate fee_date", 'required': "required"}),
            "due_date": forms.TextInput(attrs={'class': "form-control tdate due_date", 'required': "required"}),
            "amount_to_be_paid": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            "remark": forms.TextInput(attrs={'class': "form-control"}),
        }
        fields = ['date', 'amount_to_be_paid', 'remark', 'due_date']


class StudentPayForm(ModelForm):
    class Meta:
        model = StudentInstallment

        widgets = {
            "date": forms.TextInput(attrs={'class': "form-control tdate fee_date", 'required': "required", }),
            "due_date": forms.TextInput(attrs={'class': "form-control tdate due_date", 'required': "required"}),
            "amount_to_be_paid": forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            "remark": forms.TextInput(attrs={'class': "form-control"}),
            "paid_date": forms.TextInput(attrs={'class': "form-control paid_date"}),
            "paid_amount": forms.NumberInput(
                attrs={'class': "form-control", }),

        }
        fields = ['date', 'amount_to_be_paid', 'remark', 'due_date', 'paid_date', 'paid_amount']


class StudentCourseForm(forms.Form):
    course__pk = forms.ModelChoiceField(
        widget=forms.Select(attrs={'class': "form-control name js-basic-multiple", 'required': "required", }),
        queryset=Course.objects.all().order_by('batch__program'))
    date_of_joining = forms.DateField(
        widget=forms.TextInput(attrs={'class': "form-control tdate", 'required': "required", }))

    def __init__(self, *args, **kwargs):
        program = kwargs.pop('program', None)
        course = kwargs.pop('course',None)
        super(StudentCourseForm, self).__init__(*args, **kwargs)
        if course:
            self.fields['course__pk'].queryset = Course.objects.filter(batch__in=course).order_by('-start_date')
        elif program:
            self.fields['course__pk'].queryset = Course.objects.filter(batch__program__in=program).order_by('-start_date')


class MarkSheetForm(ModelForm):
    class Meta:
        model = StudentCertificate

        widgets = {
            'general': forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            'specific': forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            'practical': forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            'aggregate': forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            'certificate': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            "certificate_number": forms.TextInput(attrs={'class': "form-control"}),
            "training_date_from": forms.DateInput(attrs={'class': "form-control tdate"}),
            "training_date_to": forms.DateInput(attrs={'class': "form-control tdate"}),
            "training_hours": forms.TextInput(attrs={'class': "form-control"}),
            "issue_date": forms.DateInput(attrs={'class': "form-control tdate"}),
            "validity": forms.DateInput(attrs={'class': "form-control tdate"}),
            "work_experience": forms.TextInput(attrs={'class': "form-control"}),

        }
        fields = ['certificate', 'general', 'specific', 'practical', 'aggregate', 'certificate_number',
                  'training_date_from', 'validity', 'work_experience', 'training_date_to', 'training_hours',
                  'issue_date']

    def __init__(self, *args, **kwargs):
        course = kwargs.pop('course', None)
        super(MarkSheetForm, self).__init__(*args, **kwargs)
        self.fields['certificate'].queryset = CourseCertification.objects.filter(course__id=course)


class CertificateEditForm(ModelForm):
    class Meta:
        model = StudentCertificate

        widgets = {
            # "student": forms.Select(attrs={'class': "form-control"}),
            "certificate_number": forms.TextInput(attrs={'class': "form-control"}),
            "training_date_from": forms.DateInput(attrs={'class': "form-control tdate"}),
            "training_date_to": forms.DateInput(attrs={'class': "form-control tdate"}),
            "training_hours": forms.TextInput(attrs={'class': "form-control"}),
            "issue_date": forms.DateInput(attrs={'class': "form-control tdate"}),
            "validity": forms.DateInput(attrs={'class': "form-control tdate"}),
            "general": forms.TextInput(attrs={'class': "form-control"}),
            "specific": forms.TextInput(attrs={'class': "form-control"}),
            "practical": forms.TextInput(attrs={'class': "form-control"}),
            "aggregate": forms.TextInput(attrs={'class': "form-control"}),
            "work_experience": forms.TextInput(attrs={'class': "form-control"}),
        }
        fields = ['training_date_from', 'training_date_to', 'training_hours', 'issue_date',
                  'validity', 'general', 'specific', 'practical', 'aggregate', 'work_experience', 'certificate_number']


class UploadFileForm(forms.Form):
    file = forms.FileField()


class MarkSheetEditForm(ModelForm):
    class Meta:
        model = StudentCertificate

        widgets = {
            'general': forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            'specific': forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            'practical': forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            'aggregate': forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            "certificate_number": forms.TextInput(attrs={'class': "form-control"}),
            "training_date_from": forms.DateInput(attrs={'class': "form-control tdate"}),
            "training_date_to": forms.DateInput(attrs={'class': "form-control tdate"}),
            "training_hours": forms.TextInput(attrs={'class': "form-control"}),
            "issue_date": forms.DateInput(attrs={'class': "form-control tdate"}),
            "validity": forms.DateInput(attrs={'class': "form-control tdate"}),
            "work_experience": forms.TextInput(attrs={'class': "form-control"}),

        }
        fields = ['general', 'specific', 'practical', 'aggregate', 'certificate_number', 'training_date_from',
                  'validity', 'work_experience', 'training_date_to', 'training_hours', 'issue_date']


class QualificationEditForm(ModelForm):
    class Meta:
        model = Student

        widgets = {
            'highest_qualification': forms.TextInput(attrs={'class': "form-control", 'required': "required"}),
            'photo': forms.FileInput(attrs={'class': "form-control"}),
            'gender': forms.Select(attrs={'class': "form-control", 'required': "required"}),

        }

        fields = ['highest_qualification', 'photo', 'gender']


class StudentCourseSubjectForm(ModelForm):
    class Meta:
        model = StudentSubject

        widgets = {
            'subject': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            'total_contact_hours': forms.NumberInput(attrs={'class': "form-control"}),
            'class_attendance_hours': forms.NumberInput(attrs={'class': "form-control"}),
            'mark_received': forms.NumberInput(attrs={'class': "form-control"}),
            'grade': forms.TextInput(attrs={'class': "form-control"}),
            'letter_grade': forms.TextInput(attrs={'class': "form-control"}),
            'max_credit_awarded': forms.NumberInput(attrs={'class': "form-control"}),
            'points_earned': forms.NumberInput(attrs={'class': "form-control"}),

        }

        fields = ['subject', 'total_contact_hours', 'class_attendance_hours', 'mark_received', 'grade', 'letter_grade',
                  'max_credit_awarded', 'points_earned']

    # def __init__(self, *args, **kwargs):
    #     print (kwargs)
    #     # course = kwargs.pop('studentcourse_pk', None)
    #     # print (course)
    #     student_course=StudentCourse.objects.get(pk=9)
    #     super(StudentCourseSubjectForm, self).__init__(*args, **kwargs)
    #     self.fields['subject'].queryset = Subject.objects.filter(program__id=student_course.course.pk)


# class TranscriptEditForm(ModelForm):
#     class Meta:
#         model = StudentCourse
#
#         widgets = {
#             'subject': forms.Select(attrs={'class': "form-control", 'required': "required"}),
#             'total_contact_hours': forms.NumberInput(attrs={'class': "form-control"}),
#             'class_attendance_hours': forms.NumberInput(attrs={'class': "form-control"}),
#             'mark_received': forms.NumberInput(attrs={'class': "form-control"}),
#             'grade': forms.NumberInput(attrs={'class': "form-control"}),
#             'letter_grade': forms.TextInput(attrs={'class': "form-control"}),
#             'max_credit_awarded': forms.NumberInput(attrs={'class': "form-control"}),
#             'points_earned': forms.NumberInput(attrs={'class': "form-control"}),
#
#         }
#
#         fields = ['subject','total_contact_hours','class_attendance_hours','mark_received','grade','letter_grade','max_credit_awarded',]
#


class StudentCourseSubjectForm(ModelForm):
    class Meta:
        model = StudentSubject

        widgets = {
            'subject': forms.Select(attrs={'class': "form-control", 'required': "required"}),
            'total_contact_hours': forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            'class_attendance_hours': forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            'mark_received': forms.NumberInput(attrs={'class': "form-control", 'required': "required"}),
            # 'grade': forms.NumberInput(attrs={'class': "form-control",'required': "required"}),
            # 'letter_grade': forms.TextInput(attrs={'class': "form-control",'required': "required"}),
            # 'max_credit_awarded': forms.NumberInput(attrs={'class': "form-control"}),
            # 'points_earned': forms.NumberInput(attrs={'class': "form-control"}),

        }

        fields = ['subject', 'total_contact_hours', 'class_attendance_hours', 'mark_received']

    def __init__(self, *args, **kwargs):
        program_director = kwargs.pop('program_director', None)
        super(StudentCourseSubjectForm, self).__init__(*args, **kwargs)
        if program_director:
            self.fields['subject'].queryset = Subject.objects.filter(program__batch__program__program_director=program_director)


class StudentTranscriptEditForm(ModelForm):
    class Meta:
        model = StudentCourse

        widgets = {

            'date_of_completion': forms.TextInput(attrs={'class': "form-control tdate"}),

        }

        fields = ['date_of_completion', ]


class ExtraPayForm(ModelForm):
    class Meta:
        model = StudentInstallment

        widgets = {
            "paid_date": forms.DateInput(attrs={'class': "form-control tdate", 'required': "required"}),
            "paid_amount": forms.NumberInput(attrs={'class': "form-control"}),
            "remark": forms.TextInput(attrs={'class': "form-control"}),

        }
        fields = ['paid_date', 'remark', "paid_amount"]
