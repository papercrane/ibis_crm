from django.contrib import admin
from .models import StudentCodeSetting, Student, AddressForStudent, ContactForStudent, StudentInstallment, \
    StudentCourse, StudentPayment, StudentPaymentDetail, InvoiceCodeSetting, StudentCertificate,StudentSubject


class StudentAdmin(admin.ModelAdmin):
    list_display = ('code',)

    class Meta:
        model = Student


class StudentCourseAdmin(admin.ModelAdmin):
    list_display = ('student', 'course', 'total_amount', 'closed_amount', 'received_amount')
    list_filter = ('course',)

    class Meta:
        model = StudentCourse


class StudentPaymentAdmin(admin.ModelAdmin):
    list_display = ('date', 'student', 'amount')
    ordering = ('-date',)

    class Meta:
        model = StudentPayment


class StudentPaymentDetailAdmin(admin.ModelAdmin):
    list_display = ('get_student', 'get_date', 'get_amount', 'get_course', 'get_installment_amount', 'get_paid_amount',
                    'get_installment_amount', 'amount_splitted')

    def get_student(self, obj):
        return obj.student_payment.student

    def get_date(self, obj):
        return obj.student_payment.student

    def get_amount(self, obj):
        return obj.student_payment.amount

    def get_course(self, obj):
        return obj.student_installment.student.course

    def get_installment_amount(self, obj):
        return obj.student_installment.amount_to_be_paid

    def get_paid_amount(self, obj):
        return obj.student_installment.paid_amount

    class Meta:
        model = StudentPaymentDetail

class StudentAdmin(admin.ModelAdmin):
    list_filter = ('enquiry__contact_number',)

    class Meta:
        model = Student

class ContactForStudentAdmin(admin.ModelAdmin):
    list_filter = ('phone',)

    class Meta:
        model = ContactForStudent

admin.site.register(StudentCodeSetting)
admin.site.register(InvoiceCodeSetting)
# admin.site.register(Student)
admin.site.register(Student, StudentAdmin)
admin.site.register(AddressForStudent)
admin.site.register(ContactForStudent, ContactForStudentAdmin)
admin.site.register(StudentInstallment)
admin.site.register(StudentCourse, StudentCourseAdmin)
admin.site.register(StudentPayment, StudentPaymentAdmin)
admin.site.register(StudentPaymentDetail, StudentPaymentDetailAdmin)
admin.site.register(StudentCertificate)
admin.site.register(StudentSubject)
