from django import template

register = template.Library()

from location.models import Location


@register.simple_tag
def officeDetails():
    gst = Location.objects.last().gstin
    if gst:
        return gst
    return 'No gst'

@register.simple_tag
def pag_count(arg1, arg2,arg3):
    present_count = arg1+(arg2-1)*arg3
    return present_count

@register.assignment_tag
def totaladmission_cal(arg1):
    ta = 0
    for item in arg1:
        if 'total_admission' in item.keys():
            ta = ta + item['total_admission']
    return ta


@register.assignment_tag
def closedadmission_cal(arg1):
    ca = 0
    for item in arg1:
        if 'closed_admission' in item.keys():
            ca = ca + item['closed_admission']
    return ca


@register.assignment_tag
def currentadmission_cal(arg1):
    ca = 0
    for item in arg1:
        if 'current_admission' in item.keys():
            ca = ca + item['current_admission']
    return ca
