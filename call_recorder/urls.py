from django.conf.urls import url, include
from . import views
from rest_framework.urlpatterns import format_suffix_patterns

app_name = 'call'

urlpatterns = [
    url(r'^callrecorder/', include([
        # url(r'^$', views.home, name='dashboard'),
        url(r'^(?P<pk>[^/]+)/edit/', views.home, name='dashboard'),
        url(r'^call/$', views.CallList.as_view()),
        url(r'^call/(?P<pk>[0-9]+)/$', views.CallDetail.as_view()),
        url(r'^upload/$', views.GenerateS3KeyForUpload.as_view()),
    ]))
]

urlpatterns = format_suffix_patterns(urlpatterns)
