from rest_framework import serializers
from .models import Call


class CallSerializer(serializers.ModelSerializer):
    audio_file = serializers.CharField()

    class Meta:
        model = Call
        fields = ('phone_number', 'date', 'audio_file')

    def create(self, validated_data, user=None):
        call = Call.objects.create(audio_file='callrecording/' + validated_data.pop('audio_file'),
                                   user=user, **validated_data)
        return call
