import time

import boto3
from botocore.config import Config
from django.conf import settings
from django.utils.datetime_safe import datetime
from rest_framework.views import APIView

from call_recorder.models import Call
from call_recorder.serializers import CallSerializer
from rest_framework import generics
from django.shortcuts import render
from rest_framework.authentication import TokenAuthentication, BasicAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.decorators import login_required
from rest_framework.response import Response
from rest_framework import generics, status
from django.contrib.auth.models import User

datetime.now()


@login_required
def home(request, pk):
    medias = Call.objects.filter(user__pk=pk)
    user = User.objects.get(pk=pk)
    context = {
        'medias': medias,
        'employee': user,
    }
    return render(request, 'call/home.html', context)


class CallList(generics.ListCreateAPIView):
    queryset = Call.objects.all()
    serializer_class = CallSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication, BasicAuthentication)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = self.perform_create(serializer, user=request.user)
        serializer = self.get_serializer(instance=instance)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer, user=None):
        return serializer.create(validated_data=serializer.validated_data, user=user)


class CallDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Call.objects.all()
    serializer_class = CallSerializer


class GenerateS3KeyForUpload(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication,)

    def post(self, request):
        file_name = "media/callrecording/" + request.data.get('file_name')
        file_type = request.data.get('file_type')

        ff = file_name.split(".")

        file_name = ff[0] + "_" + str(current_milli_time()) + "." + ff[1]
        s3 = boto3.client('s3',
                          aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                          aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
                          config=Config(signature_version='s3v4'))

        presigned_post = s3.generate_presigned_post(
            Bucket=settings.AWS_STORAGE_BUCKET_NAME,
            Key=file_name,
            Fields={"acl": "public-read",
                    "Content-Type": file_type
                    },
            Conditions=[
                {"acl": "public-read"},
                {"Content-Type": file_type}
            ],
            ExpiresIn=3600
        )
        return Response(data=presigned_post)


def current_milli_time():
    return int(round(time.time() * 1000))
