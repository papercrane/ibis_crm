from django.apps import AppConfig


class CallRecorderConfig(AppConfig):
    name = 'call_recorder'
