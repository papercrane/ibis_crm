from django.db import models
from django.contrib.auth.models import User


#
# class FileItem(models.Model):
#     name = models.CharField(max_length=120, null=True, blank=True)
#     path = models.TextField(blank=True, null=True)
#     size = models.BigIntegerField(default=0)
#     file_type = models.CharField(max_length=120, null=True, blank=True)
#     timestamp = models.DateTimeField(auto_now_add=True)
#     updated = models.DateTimeField(auto_now=True)
#     isActive = models.BooleanField(default=False)
#
#     @property
#     def title(self):
#         return str(self.name)


class Call(models.Model):
    phone_number = models.CharField(max_length=20)
    date = models.DateTimeField()
    audio_file = models.FileField(upload_to='callrecording')

    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.phone_number

    class Meta:
        ordering = ('date',)
